//#include "stdafx.h"
#include "MyForm.h"
#include "MyForm1.h"
#include "MyForm2.h"

using namespace emptyTest1;            //            The name of the namespace in MyForm.h

[STAThreadAttribute]                  //            Run this program in STA mode

int main(array<System::String ^> ^args) {

	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	//MyForm^ main_form = gcnew MyForm();
	MyForm2^ main_form = gcnew MyForm2();
	Application::Run(main_form);
	//Application::Run(gcnew MyF());

	return 0;

}
