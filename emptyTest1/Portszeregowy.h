#pragma once
//#include "Ktorywymiar.h"
//#include "Kalibracja.h"

namespace portszer {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	//using namespace Ktory;

	/// <summary>
	/// Podsumowanie informacji o Portszeregowy
	/// </summary>
	public ref class Portszeregowy //: public System::Windows::Forms::Form
	{
	public:
		String^ nazwa;
		String^ port;
		Double pomiar;
		//static Ktorywymiar^ wybor;
		//Kalibracja::Kalibracja^ kalibracja;

	public:
		Portszeregowy(String^ n, String^ p)
		{


			InitializeComponent();
			nazwa = n;
			port = p;

		}

	protected:
		/// <summary>
		/// Wyczy�� wszystkie u�ywane zasoby.
		/// </summary>
		~Portszeregowy()
		{

			if (components)
			{
				delete components;
			}
		}
	private: System::IO::Ports::SerialPort^  serialPort1;
	protected:
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Wymagana zmienna projektanta.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Wymagana metoda obs�ugi projektanta � nie nale�y modyfikowa� 
		/// zawarto�� tej metody z edytorem kodu.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->serialPort1 = (gcnew System::IO::Ports::SerialPort(this->components));
			//this->SuspendLayout();
			// 
			// serialPort1
			// 
			this->serialPort1->DtrEnable = true;
			this->serialPort1->RtsEnable = true;
			this->serialPort1->DataReceived += gcnew System::IO::Ports::SerialDataReceivedEventHandler(this, &Portszeregowy::serialPort1_DataReceived);

			// 
			// Portszeregowy
			// 
			/*this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(243, 186);
			this->Name = L"Portszeregowy";
			this->Text = L"Portszeregowy";
			this->ResumeLayout(false);*/

		}
#pragma endregion
	private: System::Void serialPort1_DataReceived(System::Object^  sender, System::IO::Ports::SerialDataReceivedEventArgs^  e)
	{
		array<Byte>^ bajty = gcnew array<Byte>(1);
		int i = 0;
		while (i<13)       // TU BY�O 13!!!!!!!!!!!!!!!!!!!!!!!!!
		{
			if (bajty[i] = serialPort1->ReadByte())
			{
				if (bajty[i] == ',')
					bajty[i] = '.';
				i++;
				bajty->Resize(bajty, i + 1);
			}
			else
				break;
		}
		bajty->Resize(bajty, i);
		String^ smieci = serialPort1->ReadExisting();
		serialPort1->DiscardInBuffer();
		String^ pomiary = System::Text::Encoding::Default->GetString(bajty, 0, bajty->Length);
		pomiary = pomiary->Remove(0, pomiary->IndexOf('.') - 5);
		Globalization::CultureInfo^ kultura = Globalization::CultureInfo::CreateSpecificCulture("en-US");
		Double liczba = Double::Parse(pomiary, kultura);
		pomiar = liczba;
		/*if (kalibracja!=nullptr)
		kalibracja->pomiar(pomiar);*/

	}
	public: void otworz()
	{
		this->serialPort1->PortName = port;
		this->serialPort1->Open();
	}
	public:	void wyslij()
	{
		serialPort1->Write("01 \u000d");
	}
	};
}
