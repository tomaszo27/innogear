#pragma once
#include "Header_Form.h"

namespace emptyTest1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Security::Cryptography;
	using namespace System::Data::SqlClient;
	using namespace Npgsql;


	using namespace System::DirectoryServices::AccountManagement;

	/// <summary>
	/// Summary for MyForm1
	/// </summary>
	public ref class logowanie : public System::Windows::Forms::Form
	{
	public:
		logowanie(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~logowanie()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	public: System::Windows::Forms::TextBox^  textBoxLogin;
	private: System::Windows::Forms::Label^  label2;
	public: System::Windows::Forms::TextBox^  textBoxPassword;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::BindingSource^  bindingSource1;
	private: System::Windows::Forms::Button^  button2;
	private: System::ComponentModel::IContainer^  components;
	
	public: System::Int32 id_pracownika;
	public: System::String^ uzytkownik;
	public: System::String^ stanowisko;

	private: System::Boolean abort = false;


	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBoxLogin = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->textBoxPassword = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->bindingSource1 = (gcnew System::Windows::Forms::BindingSource(this->components));
			this->button2 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource1))->BeginInit();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(56, 66);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(33, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Login";
			this->label1->Click += gcnew System::EventHandler(this, &logowanie::label1_Click);
			// 
			// textBoxLogin
			// 
			this->textBoxLogin->BackColor = System::Drawing::SystemColors::InactiveBorder;
			this->textBoxLogin->Location = System::Drawing::Point(95, 63);
			this->textBoxLogin->Name = L"textBoxLogin";
			this->textBoxLogin->Size = System::Drawing::Size(100, 20);
			this->textBoxLogin->TabIndex = 1;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(56, 99);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(36, 13);
			this->label2->TabIndex = 0;
			this->label2->Text = L"Has�o";
			this->label2->Click += gcnew System::EventHandler(this, &logowanie::label1_Click);
			// 
			// textBoxPassword
			// 
			this->textBoxPassword->Location = System::Drawing::Point(95, 96);
			this->textBoxPassword->Name = L"textBoxPassword";
			this->textBoxPassword->PasswordChar = '*';
			this->textBoxPassword->Size = System::Drawing::Size(100, 20);
			this->textBoxPassword->TabIndex = 2;
			this->textBoxPassword->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &logowanie::textBoxPassword_KeyDown);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(162, 163);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 3;
			this->button1->Text = L"Zaloguj";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &logowanie::button1_Click);
			// 
			// button2
			// 
			this->button2->DialogResult = System::Windows::Forms::DialogResult::Abort;
			this->button2->Location = System::Drawing::Point(162, 193);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 4;
			this->button2->Text = L"Anuluj";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &logowanie::button2_Click);
			// 
			// logowanie
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::Orange;
			this->ClientSize = System::Drawing::Size(340, 309);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBoxPassword);
			this->Controls->Add(this->textBoxLogin);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Name = L"logowanie";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Logowanie";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &logowanie::logowanie_FormClosing);
			this->Load += gcnew System::EventHandler(this, &logowanie::logowanie_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
	}

	private: System::Void logowanie_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
		bool isValid;
		if (abort != true) {

			///////////  Active directory   /////////////
			PrincipalContext^ pc = gcnew PrincipalContext(ContextType::Domain, "psik.pl");
			{
				try
				{
					String^ login = textBoxLogin->Text;
					String^ haslo = textBoxPassword->Text;
					// validatacja nazwy u�ytkownika i has�a
					isValid = pc->ValidateCredentials(login, haslo);
					pc->~PrincipalContext();
					// pobieranie informacji o u�ytkowniku
					PrincipalContext^ pc = gcnew PrincipalContext(ContextType::Domain, "psik.pl", login, haslo);
					UserPrincipal^ userPrinc = UserPrincipal::FindByIdentity(pc, login);

					uzytkownik = userPrinc->Name;
					stanowisko = userPrinc->Description;
				}
				catch (Exception^ e)
				{

				}
				
			

			if (isValid != true) {
				//e->Cancel = true;
				System::Windows::Forms::DialogResult odpowiedz = MessageBox::Show("Pr�bujesz ponownie?", "logowanie", MessageBoxButtons::OK, MessageBoxIcon::Question);
				switch (odpowiedz)
				{
				case System::Windows::Forms::DialogResult::OK:
					e->Cancel = true;
					textBoxLogin->Clear();
					textBoxPassword->Clear();
					break;
				default:
					e->Cancel = true;
					break;
				}
			}

			else {
				//abort = false;
				e->Cancel = false;
			}
			}

	
		}
		else
		{
			abort = false;
			e->Cancel = false;
		}

	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}

	private: System::Void textBoxPassword_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
		if (e->KeyCode == Keys::Enter)
			this->Close();
	}

	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		abort = true;
		this->Close();

	}
	private: System::Void logowanie_Load(System::Object^  sender, System::EventArgs^  e) {		//// usu� to

		//textBoxLogin->Text = "tomek";														//// usu� to autologowanie
		textBoxPassword->Text = "ZAQ!2wsx";														//// usu� to
		//this->Close();																			//// usu� to

		//textBoxLogin->Text = "tomaszo27";														//// usu� to autologowanie
		//textBoxPassword->Text = "zg0rek";														//// usu� to
		uzytkownik = "Bartek";
		stanowisko = "Janusz prog.";
		abort = true;
		this->Close();																			//// usu� to

	}																							//// usu� to
};
}
