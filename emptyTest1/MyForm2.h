#pragma once
#include "Header_Form.h"
#include "logowanie.h"
#include "MyForm.h"

namespace emptyTest1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Data::SqlClient;
	using namespace Npgsql;
	using namespace System::Diagnostics;
	using namespace System::Reflection;

	using namespace System::Threading::Tasks;

	using namespace ClassLibrary1;				//	informacje o obrabiarce


	/// <summary>
	/// Summary for MyForm2
	/// </summary>
	public ref class MyForm2 : public System::Windows::Forms::Form
	{
	public:
		MyForm2(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			//
		}
		String^ getMD5String(String^ inputString)
		{
			array<Byte>^ byteArray = System::Text::Encoding::ASCII->GetBytes(inputString);
			MD5CryptoServiceProvider^ md5provider = gcnew MD5CryptoServiceProvider();
			array<Byte>^ byteArrayHash = md5provider->ComputeHash(byteArray);
			return BitConverter::ToString(byteArrayHash);
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm2()
		{
			if (components)
			{
				delete components;
			}
		}

	public: System::Int32 id_pracownika;
	public: System::Int32 id_operacji;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	protected:

	private: System::Windows::Forms::TextBox^  textBox11;
	private: System::Windows::Forms::TextBox^  textBox10;
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	private: System::Windows::Forms::BindingSource^  bindingSource1;























	private: System::Windows::Forms::BindingSource^  bindingSource2;

	private: System::Windows::Forms::BindingSource^  bindingSource3;
	private: System::Windows::Forms::Label^  label33;
	private: System::Windows::Forms::Label^  label32;
	private: System::Windows::Forms::Label^  label31;
	private: System::Windows::Forms::Label^  label30;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::DataGridView^  dataGridView2;
	private: System::Windows::Forms::DataGridView^  dataGridView3;








































	private: System::ComponentModel::IContainer^  components;








	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm2::typeid));
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->textBox11 = (gcnew System::Windows::Forms::TextBox());
			this->textBox10 = (gcnew System::Windows::Forms::TextBox());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->bindingSource1 = (gcnew System::Windows::Forms::BindingSource(this->components));
			this->bindingSource2 = (gcnew System::Windows::Forms::BindingSource(this->components));
			this->bindingSource3 = (gcnew System::Windows::Forms::BindingSource(this->components));
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->label31 = (gcnew System::Windows::Forms::Label());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->dataGridView2 = (gcnew System::Windows::Forms::DataGridView());
			this->dataGridView3 = (gcnew System::Windows::Forms::DataGridView());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView3))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(14, 2);
			this->pictureBox1->Margin = System::Windows::Forms::Padding(5);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(457, 121);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox1->TabIndex = 18;
			this->pictureBox1->TabStop = false;
			// 
			// textBox11
			// 
			this->textBox11->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(51)),
				static_cast<System::Int32>(static_cast<System::Byte>(51)));
			this->textBox11->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->textBox11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox11->Location = System::Drawing::Point(1341, 33);
			this->textBox11->Margin = System::Windows::Forms::Padding(4);
			this->textBox11->Multiline = true;
			this->textBox11->Name = L"textBox11";
			this->textBox11->Size = System::Drawing::Size(335, 90);
			this->textBox11->TabIndex = 24;
			this->textBox11->Text = L"Klasyfikacja eksportowa: ";
			// 
			// textBox10
			// 
			this->textBox10->BackColor = System::Drawing::SystemColors::Window;
			this->textBox10->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->textBox10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox10->Location = System::Drawing::Point(523, 33);
			this->textBox10->Margin = System::Windows::Forms::Padding(4);
			this->textBox10->Multiline = true;
			this->textBox10->Name = L"textBox10";
			this->textBox10->ScrollBars = System::Windows::Forms::ScrollBars::Both;
			this->textBox10->Size = System::Drawing::Size(781, 90);
			this->textBox10->TabIndex = 25;
			this->textBox10->Text = L"Komunikat: ";
			// 
			// dataGridView1
			// 
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->BackgroundColor = System::Drawing::Color::White;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Location = System::Drawing::Point(329, 180);
			this->dataGridView1->Margin = System::Windows::Forms::Padding(4);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::True;
			this->dataGridView1->Size = System::Drawing::Size(180, 400);
			this->dataGridView1->TabIndex = 26;
			this->dataGridView1->SelectionChanged += gcnew System::EventHandler(this, &MyForm2::dataGridView1_SelectionChanged);
			// 
			// label33
			// 
			this->label33->AutoSize = true;
			this->label33->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label33->Location = System::Drawing::Point(105, 266);
			this->label33->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(62, 26);
			this->label33->TabIndex = 32;
			this->label33->Text = L"iSkra";
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label32->Location = System::Drawing::Point(74, 226);
			this->label32->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(107, 26);
			this->label32->TabIndex = 31;
			this->label32->Text = L"Kierownik";
			// 
			// label31
			// 
			this->label31->AutoSize = true;
			this->label31->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label31->Location = System::Drawing::Point(39, 190);
			this->label31->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label31->Name = L"label31";
			this->label31->Size = System::Drawing::Size(208, 26);
			this->label31->TabIndex = 30;
			this->label31->Text = L"Tomasz Kapeluszny";
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label30->Location = System::Drawing::Point(74, 147);
			this->label30->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(82, 26);
			this->label30->TabIndex = 29;
			this->label30->Text = L"label30";
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)));
			this->button1->FlatStyle = System::Windows::Forms::FlatStyle::Popup;
			this->button1->Location = System::Drawing::Point(1341, 180);
			this->button1->Margin = System::Windows::Forms::Padding(4);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(195, 95);
			this->button1->TabIndex = 33;
			this->button1->Text = L"Wybierz operacje";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm2::button1_Click_1);
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)));
			this->button2->FlatStyle = System::Windows::Forms::FlatStyle::Popup;
			this->button2->Location = System::Drawing::Point(1341, 283);
			this->button2->Margin = System::Windows::Forms::Padding(4);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(195, 95);
			this->button2->TabIndex = 33;
			this->button2->Text = L"Zako�cz operacje";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Visible = false;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm2::button2_Click);
			// 
			// button3
			// 
			this->button3->BackColor = System::Drawing::Color::DodgerBlue;
			this->button3->FlatStyle = System::Windows::Forms::FlatStyle::Popup;
			this->button3->Location = System::Drawing::Point(1341, 480);
			this->button3->Margin = System::Windows::Forms::Padding(4);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(195, 95);
			this->button3->TabIndex = 33;
			this->button3->Text = L"Zamkni�cie techniczne";
			this->button3->UseVisualStyleBackColor = false;
			this->button3->Visible = false;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm2::button3_Click);
			// 
			// dataGridView2
			// 
			this->dataGridView2->BackgroundColor = System::Drawing::Color::White;
			this->dataGridView2->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView2->Location = System::Drawing::Point(517, 180);
			this->dataGridView2->Margin = System::Windows::Forms::Padding(4);
			this->dataGridView2->Name = L"dataGridView2";
			this->dataGridView2->ReadOnly = true;
			this->dataGridView2->RowHeadersVisible = false;
			this->dataGridView2->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::True;
			this->dataGridView2->Size = System::Drawing::Size(180, 400);
			this->dataGridView2->TabIndex = 26;
			this->dataGridView2->SelectionChanged += gcnew System::EventHandler(this, &MyForm2::dataGridView2_SelectionChanged);
			// 
			// dataGridView3
			// 
			this->dataGridView3->BackgroundColor = System::Drawing::Color::White;
			this->dataGridView3->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView3->Location = System::Drawing::Point(711, 180);
			this->dataGridView3->Name = L"dataGridView3";
			this->dataGridView3->RowHeadersVisible = false;
			this->dataGridView3->Size = System::Drawing::Size(530, 400);
			this->dataGridView3->TabIndex = 34;
			this->dataGridView3->SelectionChanged += gcnew System::EventHandler(this, &MyForm2::dataGridView3_SelectionChanged);
			// 
			// MyForm2
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(12, 25);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoScroll = true;
			this->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(51)), static_cast<System::Int32>(static_cast<System::Byte>(102)),
				static_cast<System::Int32>(static_cast<System::Byte>(153)));
			this->ClientSize = System::Drawing::Size(1284, 792);
			this->Controls->Add(this->dataGridView3);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->label33);
			this->Controls->Add(this->label32);
			this->Controls->Add(this->label31);
			this->Controls->Add(this->label30);
			this->Controls->Add(this->dataGridView2);
			this->Controls->Add(this->dataGridView1);
			this->Controls->Add(this->textBox10);
			this->Controls->Add(this->textBox11);
			this->Controls->Add(this->pictureBox1);
			this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->Margin = System::Windows::Forms::Padding(5);
			this->Name = L"MyForm2";
			this->Text = L"MyForm2";
			this->WindowState = System::Windows::Forms::FormWindowState::Maximized;
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &MyForm2::MyForm2_FormClosing);
			this->Load += gcnew System::EventHandler(this, &MyForm2::MyForm2_Load);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MyForm2::MyForm2_Paint);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView3))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
		
#pragma endregion
//	public: delegate void wyswDelegata(String^ str);
	private: NpgsqlConnection^ connection;
	private: NpgsqlCommand^ selectcomm;
	private: NpgsqlCommand^ selectcomm_g2;
	private: NpgsqlCommand^ selectcomm_g3;
	private: NpgsqlCommand^ updacom;
	private: NpgsqlTransaction^ transaction;
	private: String^ CDNumber;
	private: String^ CDNumber_2;
	public: Przekazywane^ doPrzekazania;

	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	private: System::Void MyForm2_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
	}
	private: System::Void MyForm2_Load(System::Object^  sender, System::EventArgs^  e) {
		this->Visible = false;
		doPrzekazania = gcnew Przekazywane;
		Assembly^ assembly = Assembly::LoadFrom("ClassLibrary1.dll");
		Type^ type = assembly->GetType("nazwa_obrabiarki");
		//MethodInfo^ myMethod = type->GetMethod("nazwa_obrabiarki");
//		Object^ instanceOfMyType = Activator::CreateInstance(type);
		//myMethod->Invoke(instanceOfMyType, nullptr);
		Obrabiarka^ infoObrabiarka = gcnew Obrabiarka;
		label30->Text = infoObrabiarka->nazwa_obrabiarki;
		doPrzekazania->nazwa_obrabiarki = infoObrabiarka->nazwa_obrabiarki;											//				przekazywanie nazwy maszyny
		label33->Text = "upr.: OK";
		doPrzekazania->informacja = label33->Text;
		
		logowanie^ forma = gcnew logowanie();
		forma->ShowDialog();

		if (forma->DialogResult == System::Windows::Forms::DialogResult::Abort)
			this->Close();
		else if (forma->stanowisko == "Technolog") {
			this->Close();
			Process^ panel_nadzorcy = gcnew Process();
			//panel_nadzorcy->StartInfo->FileName = "C://Users//PSIK1//Desktop//INOGEAR - aplikcja i projekt (Jacek Nowak)//PROJEKTY//panel_nadzorcy//Release//panel_nadzorcy.exe";
			//
			panel_nadzorcy->StartInfo->FileName = "panel_nadzorcy.exe";
			panel_nadzorcy->StartInfo->WorkingDirectory = "C:\\Users\\PSIK1\\Desktop\\INOGEAR - aplikcja i projekt (Jacek Nowak)\\PROJEKTY\\panel_nadzorcy\\Release";
			panel_nadzorcy->Start();
			
		}
		else {
			this->Visible = true;



			connection = gcnew NpgsqlConnection("server=localhost; Port=5432; User Id=postgres; Password=tomek; Database=postgres");

			/******************************** pobieranie danych pracownika****************************/
		/*	try
			{
				selectcomm = gcnew NpgsqlCommand("SELECT id_pracownika, imie, nazwisko, stanowisko FROM pracownicy WHERE id_pracownika = @id_pracownika", connection);
				selectcomm->Parameters->Add("@id_pracownika", forma->id_pracownika);
				DataSet^ ds = gcnew DataSet("logowanie");
				NpgsqlDataAdapter^ adapter = gcnew NpgsqlDataAdapter(selectcomm);
				adapter->Fill(ds);
				bindingSource1->DataSource = ds->Tables[0];
				DataRowView^ SelectedRow = (DataRowView^)(bindingSource1->Current);
				if (bindingSource1->Current != nullptr)					// czy cokolwiek jest w tabeli
				{
					id_pracownika = Convert::ToInt32(SelectedRow[0]);								// id pracownika
					doPrzekazania->id_operatora = Convert::ToInt32(SelectedRow[0]);
					label31->Text = SelectedRow[1]->ToString() + " " + SelectedRow[2]->ToString();
					doPrzekazania->imie_operatora = SelectedRow[1]->ToString();
					doPrzekazania->nazwisko_operatora = SelectedRow[2]->ToString();
					label32->Text = SelectedRow[3]->ToString();
					doPrzekazania->stanowisko_operatora = SelectedRow[3]->ToString();
				}
			}
			catch (Exception^ e)
			{

			}
			*/
			label31->Text = forma->uzytkownik;
			doPrzekazania->uzytkownik = forma->uzytkownik;
			label32->Text = forma->stanowisko;
			doPrzekazania->stanowisko_operatora = forma->stanowisko;
			/*****************************************************************************************/

			//label30->Text = Convert::ToString(id_pracownika);
			odswiez();
		}
	}
	private: System::Void bindingNavigatorMoveNextItem_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void odswiez() {
		//selectcomm = gcnew NpgsqlCommand("SELECT zlecenia.id_zlecenia, operacje.id_operacji, detale.id_detalu, statusy_detali.status FROM detale LEFT JOIN operacje ON detale.id_operacji = operacje.id_operacji JOIN zlecenia ON operacje.id_zlecenia = zlecenia.id_zlecenia JOIN statusy_detali ON detale.status_detalu = statusy_detali.id_statusu_detalu;", connection);
		try
		{
			selectcomm = gcnew NpgsqlCommand("SELECT nr_serii, id_serii,  rodzaj_klasyfikacji, rodzaj_technologii FROM seria;", connection);
			DataSet^ ds = gcnew DataSet("serie");
			NpgsqlDataAdapter^ adapter = gcnew NpgsqlDataAdapter(selectcomm);
			adapter->Fill(ds);
			bindingSource1->DataSource = ds->Tables[0];
			dataGridView1->DataSource = bindingSource1;
			dataGridView1->Columns[1]->Visible = false;
			dataGridView1->Columns[2]->Visible = false;
			dataGridView1->Columns[3]->Visible = false;
		}
		catch (Exception^ e)
		{

		}
		

		
	}
private: System::Void dataGridView1_SelectionChanged(System::Object^  sender, System::EventArgs^  e) {
	klasyfikacja_i_technologia();
	try
	{
		if (bindingSource1->Current != nullptr)					// czy cokolwiek jest w tabeli
		{
			selectcomm_g2 = gcnew NpgsqlCommand("SELECT DISTINCT kat_operacji.nr_operacji FROM public.operacje, public.kat_operacji, public.detale WHERE kat_operacji.id_kat_operacji = operacje.id_kat_operacji AND detale.id_detalu = operacje.id_detalu AND detale.id_serii = @id_serii AND kat_operacji.id_stanowiska = @id_stanowiska;", connection);
			selectcomm_g2->Parameters->Clear();
			selectcomm_g2->Parameters->Add("@id_serii", dataGridView1->CurrentRow->Cells[1]->Value);
			Obrabiarka^ infoObrabiarki = gcnew Obrabiarka;
			selectcomm_g2->Parameters->Add("@id_stanowiska", infoObrabiarki->id_obrabiarki);
			DataSet^ ds2 = gcnew DataSet("serie1");
			NpgsqlDataAdapter^ adapter2 = gcnew NpgsqlDataAdapter(selectcomm_g2);
			adapter2->Fill(ds2);
			bindingSource2->DataSource = ds2->Tables[0];
			dataGridView2->DataSource = bindingSource2;
			dataGridView2->Columns[0]->Width = 177;// dataGridView1->Columns[0]->Width;
		}
	}
	catch (Exception^ e)
	{

	}
}
private: System::Void dataGridView2_SelectionChanged(System::Object^  sender, System::EventArgs^  e) {
	try
	{
		if (bindingSource2->Current != nullptr)					// czy cokolwiek jest w tabeli
		{
			selectcomm_g3 = gcnew NpgsqlCommand("SELECT detale.nr_detalu, statusy_operacji.status,  operacje.id_operacji, operacje.komentarz_operatora FROM public.operacje, public.kat_operacji, public.detale, public.statusy_operacji WHERE kat_operacji.id_kat_operacji = operacje.id_kat_operacji AND detale.id_detalu = operacje.id_detalu AND  statusy_operacji.id_statusu_detalu = detale.status_detalu AND kat_operacji.nr_operacji = @nr_operacji;", connection);
			selectcomm_g3->Parameters->Clear();
			selectcomm_g3->Parameters->Add("@nr_operacji", dataGridView2->CurrentRow->Cells[0]->Value);
			DataSet^ ds3 = gcnew DataSet("serie2");
			NpgsqlDataAdapter^ adapter3 = gcnew NpgsqlDataAdapter(selectcomm_g3);
			adapter3->Fill(ds3);
			bindingSource3->DataSource = ds3->Tables[0];
			dataGridView3->DataSource = bindingSource3;
			dataGridView3->Columns[0]->Width = 177;// dataGridView1->Columns[0]->Width;
			dataGridView3->Columns[1]->Width = 350;
			dataGridView3->Columns[2]->Visible = false;
			dataGridView3->Columns[3]->Visible = false;
		}
		else
		{
			dataGridView3->ClearSelection();
		}
	}
	catch (Exception^ e)
	{
		MessageBox::Show("error");
	}
	
}


private: System::Void rysuj_kolo() {
	Graphics^ g1 = this->CreateGraphics();
	SolidBrush^ pioro = gcnew SolidBrush(System::Drawing::Color::White);
	Pen^ pioro_linia = gcnew Pen(System::Drawing::Color::White);
	Point^ punkt;
	punkt = label30->Location;
	//label30->Text = "11415";
	g1->FillPie(pioro, System::Convert::ToInt16(punkt->X) - 25, System::Convert::ToInt16(punkt->Y) + 2, 20, 20, 0, 360);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8, System::Convert::ToInt16(punkt->Y) + 2 + 8, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20 + label30->Width, System::Convert::ToInt16(punkt->Y) + 8 + 20);

	punkt = label31->Location;
	//label31->Text = "Tomasz Kapeluszny";
	g1->FillPie(pioro, System::Convert::ToInt16(punkt->X) - 25, System::Convert::ToInt16(punkt->Y) + 2, 20, 20, 0, 360);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8, System::Convert::ToInt16(punkt->Y) + 2 + 8, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20 + label31->Width, System::Convert::ToInt16(punkt->Y) + 8 + 20);

	//label32->Text = "Kierownik";
	punkt = label32->Location;
	g1->FillPie(pioro, System::Convert::ToInt16(punkt->X) - 25, System::Convert::ToInt16(punkt->Y) + 2, 20, 20, 0, 360);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8, System::Convert::ToInt16(punkt->Y) + 2 + 8, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20 + label32->Width, System::Convert::ToInt16(punkt->Y) + 8 + 20);

	//label33->Text = "Status aktualny";
	punkt = label33->Location;
	g1->FillPie(pioro, System::Convert::ToInt16(punkt->X) - 25, System::Convert::ToInt16(punkt->Y) + 2, 20, 20, 0, 360);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8, System::Convert::ToInt16(punkt->Y) + 2 + 8, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20 + label33->Width, System::Convert::ToInt16(punkt->Y) + 8 + 20);
}
private: System::Void MyForm2_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
	rysuj_kolo();
}
private: System::Void okno_pomiarow() {
	
	MyForm^ noweokienko = gcnew MyForm();
	doPrzekazania->id_operacji = Convert::ToInt32(dataGridView3->CurrentRow->Cells[2]->Value);
	noweokienko->odbierane = doPrzekazania;
	noweokienko->Show();
}
private: System::Void button1_Click_1(System::Object^  sender, System::EventArgs^  e) {
	System::Windows::Forms::DialogResult odp = MessageBox::Show("Nr serii:      " + dataGridView1->CurrentRow->Cells[0]->Value->ToString() + Environment::NewLine + Environment::NewLine + "Nr operacji:      " + dataGridView2->CurrentRow->Cells[0]->Value->ToString() + Environment::NewLine + Environment::NewLine + "Nr detalu:              " + dataGridView3->CurrentRow->Cells[0]->Value->ToString(), "Rozpocz�� operacje?", MessageBoxButtons::YesNo, MessageBoxIcon::Question, MessageBoxDefaultButton::Button2);
	switch (odp)
	{
	case System::Windows::Forms::DialogResult::Yes:

		okno_pomiarow();
		break;
	case System::Windows::Forms::DialogResult::No:
		break;
	default:
		break;
	}
	odswiez();
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	System::Windows::Forms::DialogResult odp = MessageBox::Show("Nr operacji:      " + dataGridView1->CurrentRow->Cells[0]->Value->ToString() + Environment::NewLine + Environment::NewLine + "Nr detalu:      " + dataGridView2->CurrentRow->Cells[0]->Value->ToString() + Environment::NewLine + Environment::NewLine + "Status:              " + dataGridView2->CurrentRow->Cells[1]->Value->ToString(), "Zako�czy� operacje?", MessageBoxButtons::YesNo, MessageBoxIcon::Question, MessageBoxDefaultButton::Button2);
	switch (odp)
	{
	case System::Windows::Forms::DialogResult::Yes:
		if (dataGridView2->CurrentRow->Cells["start_detalu"]->Value->ToString() != "" &&  dataGridView2->CurrentRow->Cells["stop_detalu"]->Value->ToString() == "")
		{
			updacom = gcnew NpgsqlCommand("UPDATE detale SET stop_detalu = NOW(), status_detalu = 6 WHERE id_detalu = @id_detalu", connection);
			updacom->Parameters->Clear();
			updacom->Parameters->Add("@id_detalu", dataGridView2->CurrentRow->Cells["id_detalu"]->Value);
			try
			{
				connection->Open();
				transaction = connection->BeginTransaction();
				updacom->Transaction = transaction;
				updacom->ExecuteNonQuery();
				transaction->Commit();
				connection->Close();
				//MyForm^ main_form = gcnew MyForm();
				//main_form->Show();
			}
			catch (Exception^ e)
			{
				transaction->Rollback();
				MessageBox::Show(e->Message, "B��d", MessageBoxButtons::OK, MessageBoxIcon::Error);
			}
		}
		
		break;
	case System::Windows::Forms::DialogResult::No:
		break;
	default:
		break;
	}
	odswiez();
}
private: System::Void dataGridView3_SelectionChanged(System::Object^  sender, System::EventArgs^  e) {
	textBox10->Clear();
	textBox10->AppendText("Komunikat:" + Environment::NewLine + dataGridView3->CurrentRow->Cells[3]->Value->ToString());
}
private: System::Void klasyfikacja_i_technologia() {
	String^ klasyfikacja;
	String^ technologia;

	if (Convert::ToBoolean(dataGridView1->CurrentRow->Cells[2]->Value) == true)
	{
		klasyfikacja = "wojskowa";
	}
	else
	{
		klasyfikacja = "cywilna";
	}
	if (Convert::ToBoolean(dataGridView1->CurrentRow->Cells[3]->Value) == true)
	{
		technologia = "wojskowa";
	}
	else
	{
		technologia = "cywilna";
	}
	textBox11->Text = "Klasyfikacja: " + klasyfikacja + Environment::NewLine + "Technologia: " + technologia;
	doPrzekazania->klas_export = textBox11->Text;
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
}
};

}
