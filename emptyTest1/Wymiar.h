#pragma once

namespace Wymiar1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Podsumowanie informacji o Wymiar
	/// </summary>
	public ref class Wymiar : public System::Windows::Forms::Form
	{
	private:
		int i = 0;

		Pen^ pioro;
		Double CL = 0;
		Double UCL = 0;
		Double LCL = 0;
		Double sumaMR = 0;
		Double srMR = 0;
		Double suma = 0;
		Double MR = 0;
		array<Double>^ tabMR = gcnew array<Double>(1);
		Int32 licznik;
		Graphics^ g1;
		Graphics^ g2;
		SolidBrush^ pedzel = gcnew SolidBrush(Color::Black);
		Drawing::Font^ czcionka = gcnew Drawing::Font(FontFamily::GenericSansSerif, 10, FontStyle::Regular);


		Pen^ pioro2;
		Double CL2 = 0;
		Double UCL2 = 0;
		Double LCL2 = 0;
		Double sumaMR2 = 0;
		Double srMR2 = 0;
		Double suma2 = 0;
		Double MR2 = 0;
		array<Double>^ tabMR2 = gcnew array<Double>(1);
		Int32 licznik2;
		Graphics^ g3;
		Graphics^ g4;
		SolidBrush^ pedzel2 = gcnew SolidBrush(Color::Black);
		Drawing::Font^ czcionka2 = gcnew Drawing::Font(FontFamily::GenericSansSerif, 10, FontStyle::Regular);

		bool start = true;

	public:
		Image^ karta;
		Image^ karta2;
		Image^ karta3;
		Image^ karta4;
		String^ nazwa;
		String^ nr;
		Double wartosc;
		Double tolgorna;
		Double toldolna;
		array<Double>^ pomiary = gcnew array<Double>(0);
		array<Double>^ pomiary2 = gcnew array<Double>(0);
		array<Double>^ temperatury = gcnew array<Double>(0);
		Double Cpk;
		Double Cp;
		String^ urzadzenie;

		static int trend = 0;
		static int kolejne = 0;
		static int strefaA = 0;
		static int zakresstrefaA = 0;
		static int strefaB = 0;
		static int zakresstrefaB = 0;
		static int zakres = 0;

		int nr_urzadzenia;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::TabControl^  tabControl1;
	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::TabPage^  tabPage2;
	private: System::Windows::Forms::PictureBox^  pictureBox3;
	private: System::Windows::Forms::PictureBox^  pictureBox4;
	private: System::Windows::Forms::DataGridView^  dataGridView2;


	private: System::Windows::Forms::DataGridView^  dataGridView1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column2;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::PictureBox^  pictureBox5;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::PictureBox^  pictureBox6;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;

	private: System::Windows::Forms::SplitContainer^  splitContainer1;
	public:



		Wymiar(void)
		{
			InitializeComponent();
			//
			//TODO: W tym miejscu dodaj kod konstruktora
			//	
			pioro = gcnew Pen(Color::Blue);
			pioro2 = gcnew Pen(Color::Blue);
			licznik = 10;

		}

	protected:
		/// <summary>
		/// Wyczy�� wszystkie u�ywane zasoby.
		/// </summary>
		~Wymiar()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:

	private:
		/// <summary>
		/// Wymagana zmienna projektanta.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Wymagana metoda obs�ugi projektanta � nie nale�y modyfikowa� 
		/// zawarto�� tej metody z edytorem kodu.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle6 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->pictureBox5 = (gcnew System::Windows::Forms::PictureBox());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->dataGridView2 = (gcnew System::Windows::Forms::DataGridView());
			this->dataGridViewTextBoxColumn1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->pictureBox6 = (gcnew System::Windows::Forms::PictureBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->Column1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox4 = (gcnew System::Windows::Forms::PictureBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->BeginInit();
			this->splitContainer1->Panel1->SuspendLayout();
			this->splitContainer1->Panel2->SuspendLayout();
			this->splitContainer1->SuspendLayout();
			this->tabControl1->SuspendLayout();
			this->tabPage1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			this->tabPage2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Location = System::Drawing::Point(8, 102);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(840, 774);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox1->TabIndex = 1;
			this->pictureBox1->TabStop = false;
			// 
			// splitContainer1
			// 
			this->splitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer1->FixedPanel = System::Windows::Forms::FixedPanel::Panel2;
			this->splitContainer1->IsSplitterFixed = true;
			this->splitContainer1->Location = System::Drawing::Point(0, 0);
			this->splitContainer1->Name = L"splitContainer1";
			this->splitContainer1->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this->splitContainer1->Panel1->AutoScroll = true;
			this->splitContainer1->Panel1->Controls->Add(this->tabControl1);
			// 
			// splitContainer1.Panel2
			// 
			this->splitContainer1->Panel2->Controls->Add(this->button2);
			this->splitContainer1->Panel2->Controls->Add(this->button1);
			this->splitContainer1->Panel2MinSize = 50;
			this->splitContainer1->Size = System::Drawing::Size(1518, 733);
			this->splitContainer1->SplitterDistance = 679;
			this->splitContainer1->TabIndex = 2;
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tabControl1->DrawMode = System::Windows::Forms::TabDrawMode::OwnerDrawFixed;
			this->tabControl1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->tabControl1->Location = System::Drawing::Point(0, 0);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(1518, 679);
			this->tabControl1->TabIndex = 3;
			this->tabControl1->DrawItem += gcnew System::Windows::Forms::DrawItemEventHandler(this, &Wymiar::tabControl1_DrawItem);
			this->tabControl1->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Wymiar::tabControl1_KeyDown);
			// 
			// tabPage1
			// 
			this->tabPage1->AutoScroll = true;
			this->tabPage1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(38)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)));
			this->tabPage1->Controls->Add(this->label4);
			this->tabPage1->Controls->Add(this->pictureBox5);
			this->tabPage1->Controls->Add(this->textBox1);
			this->tabPage1->Controls->Add(this->dataGridView2);
			this->tabPage1->Controls->Add(this->textBox2);
			this->tabPage1->Controls->Add(this->pictureBox2);
			this->tabPage1->Controls->Add(this->label3);
			this->tabPage1->Controls->Add(this->label2);
			this->tabPage1->Controls->Add(this->pictureBox1);
			this->tabPage1->Controls->Add(this->label1);
			this->tabPage1->ForeColor = System::Drawing::Color::White;
			this->tabPage1->Location = System::Drawing::Point(4, 29);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(1510, 646);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"Wymiary skompensowane";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->BackColor = System::Drawing::Color::Red;
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 36, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label4->Location = System::Drawing::Point(739, 0);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(251, 55);
			this->label4->TabIndex = 11;
			this->label4->Text = L"ALARM !!!";
			this->label4->Visible = false;
			// 
			// pictureBox5
			// 
			this->pictureBox5->Location = System::Drawing::Point(379, 67);
			this->pictureBox5->Name = L"pictureBox5";
			this->pictureBox5->Size = System::Drawing::Size(24, 26);
			this->pictureBox5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox5->TabIndex = 10;
			this->pictureBox5->TabStop = false;
			// 
			// textBox1
			// 
			this->textBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox1->Location = System::Drawing::Point(862, 63);
			this->textBox1->Name = L"textBox1";
			this->textBox1->ReadOnly = true;
			this->textBox1->Size = System::Drawing::Size(149, 29);
			this->textBox1->TabIndex = 9;
			this->textBox1->TabStop = false;
			// 
			// dataGridView2
			// 
			this->dataGridView2->AllowUserToAddRows = false;
			this->dataGridView2->AllowUserToDeleteRows = false;
			this->dataGridView2->AllowUserToResizeColumns = false;
			this->dataGridView2->AllowUserToResizeRows = false;
			this->dataGridView2->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView2->BackgroundColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(38)), static_cast<System::Int32>(static_cast<System::Byte>(57)));
			dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(159)),
				static_cast<System::Int32>(static_cast<System::Byte>(199)));
			dataGridViewCellStyle1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			dataGridViewCellStyle1->ForeColor = System::Drawing::Color::White;
			dataGridViewCellStyle1->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle1->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle1->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dataGridView2->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this->dataGridView2->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView2->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(2) {
				this->dataGridViewTextBoxColumn1,
					this->Column2
			});
			dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle2->BackColor = System::Drawing::SystemColors::Window;
			dataGridViewCellStyle2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			dataGridViewCellStyle2->ForeColor = System::Drawing::Color::White;
			dataGridViewCellStyle2->SelectionBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)), static_cast<System::Int32>(static_cast<System::Byte>(121)));
			dataGridViewCellStyle2->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle2->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->dataGridView2->DefaultCellStyle = dataGridViewCellStyle2;
			this->dataGridView2->Dock = System::Windows::Forms::DockStyle::Right;
			this->dataGridView2->EnableHeadersVisualStyles = false;
			this->dataGridView2->GridColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(38)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)));
			this->dataGridView2->Location = System::Drawing::Point(1686, 3);
			this->dataGridView2->MultiSelect = false;
			this->dataGridView2->Name = L"dataGridView2";
			this->dataGridView2->ReadOnly = true;
			this->dataGridView2->RowHeadersVisible = false;
			this->dataGridView2->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::DisableResizing;
			dataGridViewCellStyle3->BackColor = System::Drawing::Color::White;
			dataGridViewCellStyle3->ForeColor = System::Drawing::Color::Black;
			dataGridViewCellStyle3->SelectionBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)), static_cast<System::Int32>(static_cast<System::Byte>(121)));
			dataGridViewCellStyle3->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			this->dataGridView2->RowsDefaultCellStyle = dataGridViewCellStyle3;
			this->dataGridView2->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView2->Size = System::Drawing::Size(186, 870);
			this->dataGridView2->TabIndex = 7;
			this->dataGridView2->TabStop = false;
			// 
			// dataGridViewTextBoxColumn1
			// 
			this->dataGridViewTextBoxColumn1->FillWeight = 121.8274F;
			this->dataGridViewTextBoxColumn1->HeaderText = L"Pomiar";
			this->dataGridViewTextBoxColumn1->Name = L"dataGridViewTextBoxColumn1";
			this->dataGridViewTextBoxColumn1->ReadOnly = true;
			// 
			// Column2
			// 
			this->Column2->FillWeight = 78.17259F;
			this->Column2->HeaderText = L"Temp";
			this->Column2->Name = L"Column2";
			this->Column2->ReadOnly = true;
			// 
			// textBox2
			// 
			this->textBox2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox2->Location = System::Drawing::Point(1473, 63);
			this->textBox2->Name = L"textBox2";
			this->textBox2->ReadOnly = true;
			this->textBox2->Size = System::Drawing::Size(149, 29);
			this->textBox2->TabIndex = 9;
			this->textBox2->TabStop = false;
			// 
			// pictureBox2
			// 
			this->pictureBox2->Location = System::Drawing::Point(849, 102);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(840, 774);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox2->TabIndex = 3;
			this->pictureBox2->TabStop = false;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label3->ForeColor = System::Drawing::SystemColors::Control;
			this->label3->Location = System::Drawing::Point(34, 65);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(330, 25);
			this->label3->TabIndex = 8;
			this->label3->Text = L"Zdolno�� procesu produkcyjnego";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label2->ForeColor = System::Drawing::SystemColors::Control;
			this->label2->Location = System::Drawing::Point(468, 65);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(380, 25);
			this->label2->TabIndex = 8;
			this->label2->Text = L"Wska�nik wycentrowania procesu Cpk";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label1->ForeColor = System::Drawing::SystemColors::Control;
			this->label1->Location = System::Drawing::Point(1051, 67);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(416, 25);
			this->label1->TabIndex = 8;
			this->label1->Text = L"Wska�nik zdolno�ci (rozrzutu) procesu Cp";
			// 
			// tabPage2
			// 
			this->tabPage2->AutoScroll = true;
			this->tabPage2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(38)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)));
			this->tabPage2->Controls->Add(this->label5);
			this->tabPage2->Controls->Add(this->pictureBox6);
			this->tabPage2->Controls->Add(this->textBox3);
			this->tabPage2->Controls->Add(this->textBox4);
			this->tabPage2->Controls->Add(this->label6);
			this->tabPage2->Controls->Add(this->label7);
			this->tabPage2->Controls->Add(this->label8);
			this->tabPage2->Controls->Add(this->dataGridView1);
			this->tabPage2->Controls->Add(this->pictureBox3);
			this->tabPage2->Controls->Add(this->pictureBox4);
			this->tabPage2->ForeColor = System::Drawing::Color::White;
			this->tabPage2->Location = System::Drawing::Point(4, 29);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(1510, 646);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"Wymiary nieskompensowane";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->BackColor = System::Drawing::Color::Red;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 36, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label5->Location = System::Drawing::Point(739, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(251, 55);
			this->label5->TabIndex = 18;
			this->label5->Text = L"ALARM !!!";
			this->label5->Visible = false;
			// 
			// pictureBox6
			// 
			this->pictureBox6->Location = System::Drawing::Point(379, 67);
			this->pictureBox6->Name = L"pictureBox6";
			this->pictureBox6->Size = System::Drawing::Size(24, 26);
			this->pictureBox6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox6->TabIndex = 17;
			this->pictureBox6->TabStop = false;
			// 
			// textBox3
			// 
			this->textBox3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox3->Location = System::Drawing::Point(862, 63);
			this->textBox3->Name = L"textBox3";
			this->textBox3->ReadOnly = true;
			this->textBox3->Size = System::Drawing::Size(149, 29);
			this->textBox3->TabIndex = 15;
			this->textBox3->TabStop = false;
			// 
			// textBox4
			// 
			this->textBox4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox4->Location = System::Drawing::Point(1473, 63);
			this->textBox4->Name = L"textBox4";
			this->textBox4->ReadOnly = true;
			this->textBox4->Size = System::Drawing::Size(149, 29);
			this->textBox4->TabIndex = 16;
			this->textBox4->TabStop = false;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label6->ForeColor = System::Drawing::SystemColors::Control;
			this->label6->Location = System::Drawing::Point(34, 65);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(330, 25);
			this->label6->TabIndex = 12;
			this->label6->Text = L"Zdolno�� procesu produkcyjnego";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label7->ForeColor = System::Drawing::SystemColors::Control;
			this->label7->Location = System::Drawing::Point(468, 65);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(380, 25);
			this->label7->TabIndex = 13;
			this->label7->Text = L"Wska�nik wycentrowania procesu Cpk";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label8->ForeColor = System::Drawing::SystemColors::Control;
			this->label8->Location = System::Drawing::Point(1051, 67);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(416, 25);
			this->label8->TabIndex = 14;
			this->label8->Text = L"Wska�nik zdolno�ci (rozrzutu) procesu Cp";
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->AllowUserToResizeColumns = false;
			this->dataGridView1->AllowUserToResizeRows = false;
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->BackgroundColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(38)), static_cast<System::Int32>(static_cast<System::Byte>(57)));
			dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle4->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(159)),
				static_cast<System::Int32>(static_cast<System::Byte>(199)));
			dataGridViewCellStyle4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			dataGridViewCellStyle4->ForeColor = System::Drawing::Color::White;
			dataGridViewCellStyle4->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle4->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle4->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dataGridView1->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(1) { this->Column1 });
			dataGridViewCellStyle5->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle5->BackColor = System::Drawing::Color::White;
			dataGridViewCellStyle5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			dataGridViewCellStyle5->ForeColor = System::Drawing::Color::White;
			dataGridViewCellStyle5->SelectionBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)), static_cast<System::Int32>(static_cast<System::Byte>(121)));
			dataGridViewCellStyle5->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle5->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->dataGridView1->DefaultCellStyle = dataGridViewCellStyle5;
			this->dataGridView1->Dock = System::Windows::Forms::DockStyle::Right;
			this->dataGridView1->EnableHeadersVisualStyles = false;
			this->dataGridView1->GridColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(38)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)));
			this->dataGridView1->Location = System::Drawing::Point(1686, 3);
			this->dataGridView1->MultiSelect = false;
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::DisableResizing;
			dataGridViewCellStyle6->BackColor = System::Drawing::Color::White;
			dataGridViewCellStyle6->ForeColor = System::Drawing::Color::Black;
			dataGridViewCellStyle6->SelectionBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)), static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->dataGridView1->RowsDefaultCellStyle = dataGridViewCellStyle6;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(94, 870);
			this->dataGridView1->TabIndex = 6;
			// 
			// Column1
			// 
			this->Column1->HeaderText = L"Pomiar";
			this->Column1->Name = L"Column1";
			this->Column1->ReadOnly = true;
			// 
			// pictureBox3
			// 
			this->pictureBox3->Location = System::Drawing::Point(849, 102);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(840, 774);
			this->pictureBox3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox3->TabIndex = 5;
			this->pictureBox3->TabStop = false;
			// 
			// pictureBox4
			// 
			this->pictureBox4->Location = System::Drawing::Point(8, 102);
			this->pictureBox4->Name = L"pictureBox4";
			this->pictureBox4->Size = System::Drawing::Size(840, 774);
			this->pictureBox4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox4->TabIndex = 4;
			this->pictureBox4->TabStop = false;
			// 
			// button2
			// 
			this->button2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(57)),
				static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button2->ForeColor = System::Drawing::Color::White;
			this->button2->Location = System::Drawing::Point(1188, 2);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(156, 42);
			this->button2->TabIndex = 1;
			this->button2->TabStop = false;
			this->button2->Text = L"Usu� pomiar";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += gcnew System::EventHandler(this, &Wymiar::button2_Click);
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(57)),
				static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button1->ForeColor = System::Drawing::Color::White;
			this->button1->Location = System::Drawing::Point(1350, 2);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(156, 42);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Zamknij";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &Wymiar::button1_Click);
			// 
			// Wymiar
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
			this->AutoSize = true;
			this->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(38)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)));
			this->ClientSize = System::Drawing::Size(1518, 733);
			this->Controls->Add(this->splitContainer1);
			this->Name = L"Wymiar";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Wymiar";
			this->WindowState = System::Windows::Forms::FormWindowState::Maximized;
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Wymiar::Wymiar_FormClosing);
			this->Load += gcnew System::EventHandler(this, &Wymiar::Wymiar_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->splitContainer1->Panel1->ResumeLayout(false);
			this->splitContainer1->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->EndInit();
			this->splitContainer1->ResumeLayout(false);
			this->tabControl1->ResumeLayout(false);
			this->tabPage1->ResumeLayout(false);
			this->tabPage1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			this->tabPage2->ResumeLayout(false);
			this->tabPage2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	public: void wyswietl(Double pomiar, Double temp)
	{
		label4->Visible = false;
		label5->Visible = false;
		i++;
		if (i > licznik)
		{
			licznik += 10;
		}
		pomiary->Resize(pomiary, i);
		pomiary[i - 1] = pomiar;
		suma += pomiar;
		temperatury->Resize(temperatury, i);
		temperatury[i - 1] = temp;
		dataGridView2->Rows->Add();
		dataGridView2->Rows[i - 1]->Cells[0]->Value = Math::Round(pomiar, 3);
		dataGridView2->Rows[i - 1]->Cells[1]->Value = temp;
		if (i > 1)
		{
			MR = Math::Abs(pomiar - pomiary[i - 2]);
			tabMR->Resize(tabMR, i);
			tabMR[i - 1] = MR;
			sumaMR += MR;
			srMR = sumaMR / (i - 1);
			if (i > 4)
			{
				CL = suma / i;
				
			}
			else
			{
				CL = wartosc;
			}
			LCL = CL - 2.66*srMR;
			UCL = CL + 2.66*srMR;
			karta = Image::FromFile("..\\emptyTest1\\karta.png");
			karta2 = Image::FromFile("..\\emptyTest1\\kartaRUCHOMER.png");
			g1 = Graphics::FromImage(karta);
			g2 = Graphics::FromImage(karta2);
			for (int a = 1; a <= i; a++)
			{
				rysuj(a, licznik);
				podzialka();
			}
			pictureBox1->Image = karta;
			pictureBox2->Image = karta2;
			strefa(pomiary, UCL, LCL, "karty X wymiar�w SKOMPENSOWANYCH");
			strefa(tabMR, 3.27*srMR, 0, "karty ruchome R wymiar�w SKOMPENSOWANYCH");
			trendy(pomiary, "X wymiar�w SKOMPENSOWANYCH");
			trendy2(tabMR, "ruchome R wymiar�w SKOMPENSOWANYCH");
			StrefyA(pomiary, srMR, UCL, LCL, "wymiar�w SKOMPENSOWANYCH");
			StrefyB(pomiary, srMR, UCL, LCL, "wymiar�w SKOMPENSOWANYCH");
			przemiennie(pomiary, "X wymiar�w SKOMPENSOWANYCH");
			przemiennie2(tabMR, "ruchome R SKOMPENSOWANYCH");
			zdolnosc();
		}
	}
	private: void rysuj(Int32 i, Int32 licznik)
	{
		Single x = (i * 700) / licznik;
		Double a;

		if (srMR == 0)
			a = 0.001;
		else
			a = srMR;

		Single y = 300 - (((pomiary[i - 1] - CL) / (2.66*a)) * 150);
		pioro->Width = 6;
		g1->DrawLine(pioro, x - 3, y, x + 3, y);
		g1->DrawLine(pioro, x, y - 3, x, y + 3);
		if (i > 1)
		{
			Single x2 = (i * 700) / licznik;
			Single y2 = 475 - ((tabMR[i - 1] / a) * 100);
			g2->DrawLine(pioro, x2 - 3, y2, x2 + 3, y2);
			g2->DrawLine(pioro, x2, y2 - 3, x2, y2 + 3);
			Single y0 = 300 - (((pomiary[i - 2] - CL) / (2.66*a)) * 150);
			Single x0 = x - (700 / licznik);
			pioro->Width = 1;
			pioro->DashStyle = Drawing2D::DashStyle::Dash;
			g1->DrawLine(pioro, x0, y0, x, y);

			if (i > 2)
			{
				x0 = x2 - (700 / licznik);
				y0 = 475 - ((tabMR[i - 2] / a) * 100);
				g2->DrawLine(pioro, x0, y0, x2, y2);

			}
		}

	}
	private: void podzialka()
	{
		Int32 c;
		float i = Math::Round(CL, 3);
		float j = Math::Round(UCL, 3);
		float k = Math::Round(LCL, 3);
		g1->DrawString("0", czcionka, pedzel, 0, 500);
		g1->DrawString(i.ToString(), czcionka, pedzel, 700, 290);
		g1->DrawString(j.ToString(), czcionka, pedzel, 700, 140);
		g1->DrawString(k.ToString(), czcionka, pedzel, 700, 440);
		g1->DrawString(nazwa, czcionka, pedzel, 350, 50);
		i = Math::Round(srMR, 3);
		j = Math::Round(3.27*srMR, 3);
		g2->DrawString("0", czcionka, pedzel, 0, 500);
		g2->DrawString(i.ToString(), czcionka, pedzel, 700, 365);
		g2->DrawString(j.ToString(), czcionka, pedzel, 700, 140);
		g2->DrawString("0", czcionka, pedzel, 700, 465);
		g2->DrawString(nazwa, czcionka, pedzel, 350, 50);
		for (int b = 1; b < 11; b++)
		{
			c = b*(licznik / 10);
			g1->DrawString(c.ToString(), czcionka, pedzel, (b * 70) - 10, 500);
			g2->DrawString(c.ToString(), czcionka, pedzel, (b * 70) - 10, 500);
		}
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Visible = false;
	}
	private: System::Void Wymiar_Load(System::Object^  sender, System::EventArgs^  e)
	{
		//this->Icon = gcnew System::Drawing::Icon("..\\emptyTest1\\Logo2.ico");
		if (start)
		{
			this->Text = nazwa;
			start = false;
		}
		if (pomiary->Length != 0)
		{
			i = pomiary->Length;
			for (int a = 0; a < i; a++)
			{
				suma += pomiary[a];
				suma2 += pomiary2[a];
				dataGridView2->Rows->Add();
				dataGridView2->Rows[a]->Cells[0]->Value = Math::Round(pomiary[a], 3);
				dataGridView2->Rows[a]->Cells[1]->Value = temperatury[a];
				dataGridView1->Rows->Add();
				dataGridView1->Rows[a]->Cells[0]->Value = Math::Round(pomiary2[a], 3);
			}
			przeliczanie();
			zdolnosc();
		}
	}


	private: System::Void Wymiar_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
		e->Cancel = true;
		this->Visible = false;
	}
	private: System::Void tabControl1_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
		if (e->KeyCode == Keys::Enter)
			this->Visible = false;
	}





	public: void wyswietl2(Double pomiar)
	{
		label4->Visible = false;
		label5->Visible = false;

		if (i > licznik2)
		{
			licznik2 += 10;
		}
		pomiary2->Resize(pomiary2, i);
		pomiary2[i - 1] = pomiar;
		dataGridView1->Rows->Add();
		dataGridView1->Rows[i - 1]->Cells[0]->Value = Math::Round(pomiar, 3);
		suma2 += pomiar;
		if (i > 1)
		{
			MR2 = Math::Abs(pomiar - pomiary2[i - 2]);
			tabMR2->Resize(tabMR2, i);
			tabMR2[i - 1] = MR2;
			sumaMR2 += MR2;
			srMR2 = sumaMR2 / (i - 1);
			if (i > 4)
			{
				CL2 = suma2 / i;

			}
			else
			{
				CL2 = wartosc;
			}
			LCL2 = CL2 - 2.66*srMR2;
			UCL2 = CL2 + 2.66*srMR2;
			karta3 = Image::FromFile("..\\emptyTest1\\karta.png");
			karta4 = Image::FromFile("..\\emptyTest1\\kartaRUCHOMER.png");
			g3 = Graphics::FromImage(karta3);
			g4 = Graphics::FromImage(karta4);
			for (int a = 1; a <= i; a++)
			{
				rysuj2(a, licznik2);
				podzialka2();
			}
			pictureBox4->Image = karta3;
			pictureBox3->Image = karta4;
			strefa(pomiary2, UCL2, LCL2, "karty X wymiar�w NIESKOMPENSOWANYCH");
			strefa(tabMR2, 3.27*srMR2, 0, "karty ruchome R wymiar�w NIESKOMPENSOWANYCH");
			trendy(pomiary2, "X wymiar�w NIESKOMPENSOWANYCH");
			trendy2(tabMR2, "ruchome R wymiar�w NIESKOMPENSOWANYCH");
			StrefyA(pomiary2, srMR2, UCL2, LCL2, "wymiar�w NIESKOMPENSOWANYCH");
			StrefyB(pomiary2, srMR2, UCL2, LCL2, "wymiar�w NIESKOMPENSOWANYCH");
			przemiennie(pomiary2, "X wymiar�w NIESKOMPENSOWANYCH");
			przemiennie2(tabMR2, "ruchome R wymiar�w NIESKOMPENSOWANYCH");
			zdolnosc();
		}
	}
	private: void rysuj2(Int32 i, Int32 licznik)
	{
		Single x = (i * 700) / licznik;
		Double a;

		if (srMR2 == 0)
			a = 0.001;
		else
			a = srMR2;

		Single y = 300 - (((pomiary2[i - 1] - CL2) / (2.66*a)) * 150);
		pioro2->Width = 6;
		g3->DrawLine(pioro2, x - 3, y, x + 3, y);
		g3->DrawLine(pioro2, x, y - 3, x, y + 3);
		if (i > 1)
		{
			Single x2 = (i * 700) / licznik;
			Single y2 = 475 - ((tabMR2[i - 1] / a) * 100);
			g4->DrawLine(pioro2, x2 - 3, y2, x2 + 3, y2);
			g4->DrawLine(pioro2, x2, y2 - 3, x2, y2 + 3);
			Single y0 = 300 - (((pomiary2[i - 2] - CL2) / (2.66*a)) * 150);
			Single x0 = x - (700 / licznik);
			pioro2->Width = 1;
			pioro2->DashStyle = Drawing2D::DashStyle::Dash;
			g3->DrawLine(pioro, x0, y0, x, y);

			if (i > 2)
			{
				x0 = x2 - (700 / licznik);
				y0 = 475 - ((tabMR2[i - 2] / a) * 100);
				g4->DrawLine(pioro2, x0, y0, x2, y2);

			}
		}

	}
	private: void podzialka2()
	{
		Int32 c;
		float i = Math::Round(CL2, 3);
		float j = Math::Round(UCL2, 3);
		float k = Math::Round(LCL2, 3);
		g3->DrawString("0", czcionka, pedzel, 0, 500);
		g3->DrawString(i.ToString(), czcionka, pedzel, 700, 290);
		g3->DrawString(j.ToString(), czcionka, pedzel, 700, 140);
		g3->DrawString(k.ToString(), czcionka, pedzel, 700, 440);
		g3->DrawString(nazwa, czcionka, pedzel, 350, 50);
		i = Math::Round(srMR2, 3);
		j = Math::Round(3.27*srMR2, 3);
		g4->DrawString("0", czcionka, pedzel, 0, 500);
		g4->DrawString(i.ToString(), czcionka, pedzel, 700, 365);
		g4->DrawString(j.ToString(), czcionka, pedzel, 700, 140);
		g4->DrawString("0", czcionka, pedzel, 700, 465);
		g4->DrawString(nazwa, czcionka, pedzel, 350, 50);
		for (int b = 1; b < 11; b++)
		{
			c = b*(licznik2 / 10);
			g3->DrawString(c.ToString(), czcionka, pedzel, (b * 70) - 10, 500);
			g4->DrawString(c.ToString(), czcionka, pedzel, (b * 70) - 10, 500);
		}
	}

			 void strefa(array<Double>^ tablica, Double gora, Double dol, String^ tekst)
			 {

				 for each (Double var in tablica)
				 {
					 if ((var>gora) || (var<dol))
					 {
						 label4->Visible = true;
						 label5->Visible = true;
						 MessageBox::Show("Pomiar przekroczy� granice " + tekst, "Alarm", MessageBoxButtons::OK, MessageBoxIcon::Warning);
						 break;
					 }
				 }
			 }

	private: void trendy(array<Double>^ tablica, String^ tekst)
	{
		if (trend != 0)
		{
			int sprawdzenie = 0;
			if (tablica->Length >= trend)
			{
				for (int k = tablica->Length - trend; k < tablica->Length - 1; k++)
				{
					if (tablica[k] < tablica[k + 1])
						sprawdzenie += 1;
					if (tablica[k] > tablica[k + 1])
						sprawdzenie -= 1;
				}
				if (sprawdzenie == trend - 1)
				{
					label4->Visible = true;
					label5->Visible = true;
					MessageBox::Show("Wykryto trend rosn�cy w karcie " + tekst, "Alarm", MessageBoxButtons::OK, MessageBoxIcon::Warning);
				}
				if (sprawdzenie == 1 - trend)
				{
					label4->Visible = true;
					label5->Visible = true;
					MessageBox::Show("Wykryto trend malej�cy w karcie " + tekst, "Alarm", MessageBoxButtons::OK, MessageBoxIcon::Warning);
				}
				sprawdzenie = 0;
			}
		}
	}


	private: void trendy2(array<Double>^ tablica, String^ tekst)
	{
		if (trend != 0)
		{
			int sprawdzenie = 0;
			if (tablica->Length >= trend + 1)
			{
				for (int k = tablica->Length - trend; k < tablica->Length - 1; k++)
				{
					if (tablica[k] < tablica[k + 1])
						sprawdzenie += 1;
					if (tablica[k] > tablica[k + 1])
						sprawdzenie -= 1;
				}
				if (sprawdzenie == trend - 1)
				{
					label4->Visible = true;
					label5->Visible = true;
					MessageBox::Show("Wykryto trend rosn�cy w karcie " + tekst, "Alarm", MessageBoxButtons::OK, MessageBoxIcon::Warning);
				}
				if (sprawdzenie == 1 - trend)
				{
					label4->Visible = true;
					label5->Visible = true;
					MessageBox::Show("Wykryto trend malej�cy w karcie " + tekst, "Alarm", MessageBoxButtons::OK, MessageBoxIcon::Warning);
				}
				sprawdzenie = 0;
			}
		}
	}

			 void StrefyA(array<Double>^ tablica, Double sredniaMr, Double gora, Double dol, String^ tekst)
			 {
				 if (zakresstrefaA != 0)
				 {
					 int sprawdzenie = 0;
					 if (tablica->Length >= zakresstrefaA)
					 {
						 for (int k = tablica->Length - 1; k >= zakresstrefaA - 1; k--)
						 {
							 for (int j = k; j >= k + 1 - zakresstrefaA; j--)
							 {
								 if ((tablica[j] > (gora - 0.886667*sredniaMr)) && (tablica[j] < gora))
									 sprawdzenie += 1;
								 if ((tablica[j] < (dol + 0.886667*sredniaMr)) && (tablica[j] > dol))
									 sprawdzenie -= 1;
							 }
							 if (Math::Abs(sprawdzenie) >= strefaA)
							 {
								 label4->Visible = true;
								 label5->Visible = true;
								 MessageBox::Show("Wykryto " + strefaA.ToString() + " spo�r�d " + zakresstrefaA.ToString() + " punkt�w w strefie A w karcie X " + tekst, "Alarm", MessageBoxButtons::OK, MessageBoxIcon::Warning);
								 break;
							 }
							 sprawdzenie = 0;
						 }

					 }
				 }
			 }

			 void StrefyB(array<Double>^ tablica, Double sredniaMr, Double gora, Double dol, String^ tekst)
			 {
				 if (zakresstrefaB != 0)
				 {
					 int sprawdzenie = 0;
					 if (tablica->Length >= zakresstrefaB)
					 {
						 for (int k = tablica->Length - 1; k >= zakresstrefaB - 1; k--)
						 {
							 for (int j = k; j >= k + 1 - zakresstrefaB; j--)
							 {
								 if (tablica[j] > (gora - 1.773334*sredniaMr))
									 sprawdzenie += 1;
								 if (tablica[j] < (dol + 1.773334*sredniaMr))
									 sprawdzenie -= 1;
							 }
							 if (Math::Abs(sprawdzenie) >= strefaB)
							 {
								 label4->Visible = true;
								 label5->Visible = true;
								 MessageBox::Show("Wykryto " + strefaB.ToString() + " spo�r�d " + zakresstrefaB.ToString() + " punkt�w w strefie B lub poza ni� w karcie X " + tekst, "Alarm", MessageBoxButtons::OK, MessageBoxIcon::Warning);
								 break;
							 }
							 sprawdzenie = 0;
						 }

					 }
				 }
			 }

	private: void przemiennie(array<Double>^ tablica, String^ tekst)
	{
		if (zakres != 0)
		{
			int sprawdzenie = 0;
			if (tablica->Length > zakres)
			{
				for (int k = tablica->Length - zakres; k < tablica->Length - 1; k++)
				{
					if (tablica[k] < tablica[k + 1])
					{
						if (tablica[k] < tablica[k - 1])
							sprawdzenie += 1;
					}

					if (tablica[k] > tablica[k + 1])
					{
						if (tablica[k] > tablica[k - 1])
							sprawdzenie += 1;
					}
				}
				if (sprawdzenie == zakres - 1)
				{
					label4->Visible = true;
					label5->Visible = true;
					MessageBox::Show("Wykryto " + zakres.ToString() + " punkt�w przemiennie rosn�cych i malej�cych w karcie " + tekst, "Alarm", MessageBoxButtons::OK, MessageBoxIcon::Warning);
				}
				sprawdzenie = 0;
			}
		}
	}

	private: void przemiennie2(array<Double>^ tablica, String^ tekst)
	{
		if (zakres != 0)
		{
			int sprawdzenie = 0;
			if (tablica->Length > zakres + 1)
			{
				for (int k = tablica->Length - zakres; k < tablica->Length - 1; k++)
				{
					if (tablica[k] < tablica[k + 1])
					{
						if (tablica[k] < tablica[k - 1])
							sprawdzenie += 1;
					}

					if (tablica[k] > tablica[k + 1])
					{
						if (tablica[k] > tablica[k - 1])
							sprawdzenie += 1;
					}
				}
				if (sprawdzenie == zakres - 1)
				{
					label4->Visible = true;
					label5->Visible = true;
					MessageBox::Show("Wykryto " + zakres.ToString() + " punkt�w przemiennie rosn�cych i malej�cych w karcie " + tekst, "Alarm", MessageBoxButtons::OK, MessageBoxIcon::Warning);
				}
				sprawdzenie = 0;
			}
		}
	}

	private: void zdolnosc()
	{
		if (i > 5)
		{
			Double sumaC;
			for each (Double var in pomiary)
			{
				sumaC += var;
			}
			Double srC = sumaC / i;
			Double kwadraty = 0;
			for each (Double var in pomiary)
			{
				kwadraty += Math::Pow((srC - var), 2);
			}
			Double fi = Math::Sqrt(kwadraty / i);
			Cp = Math::Round((tolgorna - toldolna) / (6 * fi), 3);
			Double Cpl = (srC - toldolna) / (3 * fi);
			Double Cpu = (tolgorna - srC) / (3 * fi);
			Cpk = Math::Round(Math::Min(Cpl, Cpu), 3);
			textBox2->Text = Cp.ToString();
			textBox1->Text = Cpk.ToString();
			textBox4->Text = Cp.ToString();
			textBox3->Text = Cpk.ToString();

			if ((Cp > 1) && (Cpk > 1))
			{
				if ((Cp > 0.85*Cpk) && (Cp < 1.15*Cpk))
				{
					pictureBox5->Image = Image::FromFile("..\\emptyTest1\\zielone.png");
					pictureBox6->Image = Image::FromFile("..\\emptyTest1\\zielone.png");
				}
				else
				{
					pictureBox5->Image = Image::FromFile("..\\emptyTest1\\czerwone.png");
					pictureBox6->Image = Image::FromFile("..\\emptyTest1\\czerwone.png");
				}
			}
			else
			{
				pictureBox5->Image = Image::FromFile("..\\emptyTest1\\czerwone.png");
				pictureBox6->Image = Image::FromFile("..\\emptyTest1\\czerwone.png");
			}
		}
		else
		{
			textBox2->Text = "";
			textBox1->Text = "";
			textBox4->Text = "";
			textBox3->Text = "";
		}
	}

	private: System::Void tabControl1_DrawItem(System::Object^  sender, System::Windows::Forms::DrawItemEventArgs^  e) {
		e->Graphics->FillRectangle(gcnew SolidBrush(System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(38)),
			static_cast<System::Int32>(static_cast<System::Byte>(57)))), e->Bounds);
		// Then draw the current tab button text 
		Rectangle paddedBounds = e->Bounds;
		paddedBounds.Inflate(-2, -2);
		e->Graphics->DrawString(tabControl1->TabPages[e->Index]->Text, tabControl1->Font, SystemBrushes::HighlightText, paddedBounds);

		Rectangle^ lasttabrect = tabControl1->GetTabRect(tabControl1->TabPages->Count - 1);
		RectangleF emptyspacerect = RectangleF(
			lasttabrect->X + lasttabrect->Width + tabControl1->Left,
			tabControl1->Top + lasttabrect->Y,
			tabControl1->Width - (lasttabrect->X + lasttabrect->Width),
			lasttabrect->Height);

		e->Graphics->FillRectangle(gcnew SolidBrush(System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(57)),
			static_cast<System::Int32>(static_cast<System::Byte>(121)))), emptyspacerect);
	}
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		int wiersz;
		if (dataGridView2->SelectedRows[0] != nullptr)
		{
			wiersz = dataGridView2->SelectedRows[0]->Index;
		}
		else if (dataGridView1->SelectedRows[0] != nullptr)
		{
			wiersz = dataGridView1->SelectedRows[0]->Index;
		}
		dataGridView2->Rows->Remove(dataGridView2->Rows[wiersz]);
		dataGridView1->Rows->Remove(dataGridView1->Rows[wiersz]);
		suma -= pomiary[wiersz];
		suma2 -= pomiary2[wiersz];
		i--;
		pomiary->Copy(pomiary, wiersz + 1, pomiary, wiersz, pomiary->Length - (wiersz + 1));
		pomiary2->Copy(pomiary2, wiersz + 1, pomiary2, wiersz, pomiary2->Length - (wiersz + 1));
		pomiary->Resize(pomiary, i);
		pomiary2->Resize(pomiary2, i);
		temperatury->Copy(temperatury, wiersz + 1, temperatury, wiersz, temperatury->Length - (wiersz + 1));
		temperatury->Resize(temperatury, i);
		przeliczanie();
		zdolnosc();
	}


			 void przeliczanie()
			 {
				 sumaMR = 0;
				 if (i > licznik)
				 {
					 licznik += 10;
				 }
				 if (i > 1)
				 {
					 for (int b = 1; b < i; b++)
					 {
						 MR = Math::Abs(pomiary[b] - pomiary[b - 1]);
						 tabMR->Resize(tabMR, b + 1);
						 tabMR[b] = MR;
						 sumaMR += MR;
					 }
					 srMR = sumaMR / (i - 1);
					 CL = suma / i;
					 LCL = CL - 2.66*srMR;
					 UCL = CL + 2.66*srMR;
					 karta = Image::FromFile("..\\emptyTest1\\karta.png");
					 karta2 = Image::FromFile("..\\emptyTest1\\kartaRUCHOMER.png");
					 g1 = Graphics::FromImage(karta);
					 g2 = Graphics::FromImage(karta2);
					 for (int a = 1; a <= i; a++)
					 {
						 rysuj(a, licznik);
						 podzialka();
					 }
					 pictureBox1->Image = karta;
					 pictureBox2->Image = karta2;
					 strefa(pomiary, UCL, LCL, "karty X wymiar�w SKOMPENSOWANYCH");
					 strefa(tabMR, 3.27*srMR, 0, "karty ruchome R wymiar�w SKOMPENSOWANYCH");
					 trendy(pomiary, "X wymiar�w SKOMPENSOWANYCH");
					 trendy2(tabMR, "ruchome R wymiar�w SKOMPENSOWANYCH");
					 StrefyA(pomiary, srMR, UCL, LCL, "wymiar�w SKOMPENSOWANYCH");
					 StrefyB(pomiary, srMR, UCL, LCL, "wymiar�w SKOMPENSOWANYCH");
					 przemiennie(pomiary, "X wymiar�w SKOMPENSOWANYCH");
					 przemiennie2(tabMR, "ruchome R SKOMPENSOWANYCH");
				 }

				 sumaMR2 = 0;
				 if (i > licznik2)
				 {
					 licznik2 += 10;
				 }
				 if (i > 1)
				 {
					 for (int b = 1; b < i; b++)
					 {
						 MR2 = Math::Abs(pomiary2[b] - pomiary2[b - 1]);
						 tabMR2->Resize(tabMR2, b + 1);
						 tabMR2[b] = MR2;
						 sumaMR2 += MR2;
					 }
					 srMR2 = sumaMR2 / (i - 1);
					 CL2 = suma2 / i;
					 LCL2 = CL2 - 2.66*srMR2;
					 UCL2 = CL2 + 2.66*srMR2;
					 karta3 = Image::FromFile("..\\emptyTest1\\karta.png");
					 karta4 = Image::FromFile("..\\emptyTest1\\kartaRUCHOMER.png");
					 g3 = Graphics::FromImage(karta3);
					 g4 = Graphics::FromImage(karta4);
					 for (int a = 1; a <= i; a++)
					 {
						 rysuj2(a, licznik2);
						 podzialka2();
					 }
					 pictureBox4->Image = karta3;
					 pictureBox3->Image = karta4;
					 strefa(pomiary2, UCL2, LCL2, "karty X wymiar�w NIESKOMPENSOWANYCH");
					 strefa(tabMR2, 3.27*srMR2, 0, "karty ruchome R wymiar�w NIESKOMPENSOWANYCH");
					 trendy(pomiary2, "X wymiar�w NIESKOMPENSOWANYCH");
					 trendy2(tabMR2, "ruchome R wymiar�w NIESKOMPENSOWANYCH");
					 StrefyA(pomiary2, srMR2, UCL2, LCL2, "wymiar�w NIESKOMPENSOWANYCH");
					 StrefyB(pomiary2, srMR2, UCL2, LCL2, "wymiar�w NIESKOMPENSOWANYCH");
					 przemiennie(pomiary2, "X wymiar�w NIESKOMPENSOWANYCH");
					 przemiennie2(tabMR2, "ruchome R wymiar�w NIESKOMPENSOWANYCH");
				 }


			 }

	};
}
