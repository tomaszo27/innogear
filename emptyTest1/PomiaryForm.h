#pragma once
#include "obrazekForm.h"
#include "Header_Form.h"

namespace emptyTest1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Text;
	using namespace System::Threading;
	using namespace System::Data::SqlClient;
	using namespace Npgsql;

	/// <summary>
	/// Summary for PomiaryForm
	/// </summary>
	public ref class PomiaryForm : public System::Windows::Forms::Form
	{
	public:
		PomiaryForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~PomiaryForm()
		{
			if (components)
			{
				delete components;
			}
		}
	
	public:	Int32 nrZlecenia;
	public: Int32 nrOperacji;
	public: Int32 nrCzesci;
	public: Int32 nrCechyTomek;
	array<String^>^ linie;
	array<Byte>^ bajty;
	array<String^>^ nrwymiaru;
	array<Double>^ wartosc;
	array<Double>^ tolgorna;
	array<Double>^ toldolna;
	array<Int32>^ idWymiarow;
	array<Int16>^ nrCechy;

	String^ wprowadzajacy;


	protected:

	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::TextBox^  textBox3;


	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;








	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(35, 91);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(155, 387);
			this->textBox1->TabIndex = 1;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(252, 91);
			this->textBox2->Multiline = true;
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(155, 387);
			this->textBox2->TabIndex = 1;
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(462, 91);
			this->textBox3->Multiline = true;
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(155, 387);
			this->textBox3->TabIndex = 1;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->ForeColor = System::Drawing::Color::White;
			this->label2->Location = System::Drawing::Point(30, 62);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(136, 26);
			this->label2->TabIndex = 4;
			this->label2->Text = L"nieskompen.";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->ForeColor = System::Drawing::Color::White;
			this->label3->Location = System::Drawing::Point(247, 62);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(107, 26);
			this->label3->TabIndex = 4;
			this->label3->Text = L"skompen.";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->ForeColor = System::Drawing::Color::White;
			this->label4->Location = System::Drawing::Point(457, 62);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(146, 26);
			this->label4->TabIndex = 4;
			this->label4->Text = L"temp pomiaru";
			// 
			// PomiaryForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(12, 25);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(51)), static_cast<System::Int32>(static_cast<System::Byte>(102)),
				static_cast<System::Int32>(static_cast<System::Byte>(153)));
			this->ClientSize = System::Drawing::Size(1312, 564);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->Margin = System::Windows::Forms::Padding(6);
			this->Name = L"PomiaryForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"PomiaryForm";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &PomiaryForm::PomiaryForm_FormClosing);
			this->Load += gcnew System::EventHandler(this, &PomiaryForm::PomiaryForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private:
		NpgsqlConnection^ connection;
		NpgsqlCommand^ updacom;
		NpgsqlTransaction^ transaction;
		int a = 0;


	private: System::Void PomiaryForm_Load(System::Object^  sender, System::EventArgs^  e) {

		connection = gcnew NpgsqlConnection("server=127.0.0.1; Port=5432; User Id=postgres; Password=tomek; Database=postgres");


		/********************  pobieranie wartosci dla tego samego wymiaru w innych detalach *****************/
		PomiaryWykonane^ wykonanePomiary = gcnew PomiaryWykonane();										// tworzony jest nowy obiekt
		array<array<Double>^>^ rawData = wykonanePomiary->pobierzPomiary(nrCechyTomek, nrOperacji, nrZlecenia);		// metoda zwraca tablice Doubli
		

		for each (Double var in rawData[0])
		{
			textBox1->AppendText(var.ToString() + Environment::NewLine);								// do textboxa s� wpisywane wszystkie wymiary 
			
		}
		for each (Double var in rawData[1])
		{
			textBox2->AppendText(var.ToString("0.000") + Environment::NewLine);							// do textboxa s� wpisywane wszystkie wymiary skompensowane
		}
		for each (Double var in rawData[2])
		{
			textBox3->AppendText(var.ToString() + Environment::NewLine);								// do textboxa s� wpisywane temperatury przy jakich by�y wykonywane pomiary
		}
		
		
		
		///////////////////////////////// KONIEC /////////////////////////////////////////////////
		
		//
		
	}

	
private: System::Void zapisz_Pomiary() {
	/*Image^ karta;
	int i = 0;
	for (i = 0; i < remoteObj->wymiary->Length; i++)
	{
		Double ostatniPomiar = remoteObj->wymiary[i]->pomiary[remoteObj->wymiary[i]->pomiary->Length-1];
		zapiszDoBazy(ostatniPomiar, i);
		Image^ kartaX = Image::FromStream(gcnew MemoryStream(remoteObj->wymiary[i]->kartaX));
		karta = kartaX;
		MemoryStream^ stream = gcnew MemoryStream(remoteObj->wymiary[i]->kartaX);
		array<Byte>^ binar = gcnew array<Byte>(stream->Length);
		stream->Read(binar, 0, (int)stream->Length);
		try
		{
			zapiszObrazek(binar);
		}
		catch (Exception^ e)
		{

		}
	}

	if (a + 1 == remoteObj->wymiary->Length)
		a = 0;
	else
	{
		a++;
	}
	//textBox1->Text = remoteObj->wymiary[a]->pomiary[0].ToString();
	*/
}
private: Void zapiszDoBazy(Double wartosc, Int32 idWymiaru) {
	updacom = gcnew NpgsqlCommand("UPDATE wymiary SET wartosc_zmierzona = @wartosc_zmierzona, wprowadzajacy = @wprowadzajacy WHERE id_wymiaru = @id_wymiaru", connection);
	updacom->Parameters->Clear();
	updacom->Parameters->Add("@wartosc_zmierzona", wartosc);
	updacom->Parameters->Add("@wprowadzajacy", wprowadzajacy);
	updacom->Parameters->Add("@id_wymiaru", idWymiarow[idWymiaru]);

	try {
		connection->Open();
		transaction = connection->BeginTransaction();
		updacom->Transaction = transaction;
		updacom->ExecuteNonQuery();
		transaction->Commit();
		connection->Close();
	}
	catch (Exception^ e)
	{
		transaction->Rollback();
		MessageBox::Show(e->Message, "B��d", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
}
private: Void zapiszObrazek(array<Byte>^ obrazek) {

	updacom = gcnew NpgsqlCommand("UPDATE kat_wymiarow SET kartax = @kartaX WHERE nr_cechy = @nr_cechy", connection);
	updacom->Parameters->Clear();
	updacom->Parameters->Add("@kartaX", obrazek);
	updacom->Parameters->Add("@nr_cechy", 1112);

	try {
		connection->Open();
		transaction = connection->BeginTransaction();
		updacom->Transaction = transaction;
		updacom->ExecuteNonQuery();
		transaction->Commit();
		connection->Close();
	}
	catch (Exception^ e)
	{
		transaction->Rollback();
		MessageBox::Show(e->Message, "B��d", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
}
private: System::Void PomiaryForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
	zapisz_Pomiary();
}

};
}
