#pragma once
#include "ServerTCP.h"

namespace emptyTest1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Globalization;

	using namespace System::Threading;
	using namespace System::ComponentModel;

	/// <summary>
	/// Summary for ObrobkaForm
	/// </summary>
	public ref class ObrobkaForm : public System::Windows::Forms::Form
	{
	public:
		ObrobkaForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//

		}
		ServerTCP^ tcpServer;
		Thread^ watekLiczenia;
	private: System::Windows::Forms::TextBox^  textBoxTcpClient;

	public:

	public: delegate void UpdateLogDelegate(String^ sender, String^ message);
	public: delegate void UpdateLogToTextBoxDelegate(String^ arg);
	public: delegate void UpdateTextBoxDelegate(TextBox^ textbox, String^ text);


	private: System::Windows::Forms::TextBox^  logBox;
	private: System::Windows::Forms::TextBox^  textBox1;

	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::TextBox^  textBox5;
	private: System::Windows::Forms::TextBox^  textBox6;
	private: System::Windows::Forms::TextBox^  textBox7;
	private: System::Windows::Forms::TextBox^  textBox8;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::CheckBox^  checkBox1;
	private: System::Windows::Forms::Label^  label1;

	public:
	public: static ManualResetEvent^ tcpClientConnected = gcnew ManualResetEvent(false);


	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ObrobkaForm()
		{
			if (components)
			{
				delete components;
			}
		}


	private: System::Windows::Forms::TextBox^  textBoxTCPServer;

			 
			 int _stanProcesu;								// zmienna przechowuj�ca informacje czy orabiarka wykonuje program czy nie
	protected:

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBoxTCPServer = (gcnew System::Windows::Forms::TextBox());
			this->textBoxTcpClient = (gcnew System::Windows::Forms::TextBox());
			this->logBox = (gcnew System::Windows::Forms::TextBox());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// textBoxTCPServer
			// 
			this->textBoxTCPServer->Location = System::Drawing::Point(12, 12);
			this->textBoxTCPServer->Name = L"textBoxTCPServer";
			this->textBoxTCPServer->Size = System::Drawing::Size(226, 20);
			this->textBoxTCPServer->TabIndex = 2;
			// 
			// textBoxTcpClient
			// 
			this->textBoxTcpClient->Location = System::Drawing::Point(12, 38);
			this->textBoxTcpClient->Name = L"textBoxTcpClient";
			this->textBoxTcpClient->Size = System::Drawing::Size(226, 20);
			this->textBoxTcpClient->TabIndex = 4;
			// 
			// logBox
			// 
			this->logBox->Location = System::Drawing::Point(334, -5);
			this->logBox->Multiline = true;
			this->logBox->Name = L"logBox";
			this->logBox->Size = System::Drawing::Size(82, 209);
			this->logBox->TabIndex = 5;
			this->logBox->Visible = false;
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(111, 105);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(100, 20);
			this->textBox1->TabIndex = 6;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(111, 131);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(100, 20);
			this->textBox2->TabIndex = 8;
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(111, 157);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(100, 20);
			this->textBox3->TabIndex = 8;
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(111, 183);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(100, 20);
			this->textBox4->TabIndex = 8;
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(111, 209);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(100, 20);
			this->textBox5->TabIndex = 8;
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(111, 235);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(100, 20);
			this->textBox6->TabIndex = 8;
			// 
			// textBox7
			// 
			this->textBox7->Location = System::Drawing::Point(111, 261);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(100, 20);
			this->textBox7->TabIndex = 8;
			// 
			// textBox8
			// 
			this->textBox8->Location = System::Drawing::Point(111, 287);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(100, 20);
			this->textBox8->TabIndex = 8;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(32, 108);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(73, 13);
			this->label2->TabIndex = 7;
			this->label2->Text = L"Stan procesu:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(37, 134);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(68, 13);
			this->label3->TabIndex = 7;
			this->label3->Text = L"Nr programu:";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(13, 160);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(92, 13);
			this->label4->TabIndex = 7;
			this->label4->Text = L"Obroty wrzeciona:";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(58, 186);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(47, 13);
			this->label5->TabIndex = 7;
			this->label5->Text = L"Drgania:";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(31, 212);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(74, 13);
			this->label6->TabIndex = 7;
			this->label6->Text = L"Pos�w szybki:";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(33, 238);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(72, 13);
			this->label7->TabIndex = 7;
			this->label7->Text = L"Pos�w wolny:";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(12, 264);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(93, 13);
			this->label8->TabIndex = 7;
			this->label8->Text = L"Pocz�tek obr�bki:";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(24, 290);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(81, 13);
			this->label9->TabIndex = 7;
			this->label9->Text = L"Koniec obr�bki:";
			// 
			// checkBox1
			// 
			this->checkBox1->AutoSize = true;
			this->checkBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->checkBox1->Location = System::Drawing::Point(159, 60);
			this->checkBox1->Name = L"checkBox1";
			this->checkBox1->Size = System::Drawing::Size(164, 24);
			this->checkBox1->TabIndex = 9;
			this->checkBox1->Text = L"NIE REJESTRUJE";
			this->checkBox1->UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label1->Location = System::Drawing::Point(8, 61);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(145, 20);
			this->label1->TabIndex = 7;
			this->label1->Text = L"Stan rejestrowania:";
			// 
			// ObrobkaForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(456, 364);
			this->Controls->Add(this->checkBox1);
			this->Controls->Add(this->textBox8);
			this->Controls->Add(this->textBox7);
			this->Controls->Add(this->textBox6);
			this->Controls->Add(this->textBox5);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->logBox);
			this->Controls->Add(this->textBoxTcpClient);
			this->Controls->Add(this->textBoxTCPServer);
			this->Name = L"ObrobkaForm";
			this->Text = L"MyForm";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &ObrobkaForm::ObrobkaForm_FormClosing);
			this->Load += gcnew System::EventHandler(this, &ObrobkaForm::ObrobkaForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


	public: void UpdateLog(String^ sender, String^  message) {
		if (InvokeRequired)
		{
			//Allow for cross-thread updating
			BeginInvoke(gcnew UpdateLogDelegate(this, &ObrobkaForm::UpdateLog), gcnew array<Object^>{sender, message });
			return;
		}
		logBox->AppendText(sender + " " + message + Environment::NewLine);
	}
	public: void UpdateLogToTextBox(String^  arg) {
		if (InvokeRequired)
		{
			//Allow for cross-thread updating
			BeginInvoke(gcnew UpdateLogToTextBoxDelegate(this, &ObrobkaForm::UpdateLogToTextBox), gcnew array<Object^>{ arg });
			return;
		}
		//logBox->AppendText(sender + " " + message + Environment::NewLine);

		int iloscParam = 8;							// labview przekazuje nam 8 parametr�w
		int dlugoscParam = 19;						// labview przekazuje parametry o d�. 19 znak�w ka�dy

		array<String^>^ tabArg = gcnew array<String^>(iloscParam);
		for (int i = 0; i < iloscParam; i++)
		{
			tabArg[i] = arg->Substring(i*dlugoscParam, dlugoscParam);
		}
//stanProcesu = Convert::ToInt(tabArg[0]);
		textBox1->Text = tabArg[0];
//		textBox1->Text = Convert::ToString(stanProcesu);
		textBox2->Text = tabArg[1];
		textBox3->Text = tabArg[2];
		textBox4->Text = tabArg[3];
		textBox5->Text = tabArg[4];
		textBox6->Text = tabArg[5];
		textBox7->Text = tabArg[6];
		textBox8->Text = tabArg[7];

	/*	if (stanProcesu == 1) {
			checkBox1->Checked = true;
			checkBox1->Text = "rejestruje";
		}
		else
		{

			checkBox1->Checked = false;
			checkBox1->Text = "nie rejestruje";
		}*/
			

	}

			void UpdateTextBox(TextBox^ textBox, String^ text)
			{
				if (InvokeRequired)
				{
					//Allow for cross-thread updating
					BeginInvoke(gcnew UpdateTextBoxDelegate(this, &ObrobkaForm::UpdateTextBox), gcnew array<Object^>{ textBox, text });
					return;
				}
				textBox->Text = text;
			}

	public: void LogMessageEventHandler(Object^ sender, String^ message) {
		UpdateLog(sender->GetType()->Name, message);
	}
	public: void LogToTextBoxMessageEventHandler(String^ arg) {
		UpdateLogToTextBox(arg);
	}
			void tcpServer_TcpConnectionChangedEvent(String^ sender, String^ status)
			{
				if (sender == "Client")
					UpdateTextBox(textBoxTcpClient, status);
				else if (sender == "Server")
					UpdateTextBox(textBoxTCPServer, status);
			}

			void tcpServer_TcpMessageReceivedEvent(String^ function, String^ argument)
			{
				if (function == "PopupTestMessage")
					//dnRemotingServer->remoteObject->PopupTestMessage(argument);
					MessageBox::Show(argument);

				else if (function == "praca")

				{

					tcpServer->parametersToTextBox(argument);
				}



				else if (function == "GetX")
				{

					//int x = dnRemotingServer->remoteObject->GetX();
					//tcpServer->SendMessage(x.ToString());
				}

				else if (function == "CalculateSine")
				{
					//double sine = dnRemotingServer->remoteObject->CalculateSine();
					//tcpServer->SendMessage(sine.ToString());
				}

				else
					UpdateLog("Server", "Unknown TCP Command - " + function);

				tcpServer->WaitForMessages();
			}


	private: System::Void ObrobkaForm_Load(System::Object^  sender, System::EventArgs^  e) {
		tcpServer = gcnew ServerTCP();		// nowy  obiekt klasy ServerTCP

											//watekLiczenia = gcnew Thread(gcnew ThreadStart(this, &MyForm::watek));
											//watekLiczenia->Start();

		tcpServer->logMessageEvent += gcnew logMessageEventDelegat(this, &ObrobkaForm::LogMessageEventHandler);
		tcpServer->logToTextBoxMessageEvent += gcnew logToTextBoxMessageEventDelegat(this, &ObrobkaForm::LogToTextBoxMessageEventHandler);
		tcpServer->TcpConnectionChangedEvent += gcnew TcpConnectionChanged(this, &ObrobkaForm::tcpServer_TcpConnectionChangedEvent);
		tcpServer->TcpMessageReceivedEvent += gcnew TcpMessageReceivedEventDelegate(this, &ObrobkaForm::tcpServer_TcpMessageReceivedEvent);

		tcpServer->StartServerTCP();
	}
	private: System::Void ObrobkaForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
		tcpServer->ShutdownTCPServer();
	}



};
}
