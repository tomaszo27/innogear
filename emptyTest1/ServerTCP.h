#pragma once
using namespace System;
using namespace System::Text;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Net::Sockets;
using namespace System::Net;
using namespace System::IO;
using namespace System::Threading;
using namespace System::Collections::Generic;


public delegate void logMessageEventDelegat(Object^ sender, String^ message);	
public delegate void logToTextBoxMessageEventDelegat(String^ arg);
public delegate void TcpConnectionChanged(String^ sender, String^ status);
public delegate void TcpMessageReceivedEventDelegate(String^ function, String^ argument);
ref class ServerTCP
{
public:
	event logMessageEventDelegat^ logMessageEvent;						// Event do przekazania ze wiadomosc ma byc wypisana
	event logToTextBoxMessageEventDelegat^ logToTextBoxMessageEvent;	// Event do przekazania ze wiadomosc ma byc wypisana
	event TcpConnectionChanged^ TcpConnectionChangedEvent;				// Event do poinformowania gdy kli9ent sie po��czy/roz��czy
	event TcpMessageReceivedEventDelegate^ TcpMessageReceivedEvent;		// Event do poinformowania ze odebrano wiadomosc od klienta

	//IPAddress^ listenAdress = IPAddress::Parse("127.0.0.1");
	IPAddress^ listenAdress = IPAddress::Parse("10.10.10.206");
	unsigned short listenPort = 3000;
	bool isTcpClientConnected = false;

	TcpListener^ tcpListener = nullptr;
	TcpClient^ tcpClient = nullptr;

	// Do synchronizacji w�tk�w
	static ManualResetEvent^ tcpClientConnected = gcnew ManualResetEvent(false);

	Thread^ tcpListenerThread;											// W�tek do nas�uchiwania nadchodzacych po��cze�
	Thread^ tcpReaderThread;											// W�tek do nas�uchiwania nadchodz�cych wiadomo�ci

	Int32 tcpBytesRead = 0;
	array<Byte>^ tcpReadData;

	void StartServerTCP() {												// StartServerTCP: otwiera port i nas�uchuje po��cze�
		tcpListener = gcnew TcpListener(listenAdress, listenPort);
		//tcpListener = gcnew TcpListener(IPAddress::Loopback, listenPort);
		Log("Serwer uruchomiony. Nas�uch na: " + tcpListener->LocalEndpoint->ToString());
		TcpConnectionChangedEvent("Server", "Connected: " + tcpListener->LocalEndpoint);

		tcpListenerThread = gcnew Thread(gcnew ThreadStart(this, &ServerTCP::WaitForConnection));
		tcpListenerThread->Start();
	}

	void Log(String^ message)
	{
		logMessageEvent(this, message);
	}
	void LogToTextBox(String^ arg)
	{
		logToTextBoxMessageEvent(arg);
	}

private:
	void WaitForConnection()											// Czekanie na klienta ktory po��czy si� z portem TCP
	{
		Log("Oczekiwanie na po��czenie z Klientem");
		TcpConnectionChangedEvent("Client", "Waiting for connection...");
		tcpClientConnected->Reset();
		tcpListener->Start();
		
		tcpListener->BeginAcceptTcpClient(gcnew AsyncCallback(this, &ServerTCP::DoAcceptTcpClientCallback), nullptr);
		
		// czekanie a� klient si� po��czy
		tcpClientConnected->WaitOne();

		Log("Klient si� po��czy�, huraa");
		isTcpClientConnected = true;
		TcpConnectionChangedEvent("Client", "Connected: " + tcpListener->LocalEndpoint);

		// oczekiwanie na dane od klienta
		WaitForMessages();
	}
	void DoAcceptTcpClientCallback(IAsyncResult^ ar)					// Process the client connection.
	{
		//TcpListener^ tcpListener = (TcpListener^)ar->AsyncState;

		if (tcpListener == nullptr)
			return;

		// End the operation and display the received data on 
		// the console.
		//TcpClient^ tcpClient = tcpListener->EndAcceptTcpClient(ar);
		tcpClient = tcpListener->EndAcceptTcpClient(ar);
		// Signal the calling thread to continue.
		tcpClientConnected->Set();

	}
public: void WaitForMessages()											//WaitForMessages: Asynchroniczne oczekiwanie na wiadomo�ci na porcie
	{
		tcpReaderThread = gcnew Thread(gcnew ThreadStart(this, &ServerTCP::TcpWaitAndReadData));
		tcpReaderThread->Start();
	}

/*		
public: void TcpWaitAndReadData()										//TcpWaitAndReadData: Wait for a TCP Message and notify. Blocking call. 
	{
		NetworkStream^ clientStream = tcpClient->GetStream();

		tcpReadData = gcnew array<Byte>(4096);// byte[4096];
		Log("Listening for messages...");

		try			
		{
			tcpBytesRead = clientStream->Read(tcpReadData, 0, 4096);

			ASCIIEncoding^ encoder = gcnew ASCIIEncoding();
			if (tcpBytesRead == 0)		
			{
					//Connection Lost
				throw gcnew Exception();
			}
			String^ message = encoder->GetString(tcpReadData, 0, tcpBytesRead);
			Log("odebrano: \"" + message + "\"");
			
			int index = message->IndexOf(',');
			String^ function = message->Substring(0, index);
			String^ arguments = message->Substring(index + 1, message->Length - (index + 1));

			TcpMessageReceivedEvent(function, arguments);
		}

		catch (Exception^)
		{
			//Connection Lost
			ClientDisconnected();	
		}
	}*/
public: void TcpWaitAndReadData()										//TcpWaitAndReadData: Wait for a TCP Message and notify. Blocking call. 
{
	NetworkStream^ clientStream = tcpClient->GetStream();

	tcpReadData = gcnew array<Byte>(4096);// byte[4096];

	try
	{
		tcpBytesRead = clientStream->Read(tcpReadData, 0, 4096);

		ASCIIEncoding^ encoder = gcnew ASCIIEncoding();
		if (tcpBytesRead == 0)
		{
			//Connection Lost
			throw gcnew Exception();
		}
		String^ message = encoder->GetString(tcpReadData, 0, tcpBytesRead);
		Log(Environment::NewLine);
		Log("bieszczady \"" + message + "\"");

		int index = message->IndexOf(',');
		String^ function = message->Substring(0, index);
		String^ arguments = message->Substring(index + 1, message->Length - (index + 1));
		

		TcpMessageReceivedEvent(function, arguments);
	}

	catch (Exception^)
	{
		//Connection Lost
		ClientDisconnected();
	}
}

public: void parametersToTextBox(String^ arguments)
{
	LogToTextBox(arguments);
}
public: void SendMessage(String^ message)
		{
			NetworkStream^ clientStream = tcpClient->GetStream();

			ASCIIEncoding^ encoder = gcnew ASCIIEncoding();
			array<Byte>^ buffer = encoder->GetBytes(message);

			clientStream->Write(buffer, 0, buffer->Length);
			clientStream->Flush();

			Log("Sent: \"" + message + "\"");
		}
		void ClientDisconnected()
		{
			try
			{
				Log("TCP Client disconnected");
				isTcpClientConnected = false;
				TcpConnectionChangedEvent("Client", "No client connected");
				tcpClient->GetStream()->Close();
				tcpClient->Close();
				tcpClient = nullptr;

				//Start Listening for connections again
				tcpListenerThread = gcnew Thread(gcnew ThreadStart(this, &ServerTCP::WaitForConnection));
				tcpListenerThread->Start();
			}
			catch (Exception^)
			{
				
			}
		}

		
		void ShutdownTCPServer()
		{
			Log("Shutting down");
			isTcpClientConnected = false;
			TcpConnectionChangedEvent("Client", "No client connected");
			if (tcpReaderThread != nullptr && tcpReaderThread->IsAlive)
				tcpReaderThread->Abort();

			if (tcpListenerThread != nullptr && tcpListenerThread->IsAlive)
				tcpListenerThread->Abort();

			if (tcpClient != nullptr)
			{
				tcpClient->GetStream()->Close();
				tcpClient->Close();
			}

			if (tcpListener != nullptr)
			{
				tcpListener->Stop();
				tcpListener = nullptr;
			}
		}

};







