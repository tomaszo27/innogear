#pragma once
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Security::Cryptography;
using namespace System::Text;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Data::SqlClient;
using namespace Npgsql;
using namespace System::Diagnostics;

using namespace System::Threading::Tasks;

ref class Passw
{
public:

	Passw()
	{

	}
	String^ getMD5String(String^ inputString)
	{
		array<Byte>^ byteArray = System::Text::Encoding::ASCII->GetBytes(inputString);
		MD5CryptoServiceProvider^ md5provider = gcnew MD5CryptoServiceProvider();
		array<Byte>^ byteArrayHash = md5provider->ComputeHash(byteArray);
		return BitConverter::ToString(byteArrayHash);
	}

};





ref class Przekazywane
{
public:
	Int32 id_operatora;
	String^ uzytkownik;
	String^ stanowisko_operatora;
	String^ informacja;
	Int32 id_operacji;
	String^ nazwa_obrabiarki;
	Int16   id_obrabiarki;
	String^ klas_export;
};

ref class PomiaryWykonane
{
public:
	
	PomiaryWykonane(void) {
		connection = gcnew NpgsqlConnection("server=127.0.0.1; Port=5432; User Id=postgres; Password=tomek; Database=postgres");
		connection->Open();

	}
	~PomiaryWykonane(void) {

	}

private:
	
	NpgsqlConnection^ connection;
	NpgsqlCommand^ updacom;
	NpgsqlTransaction^ transaction;

	

public:
	array<array<Double>^>^ pobierzPomiary(Int32 nr_cechy, Int32 nr_operacji, Int32 nr_serii) {
		
		
		NpgsqlCommand^ selectcomm = gcnew NpgsqlCommand("SELECT kat_operacji.nr_operacji, wymiary.wartosc_zmierzona, wymiary.wartosc_zmierzona_skomp, wymiary.temperatura, seria.nr_serii, wymiary.id_cechy, wymiary.id_wymiaru, wymiary.wykluczenie FROM public.seria, public.operacje, public.kat_operacji, public.wymiary, public.kat_wymiarow WHERE seria.id_serii = kat_operacji.id_serii AND operacje.id_operacji = wymiary.id_operacji AND kat_operacji.id_kat_operacji = operacje.id_kat_operacji AND kat_wymiarow.id_wymiaru = wymiary.id_cechy AND kat_operacji.nr_operacji = @nr_operacji AND seria.nr_serii = @nr_serii AND kat_wymiarow.nr_cechy = @nr_cechy;", connection);
		selectcomm->Parameters->Add("@nr_cechy", nr_cechy);
		selectcomm->Parameters->Add("@nr_operacji", nr_operacji);
		selectcomm->Parameters->Add("@nr_serii", nr_serii);
		DataTable^ dt = gcnew DataTable("Wymiary1");
		NpgsqlDataAdapter^ adapter = gcnew NpgsqlDataAdapter(selectcomm);
		adapter->Fill(dt);
		//array<Byte>^ kartaBin = Convert::ToByte(ds->Tables[0]->Rows[0][0]);
		
		List<Double>^ wymiar = gcnew List<Double>();
		List<Double>^ wymiar_skomp = gcnew List<Double>();
		List<Double>^ temperatura = gcnew List<Double>();
		List<Double>^ idWymiaru = gcnew List<Double>();
		List<Double>^ wykluczenie = gcnew List<Double>();
		for each (DataRow^ var in dt->Rows)
		{
			if (var["wartosc_zmierzona"] != DBNull::Value)
			{
				wymiar->Add(Convert::ToDouble(var["wartosc_zmierzona"]));
				wymiar_skomp->Add(Convert::ToDouble(var["wartosc_zmierzona_skomp"]));
				temperatura->Add(Convert::ToDouble(var["temperatura"]));
				idWymiaru->Add(Convert::ToDouble(var["id_wymiaru"]));
				wykluczenie->Add(Convert::ToDouble(var["wykluczenie"]));
			}
			
			
		}
		array<Double>^ rawData_nieskomp = wymiar->ToArray();
		array<Double>^ rawData_skomp = wymiar_skomp->ToArray();
		array<Double>^ rawData_temp = temperatura->ToArray();
		array<Double>^ rawData_idWymiaru = idWymiaru->ToArray();
		array<Double>^ rawData_wykluczenie = wykluczenie->ToArray();
		array<array<Double>^>^ rawData = { rawData_nieskomp , rawData_skomp ,  rawData_temp , rawData_idWymiaru , rawData_wykluczenie };


		//Object^ SqlRetVal = nullptr;
		//SqlRetVal = selectcomm->ExecuteScalar();

		//array<Int32>^ rawData = gcnew array<Int32>(0);
		//rawData = safe_cast<array<Int32>^>(SqlRetVal);
		return rawData;

		//MemoryStream^ memStream = gcnew MemoryStream();
		//memStream->Write(rawData, 0, rawData->Length);
		//Image^ kartaX = Image::FromStream(memStream);
	}


public: 
	array<array<Double>^>^ pobierzWykluczenia(Int32 nr_cechy, Int32 nr_operacji, Int32 nr_serii) {

	NpgsqlCommand^ selectcomm = gcnew NpgsqlCommand("SELECT kat_operacji.nr_operacji, wymiary.wartosc_zmierzona, wymiary.wartosc_zmierzona_skomp, wymiary.temperatura, seria.nr_serii, wymiary.id_cechy, wymiary.id_wymiaru, wymiary.wykluczenie FROM public.seria, public.operacje, public.kat_operacji, public.wymiary, public.kat_wymiarow WHERE seria.id_serii = kat_operacji.id_serii AND operacje.id_operacji = wymiary.id_operacji AND kat_operacji.id_kat_operacji = operacje.id_kat_operacji AND kat_wymiarow.id_wymiaru = wymiary.id_cechy AND kat_operacji.nr_operacji = @nr_operacji AND seria.nr_serii = @nr_serii AND kat_wymiarow.nr_cechy = @nr_cechy AND wymiary.wykluczenie = TRUE;", connection);
	selectcomm->Parameters->Add("@nr_cechy", nr_cechy);
	selectcomm->Parameters->Add("@nr_operacji", nr_operacji);
	selectcomm->Parameters->Add("@nr_serii", nr_serii);
	DataTable^ dt = gcnew DataTable("Wymiary1");
	NpgsqlDataAdapter^ adapter = gcnew NpgsqlDataAdapter(selectcomm);
	adapter->Fill(dt);
	//array<Byte>^ kartaBin = Convert::ToByte(ds->Tables[0]->Rows[0][0]);

	List<Double>^ wymiar = gcnew List<Double>();
	List<Double>^ wymiar_skomp = gcnew List<Double>();
	List<Double>^ temperatura = gcnew List<Double>();
	List<Double>^ idWymiaru = gcnew List<Double>();
	List<Double>^ wykluczenie = gcnew List<Double>();
	for each (DataRow^ var in dt->Rows)
	{
		if (var["wartosc_zmierzona"] != DBNull::Value)
		{
			wymiar->Add(Convert::ToDouble(var["wartosc_zmierzona"]));
			wymiar_skomp->Add(Convert::ToDouble(var["wartosc_zmierzona_skomp"]));
			temperatura->Add(Convert::ToDouble(var["temperatura"]));
			idWymiaru->Add(Convert::ToDouble(var["id_wymiaru"]));
			wykluczenie->Add(Convert::ToDouble(var["wykluczenie"]));
		}


	}
	array<Double>^ rawData_nieskomp = wymiar->ToArray();
	array<Double>^ rawData_skomp = wymiar_skomp->ToArray();
	array<Double>^ rawData_temp = temperatura->ToArray();
	array<Double>^ rawData_idWymiaru = idWymiaru->ToArray();
	array<Double>^ rawData_wykluczenie = wykluczenie->ToArray();
	array<array<Double>^>^ rawData = { rawData_nieskomp , rawData_skomp ,  rawData_temp , rawData_idWymiaru , rawData_wykluczenie };


	//Object^ SqlRetVal = nullptr;
	//SqlRetVal = selectcomm->ExecuteScalar();

	//array<Int32>^ rawData = gcnew array<Int32>(0);
	//rawData = safe_cast<array<Int32>^>(SqlRetVal);
	return rawData;

	//MemoryStream^ memStream = gcnew MemoryStream();
	//memStream->Write(rawData, 0, rawData->Length);
	//Image^ kartaX = Image::FromStream(memStream);
}

};

ref class Temperatura
{
private:

	NpgsqlConnection^ connection;

public:
	Temperatura(void) {
		connection = gcnew NpgsqlConnection("server=127.0.0.1; Port=5432; User Id=postgres; Password=tomek; Database=postgres");
		connection->Open();
	}
public:
	Double GetTemp(void) {
		
		NpgsqlCommand^ selectcomm = gcnew NpgsqlCommand("SELECT temperatura.temperatura FROM public.temperatura;", connection);
		
		DataTable^ dt = gcnew DataTable("Temperatura aktualna");
		NpgsqlDataAdapter^ adapter = gcnew NpgsqlDataAdapter(selectcomm);
		adapter->Fill(dt);
		//array<Byte>^ kartaBin = Convert::ToByte(ds->Tables[0]->Rows[0][0]);

		List<Double>^ temp = gcnew List<Double>();
		for each (DataRow^ var in dt->Rows)
		{
			if (var[0] != DBNull::Value)
			{
				temp->Add(Convert::ToDouble(var[0]));
			}
		}
		array<Double>^ rawData = temp->ToArray();

		return rawData[0];

	}

public:
	Double GetWsp(String^ nrDetalu) {
		NpgsqlCommand^ selectcomm = gcnew NpgsqlCommand("SELECT detale.wsp_rozszerzalnosci, detale.nr_detalu FROM public.detale WHERE detale.nr_detalu = @nr_detalu;", connection);
		selectcomm->Parameters->Add("@nr_detalu", nrDetalu);

		DataTable^ dt = gcnew DataTable("Wspolczynnik rozszerzalnosci");
		NpgsqlDataAdapter^ adapter = gcnew NpgsqlDataAdapter(selectcomm);
		adapter->Fill(dt);
		//array<Byte>^ kartaBin = Convert::ToByte(ds->Tables[0]->Rows[0][0]);

		List<Double>^ temp = gcnew List<Double>();
		for each (DataRow^ var in dt->Rows)
		{
			if (var["wsp_rozszerzalnosci"] != DBNull::Value)
			{
				temp->Add(Convert::ToDouble(var["wsp_rozszerzalnosci"]));
			}
		}
		array<Double>^ rawData = temp->ToArray();

		return rawData[0];
	}

private:
	//
	;
};

