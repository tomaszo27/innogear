#pragma once
#include "Portszeregowy.h"


namespace Ustawieniasuw {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace portszer;

	/// <summary>
	/// Podsumowanie informacji o UstawieniaSuw
	/// </summary>
	public ref class UstawieniaSuw : public System::Windows::Forms::Form
	{

	public:
		Portszeregowy^ nowy;

	public:
		UstawieniaSuw()
		{
			InitializeComponent();

			//
			//TODO: W tym miejscu dodaj kod konstruktora
			//
		}

	protected:
		/// <summary>
		/// Wyczy�� wszystkie u�ywane zasoby.
		/// </summary>
		~UstawieniaSuw()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected:
	private: System::Windows::Forms::ComboBox^  comboBox1;


	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::IO::Ports::SerialPort^  serialPort1;
	private: System::Windows::Forms::Button^  button1;


	private: System::ComponentModel::IContainer^  components;



	private:
		/// <summary>
		/// Wymagana zmienna projektanta.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Wymagana metoda obs�ugi projektanta � nie nale�y modyfikowa� 
		/// zawarto�� tej metody z edytorem kodu.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->serialPort1 = (gcnew System::IO::Ports::SerialPort(this->components));
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label1->Location = System::Drawing::Point(9, 42);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(247, 32);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Wybierz port komunikacyjny, do kt�rego\r\npod��czone jest urz�dzenie pomiarowe:";
			// 
			// comboBox1
			// 
			this->comboBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->comboBox1->ImeMode = System::Windows::Forms::ImeMode::NoControl;
			this->comboBox1->IntegralHeight = false;
			this->comboBox1->Location = System::Drawing::Point(262, 50);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(257, 24);
			this->comboBox1->TabIndex = 1;
			this->comboBox1->TabStop = false;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &UstawieniaSuw::comboBox1_SelectedIndexChanged);
			this->comboBox1->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &UstawieniaSuw::textBox2_KeyDown);
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(57)),
				static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button2->Location = System::Drawing::Point(46, 198);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(195, 50);
			this->button2->TabIndex = 4;
			this->button2->Text = L"OK";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += gcnew System::EventHandler(this, &UstawieniaSuw::button2_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label2->Location = System::Drawing::Point(9, 100);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(466, 32);
			this->label2->TabIndex = 5;
			this->label2->Text = L"Wykonaj pomiar.Je�eli test przebiegnie pomy�lnie, w polu tekstowym poni�ej \r\npowi"
				L"nna wy�wietli� si� zmierzona warto��.";
			// 
			// textBox1
			// 
			this->textBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox1->Location = System::Drawing::Point(12, 135);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->ReadOnly = true;
			this->textBox1->Size = System::Drawing::Size(504, 57);
			this->textBox1->TabIndex = 6;
			this->textBox1->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &UstawieniaSuw::textBox2_KeyDown);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label3->Location = System::Drawing::Point(9, 9);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(156, 16);
			this->label3->TabIndex = 0;
			this->label3->Text = L"Podaj nazw� urz�dzenia:";
			// 
			// textBox2
			// 
			this->textBox2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBox2->Location = System::Drawing::Point(174, 6);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(345, 22);
			this->textBox2->TabIndex = 7;
			this->textBox2->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &UstawieniaSuw::textBox2_KeyDown);
			// 
			// serialPort1
			// 
			this->serialPort1->DiscardNull = true;
			this->serialPort1->DtrEnable = true;
			this->serialPort1->RtsEnable = true;
			this->serialPort1->DataReceived += gcnew System::IO::Ports::SerialDataReceivedEventHandler(this, &UstawieniaSuw::serialPort1_DataReceived);
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(57)),
				static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button1->Location = System::Drawing::Point(285, 198);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(190, 49);
			this->button1->TabIndex = 8;
			this->button1->Text = L"Anuluj";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &UstawieniaSuw::button1_Click);
			// 
			// UstawieniaSuw
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
			this->AutoSize = true;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(38)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)));
			this->ClientSize = System::Drawing::Size(536, 261);
			this->ControlBox = false;
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->comboBox1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label1);
			this->ForeColor = System::Drawing::Color::White;
			this->Name = L"UstawieniaSuw";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
			this->Text = L"Dodaj urz�dzenie";
			this->Load += gcnew System::EventHandler(this, &UstawieniaSuw::UstawieniaSuw_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		if (textBox2->Text == "")
		{
			MessageBox::Show("Podaj nazw� urz�dzenia", "B��d", MessageBoxButtons::OK, MessageBoxIcon::Warning);
			if (comboBox1->Text == "")
			{
				MessageBox::Show("Wybierz port", "B��d", MessageBoxButtons::OK, MessageBoxIcon::Warning);
			}
		}
		else
		{
			serialPort1->Close();
			String^ nazwa = textBox2->Text;
			String^ port = comboBox1->Text;
			nowy = gcnew Portszeregowy(nazwa, port);
			nowy->otworz();
			this->Close();
		}

	}


	private: System::Void UstawieniaSuw_Load(System::Object^  sender, System::EventArgs^  e) {
		//this->Icon = gcnew System::Drawing::Icon("..\\Suwmiarka\\Logo2.ico");
		array<String^>^ porty = serialPort1->GetPortNames();							// utworzenie obiektu z pobranymi dost�pnymi portami
		comboBox1->Items->AddRange(porty);												// przepisanie dost�pnych port�w do comboBoxa
	}
	public:	Void wyzwol()
	{
		if (serialPort1->IsOpen == true)
			serialPort1->Write("01 \u000d");								//TU BEDZIE ADRES URZADZENIA!!!!!!!!!!!!!!!!!!!!!
	}

	private: System::Void serialPort1_DataReceived(System::Object^  sender, System::IO::Ports::SerialDataReceivedEventArgs^  e) {
		try
		{
			array<Byte>^ bajty = gcnew array<Byte>(1);
			int i = 0;
			while (i<13)    //TU BY�O 13!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			{
				if (bajty[i] = serialPort1->ReadByte())
				{
					if (bajty[i] == ',')
						bajty[i] = '.';
					i++;
					bajty->Resize(bajty, i + 1);
				}
				else
					break;
			}
			bajty->Resize(bajty, i);
			String^ smieci = serialPort1->ReadExisting();
			serialPort1->DiscardInBuffer();
			String^ pomiar = System::Text::Encoding::Default->GetString(bajty, 0, bajty->Length);
			pomiar = pomiar->Remove(0, pomiar->IndexOf('.') - 5);
			Globalization::CultureInfo^ kultura = Globalization::CultureInfo::CreateSpecificCulture("en-US");
			Double liczba = Double::Parse(pomiar, kultura);
			textBox1->AppendText(liczba.ToString() + Environment::NewLine);
		}
		catch (Exception^ e)
		{
			MessageBox::Show(e->Message, "B��d", MessageBoxButtons::OK, MessageBoxIcon::Error);
		}

	}
	private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		try
		{
			if (serialPort1->IsOpen)
			{
				serialPort1->Close();
			}
			serialPort1->PortName = comboBox1->Text;
			serialPort1->Open();
		}
		catch (Exception^ e)
		{
			MessageBox::Show(e->Message, "B��d", MessageBoxButtons::OK, MessageBoxIcon::Error);
		}

	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}
	private: System::Void textBox2_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
		if (e->KeyCode == Keys::Enter)
			wyzwol();
	}
	};
}
