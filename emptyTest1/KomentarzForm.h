#pragma once

namespace emptyTest1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Data::SqlClient;
	using namespace Npgsql;

	/// <summary>
	/// Summary for KomentarzForm
	/// </summary>
	public ref class KomentarzForm : public System::Windows::Forms::Form
	{
	public:
		KomentarzForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~KomentarzForm()
		{
			if (components)
			{
				delete components;
			}
		}

	public: System::Int32 id_operacji;

	private: System::Windows::Forms::TextBox^  textBox1;
	protected:
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(24, 22);
			this->textBox1->Margin = System::Windows::Forms::Padding(5, 6, 5, 6);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->textBox1->Size = System::Drawing::Size(848, 368);
			this->textBox1->TabIndex = 0;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(73, 460);
			this->button1->Margin = System::Windows::Forms::Padding(5, 6, 5, 6);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(283, 118);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Edytuj komentarz";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &KomentarzForm::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(539, 460);
			this->button2->Margin = System::Windows::Forms::Padding(5, 6, 5, 6);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(283, 118);
			this->button2->TabIndex = 1;
			this->button2->Text = L"Wyj�cie";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &KomentarzForm::button2_Click);
			// 
			// KomentarzForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(12, 25);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::Orange;
			this->ClientSize = System::Drawing::Size(899, 624);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox1);
			this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Margin = System::Windows::Forms::Padding(5, 6, 5, 6);
			this->Name = L"KomentarzForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Edycja komentarza";
			this->Load += gcnew System::EventHandler(this, &KomentarzForm::KomentarzForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: NpgsqlConnection^ connection;
	private: NpgsqlCommand^ selectcomm;
	private: NpgsqlCommand^ updacom;
	private: NpgsqlTransaction^ transaction;

	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	private: System::Void KomentarzForm_Load(System::Object^  sender, System::EventArgs^  e) {
		connection = gcnew NpgsqlConnection("server=127.0.0.1; Port=5432; User Id=postgres; Password=tomek; Database=postgres");
		pobierzKomentarz();	
		
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		zapiszKomentarz();
	}
	private: System::Void pobierzKomentarz() {
		selectcomm = gcnew NpgsqlCommand("SELECT operacje.komentarz_operatora FROM public.operacje WHERE  operacje.id_operacji = @id_operacji;", connection);
		selectcomm->Parameters->Add("@id_operacji", id_operacji);
		DataSet^ ds = gcnew DataSet("komentarz");
		NpgsqlDataAdapter^ adapter = gcnew NpgsqlDataAdapter(selectcomm);
		adapter->Fill(ds);
		String^ komentarz = Convert::ToString(ds->Tables[0]->Rows[0][0]);
		textBox1->Text = komentarz;
		textBox1->AppendText(Environment::NewLine);

	}
	private: System::Void zapiszKomentarz() {
		updacom = gcnew NpgsqlCommand("UPDATE operacje SET komentarz_operatora = @komentarz_operatora WHERE  id_operacji = @id_operacji;", connection);
		updacom->Parameters->Clear();
		updacom->Parameters->Add("@komentarz_operatora ", textBox1->Text);
		updacom->Parameters->Add("@id_operacji", id_operacji);

		try {
			connection->Open();
			transaction = connection->BeginTransaction();
			updacom->Transaction = transaction;
			updacom->ExecuteNonQuery();
			transaction->Commit();
			connection->Close();
		}
		catch (Exception^ e)
		{
			transaction->Rollback();
			MessageBox::Show(e->Message, "B��d", MessageBoxButtons::OK, MessageBoxIcon::Error);
		}

		pobierzKomentarz();
	}
};
}
