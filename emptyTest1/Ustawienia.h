#pragma once
#include "UstawieniaSuw.h"
#include "Kalibracja.h"


namespace Ustawienia1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace Ustawieniasuw;
	using namespace System::IO;

	/// <summary>
	/// Podsumowanie informacji o Ustawienia
	/// </summary>
	public ref class Ustawienia : public System::Windows::Forms::Form
	{
	private: System::Windows::Forms::TabControl^  tabControl1;
	public:
	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::TabPage^  tabPage2;
	public: System::Windows::Forms::CheckBox^  checkBox4;
	private:
	public: System::Windows::Forms::CheckBox^  checkBox3;
	public: System::Windows::Forms::CheckBox^  checkBox2;
	public: System::Windows::Forms::CheckBox^  checkBox1;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::PictureBox^  pictureBox4;
	private: System::Windows::Forms::PictureBox^  pictureBox3;
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::PictureBox^  pictureBox1;

	public: System::Windows::Forms::TextBox^  textBox6;
	private:
	public: System::Windows::Forms::TextBox^  textBox5;
	public: System::Windows::Forms::TextBox^  textBox4;
	public: System::Windows::Forms::TextBox^  textBox3;
	public: System::Windows::Forms::TextBox^  textBox2;
	public: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Nazwaurzadzenia;
	public:
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Portszeregowy;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Kalibracja;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  cp;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Cpk;









	public:	static array<portszer::Portszeregowy^>^ urzadzenia = gcnew array<portszer::Portszeregowy^>(0);

	public:
		Ustawienia(void)
		{
			InitializeComponent();
			//
			//TODO: W tym miejscu dodaj kod konstruktora
			//

		}
		Ustawienia(array<String^>^ nazwy, array<String^>^ nrportow, array<String^>^ kalibracje, array<String^>^ tabCg, array<String^>^ tabCgk)
		{
			InitializeComponent();

			try
			{
				int i = 0;
				for each (String^ var in nazwy)
				{
					urzadzenia->Resize(urzadzenia, i + 1);
					urzadzenia[i] = gcnew portszer::Portszeregowy(var, nrportow[i]);
					urzadzenia[i]->otworz();
					dataGridView1->Rows->Add();
					dataGridView1->Rows[dataGridView1->RowCount - 1]->Cells[0]->Value = urzadzenia[urzadzenia->Length - 1]->nazwa;
					dataGridView1->Rows[dataGridView1->RowCount - 1]->Cells[1]->Value = urzadzenia[urzadzenia->Length - 1]->port;
					dataGridView1->Rows[dataGridView1->RowCount - 1]->Cells[2]->Value = kalibracje[i];
					dataGridView1->Rows[dataGridView1->RowCount - 1]->Cells[3]->Value = tabCg[i];
					dataGridView1->Rows[dataGridView1->RowCount - 1]->Cells[4]->Value = tabCgk[i];
					i++;

				}
			}
			catch (Exception^ e)
			{
				delete urzadzenia[urzadzenia->Length - 1];
				urzadzenia->Resize(urzadzenia, urzadzenia->Length - 1);
				MessageBox::Show(e->Message, "B��d", MessageBoxButtons::OK, MessageBoxIcon::Error);
			}
		}

	protected:
		/// <summary>
		/// Wyczy�� wszystkie u�ywane zasoby.
		/// </summary>
		~Ustawienia()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:



	private: System::Windows::Forms::Button^  button1;

	private: System::Windows::Forms::Button^  button2;




	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;





	public: System::Windows::Forms::DataGridView^  dataGridView1;
	private:



	private: System::IO::Ports::SerialPort^  serialPort1;
	private: System::ComponentModel::IContainer^  components;







	private:
		/// <summary>
		/// Wymagana zmienna projektanta.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Wymagana metoda obs�ugi projektanta � nie nale�y modyfikowa� 
		/// zawarto�� tej metody z edytorem kodu.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle7 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle6 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->Nazwaurzadzenia = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Portszeregowy = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Kalibracja = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->cp = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Cpk = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->serialPort1 = (gcnew System::IO::Ports::SerialPort(this->components));
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->pictureBox4 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->checkBox4 = (gcnew System::Windows::Forms::CheckBox());
			this->checkBox3 = (gcnew System::Windows::Forms::CheckBox());
			this->checkBox2 = (gcnew System::Windows::Forms::CheckBox());
			this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->tabControl1->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->tabPage2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->AllowUserToResizeColumns = false;
			this->dataGridView1->AllowUserToResizeRows = false;
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->BackgroundColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(38)), static_cast<System::Int32>(static_cast<System::Byte>(57)));
			dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(159)),
				static_cast<System::Int32>(static_cast<System::Byte>(199)));
			dataGridViewCellStyle1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(238)));
			dataGridViewCellStyle1->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle1->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle1->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dataGridView1->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
				this->Nazwaurzadzenia,
					this->Portszeregowy, this->Kalibracja, this->cp, this->Cpk
			});
			this->dataGridView1->EnableHeadersVisualStyles = false;
			this->dataGridView1->GridColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(38)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)));
			this->dataGridView1->Location = System::Drawing::Point(-4, 0);
			this->dataGridView1->MultiSelect = false;
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			dataGridViewCellStyle7->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(238)));
			dataGridViewCellStyle7->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle7->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle7->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dataGridView1->RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(837, 502);
			this->dataGridView1->TabIndex = 0;
			// 
			// Nazwaurzadzenia
			// 
			dataGridViewCellStyle2->BackColor = System::Drawing::Color::White;
			dataGridViewCellStyle2->ForeColor = System::Drawing::Color::Black;
			dataGridViewCellStyle2->SelectionBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)), static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->Nazwaurzadzenia->DefaultCellStyle = dataGridViewCellStyle2;
			this->Nazwaurzadzenia->FillWeight = 253.8071F;
			this->Nazwaurzadzenia->HeaderText = L"Nazwa urz�dzenia";
			this->Nazwaurzadzenia->Name = L"Nazwaurzadzenia";
			this->Nazwaurzadzenia->ReadOnly = true;
			this->Nazwaurzadzenia->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// Portszeregowy
			// 
			dataGridViewCellStyle3->ForeColor = System::Drawing::Color::Black;
			dataGridViewCellStyle3->SelectionBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)), static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->Portszeregowy->DefaultCellStyle = dataGridViewCellStyle3;
			this->Portszeregowy->FillWeight = 61.54825F;
			this->Portszeregowy->HeaderText = L"Port szeregowy";
			this->Portszeregowy->Name = L"Portszeregowy";
			this->Portszeregowy->ReadOnly = true;
			this->Portszeregowy->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// Kalibracja
			// 
			dataGridViewCellStyle4->ForeColor = System::Drawing::Color::Black;
			dataGridViewCellStyle4->SelectionBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)), static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->Kalibracja->DefaultCellStyle = dataGridViewCellStyle4;
			this->Kalibracja->FillWeight = 61.54825F;
			this->Kalibracja->HeaderText = L"Kalibracja";
			this->Kalibracja->Name = L"Kalibracja";
			this->Kalibracja->ReadOnly = true;
			this->Kalibracja->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// cp
			// 
			dataGridViewCellStyle5->ForeColor = System::Drawing::Color::Black;
			dataGridViewCellStyle5->SelectionBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)), static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->cp->DefaultCellStyle = dataGridViewCellStyle5;
			this->cp->FillWeight = 61.54825F;
			this->cp->HeaderText = L"Cg";
			this->cp->Name = L"cp";
			this->cp->ReadOnly = true;
			this->cp->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// Cpk
			// 
			dataGridViewCellStyle6->ForeColor = System::Drawing::Color::Black;
			dataGridViewCellStyle6->SelectionBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)), static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->Cpk->DefaultCellStyle = dataGridViewCellStyle6;
			this->Cpk->FillWeight = 61.54825F;
			this->Cpk->HeaderText = L"Cgk";
			this->Cpk->Name = L"Cpk";
			this->Cpk->ReadOnly = true;
			this->Cpk->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->button1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(57)),
				static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button1->Location = System::Drawing::Point(-4, 508);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(92, 32);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Dodaj";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &Ustawienia::button1_Click);
			// 
			// button2
			// 
			this->button2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->button2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(57)),
				static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button2->Location = System::Drawing::Point(94, 508);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(92, 32);
			this->button2->TabIndex = 1;
			this->button2->Text = L"Usu�";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += gcnew System::EventHandler(this, &Ustawienia::button2_Click);
			// 
			// button3
			// 
			this->button3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->button3->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(57)),
				static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->button3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button3->Location = System::Drawing::Point(192, 508);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(92, 32);
			this->button3->TabIndex = 1;
			this->button3->Text = L"Kalibruj";
			this->button3->UseVisualStyleBackColor = false;
			this->button3->Click += gcnew System::EventHandler(this, &Ustawienia::button3_Click);
			// 
			// button4
			// 
			this->button4->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button4->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(57)),
				static_cast<System::Int32>(static_cast<System::Byte>(121)));
			this->button4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button4->Location = System::Drawing::Point(741, 508);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(92, 32);
			this->button4->TabIndex = 1;
			this->button4->Text = L"Zamknij";
			this->button4->UseVisualStyleBackColor = false;
			this->button4->Click += gcnew System::EventHandler(this, &Ustawienia::button4_Click);
			// 
			// serialPort1
			// 
			this->serialPort1->DtrEnable = true;
			this->serialPort1->RtsEnable = true;
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Cursor = System::Windows::Forms::Cursors::Default;
			this->tabControl1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tabControl1->DrawMode = System::Windows::Forms::TabDrawMode::OwnerDrawFixed;
			this->tabControl1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->tabControl1->Location = System::Drawing::Point(0, 0);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(844, 573);
			this->tabControl1->TabIndex = 2;
			this->tabControl1->TabStop = false;
			this->tabControl1->DrawItem += gcnew System::Windows::Forms::DrawItemEventHandler(this, &Ustawienia::tabControl1_DrawItem);
			// 
			// tabPage1
			// 
			this->tabPage1->AutoScroll = true;
			this->tabPage1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(38)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)));
			this->tabPage1->Controls->Add(this->dataGridView1);
			this->tabPage1->Controls->Add(this->button1);
			this->tabPage1->Controls->Add(this->button2);
			this->tabPage1->Controls->Add(this->button3);
			this->tabPage1->Controls->Add(this->button4);
			this->tabPage1->ForeColor = System::Drawing::Color::White;
			this->tabPage1->Location = System::Drawing::Point(4, 27);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(836, 542);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"Urz�dzenia";
			// 
			// tabPage2
			// 
			this->tabPage2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(38)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)));
			this->tabPage2->Controls->Add(this->textBox6);
			this->tabPage2->Controls->Add(this->textBox5);
			this->tabPage2->Controls->Add(this->textBox4);
			this->tabPage2->Controls->Add(this->textBox3);
			this->tabPage2->Controls->Add(this->textBox2);
			this->tabPage2->Controls->Add(this->textBox1);
			this->tabPage2->Controls->Add(this->pictureBox4);
			this->tabPage2->Controls->Add(this->pictureBox3);
			this->tabPage2->Controls->Add(this->pictureBox2);
			this->tabPage2->Controls->Add(this->pictureBox1);
			this->tabPage2->Controls->Add(this->checkBox4);
			this->tabPage2->Controls->Add(this->checkBox3);
			this->tabPage2->Controls->Add(this->checkBox2);
			this->tabPage2->Controls->Add(this->checkBox1);
			this->tabPage2->Controls->Add(this->label4);
			this->tabPage2->Controls->Add(this->label3);
			this->tabPage2->Controls->Add(this->label6);
			this->tabPage2->Controls->Add(this->label5);
			this->tabPage2->Controls->Add(this->label2);
			this->tabPage2->Controls->Add(this->label1);
			this->tabPage2->ForeColor = System::Drawing::Color::White;
			this->tabPage2->Location = System::Drawing::Point(4, 27);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(836, 542);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"Alarmy";
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(43, 412);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(51, 24);
			this->textBox6->TabIndex = 6;
			this->textBox6->TabStop = false;
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(43, 281);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(51, 24);
			this->textBox5->TabIndex = 4;
			this->textBox5->TabStop = false;
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(122, 281);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(51, 24);
			this->textBox4->TabIndex = 5;
			this->textBox4->TabStop = false;
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(43, 150);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(51, 24);
			this->textBox3->TabIndex = 2;
			this->textBox3->TabStop = false;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(122, 150);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(51, 24);
			this->textBox2->TabIndex = 3;
			this->textBox2->TabStop = false;
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(43, 18);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(51, 24);
			this->textBox1->TabIndex = 1;
			this->textBox1->TabStop = false;
			// 
			// pictureBox4
			// 
			this->pictureBox4->Location = System::Drawing::Point(574, 412);
			this->pictureBox4->Name = L"pictureBox4";
			this->pictureBox4->Size = System::Drawing::Size(254, 122);
			this->pictureBox4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox4->TabIndex = 5;
			this->pictureBox4->TabStop = false;
			// 
			// pictureBox3
			// 
			this->pictureBox3->Location = System::Drawing::Point(574, 281);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(254, 122);
			this->pictureBox3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox3->TabIndex = 5;
			this->pictureBox3->TabStop = false;
			// 
			// pictureBox2
			// 
			this->pictureBox2->Location = System::Drawing::Point(579, 150);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(254, 122);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox2->TabIndex = 5;
			this->pictureBox2->TabStop = false;
			// 
			// pictureBox1
			// 
			this->pictureBox1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(38)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)));
			this->pictureBox1->Location = System::Drawing::Point(574, 19);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(254, 122);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox1->TabIndex = 5;
			this->pictureBox1->TabStop = false;
			// 
			// checkBox4
			// 
			this->checkBox4->AutoSize = true;
			this->checkBox4->Checked = true;
			this->checkBox4->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBox4->Location = System::Drawing::Point(22, 418);
			this->checkBox4->Name = L"checkBox4";
			this->checkBox4->Size = System::Drawing::Size(15, 14);
			this->checkBox4->TabIndex = 10;
			this->checkBox4->UseVisualStyleBackColor = true;
			this->checkBox4->CheckedChanged += gcnew System::EventHandler(this, &Ustawienia::checkBox4_CheckedChanged);
			// 
			// checkBox3
			// 
			this->checkBox3->AutoSize = true;
			this->checkBox3->Checked = true;
			this->checkBox3->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBox3->Location = System::Drawing::Point(22, 288);
			this->checkBox3->Name = L"checkBox3";
			this->checkBox3->Size = System::Drawing::Size(15, 14);
			this->checkBox3->TabIndex = 9;
			this->checkBox3->UseVisualStyleBackColor = true;
			this->checkBox3->CheckedChanged += gcnew System::EventHandler(this, &Ustawienia::checkBox3_CheckedChanged);
			// 
			// checkBox2
			// 
			this->checkBox2->AutoSize = true;
			this->checkBox2->Checked = true;
			this->checkBox2->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBox2->Location = System::Drawing::Point(22, 156);
			this->checkBox2->Name = L"checkBox2";
			this->checkBox2->Size = System::Drawing::Size(15, 14);
			this->checkBox2->TabIndex = 8;
			this->checkBox2->UseVisualStyleBackColor = true;
			this->checkBox2->CheckedChanged += gcnew System::EventHandler(this, &Ustawienia::checkBox2_CheckedChanged);
			// 
			// checkBox1
			// 
			this->checkBox1->AutoSize = true;
			this->checkBox1->Checked = true;
			this->checkBox1->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBox1->Location = System::Drawing::Point(22, 25);
			this->checkBox1->Name = L"checkBox1";
			this->checkBox1->Size = System::Drawing::Size(15, 14);
			this->checkBox1->TabIndex = 7;
			this->checkBox1->UseVisualStyleBackColor = true;
			this->checkBox1->CheckedChanged += gcnew System::EventHandler(this, &Ustawienia::checkBox1_CheckedChanged);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(100, 415);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(388, 18);
			this->label4->TabIndex = 0;
			this->label4->Text = L"kolejnych punkt�w przemiennie rosn�cych lub malej�cych";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(179, 284);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(286, 18);
			this->label3->TabIndex = 0;
			this->label3->Text = L"kolejnych punkt�w w strefie B lub poza ni�";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(100, 284);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(16, 18);
			this->label6->TabIndex = 0;
			this->label6->Text = L"z";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(100, 153);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(16, 18);
			this->label5->TabIndex = 0;
			this->label5->Text = L"z";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(179, 153);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(202, 18);
			this->label2->TabIndex = 0;
			this->label2->Text = L"kolejnych punkt�w w strefie A";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(100, 21);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(238, 18);
			this->label1->TabIndex = 0;
			this->label1->Text = L"punkt�w malej�cych lub rosn�cych";
			// 
			// Ustawienia
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
			this->AutoSize = true;
			this->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(38)),
				static_cast<System::Int32>(static_cast<System::Byte>(57)));
			this->ClientSize = System::Drawing::Size(844, 573);
			this->Controls->Add(this->tabControl1);
			this->ForeColor = System::Drawing::Color::White;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"Ustawienia";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Ustawienia";
			this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &Ustawienia::Ustawienia_FormClosed);
			this->Load += gcnew System::EventHandler(this, &Ustawienia::Ustawienia_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->tabControl1->ResumeLayout(false);
			this->tabPage1->ResumeLayout(false);
			this->tabPage2->ResumeLayout(false);
			this->tabPage2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion

	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {											//DODAJ
		UstawieniaSuw^ dodaj = gcnew UstawieniaSuw();
		dodaj->ShowDialog();
		if (dodaj->nowy != nullptr)
		{
			urzadzenia->Resize(urzadzenia, urzadzenia->Length + 1);
			urzadzenia[urzadzenia->Length - 1] = dodaj->nowy;

			dataGridView1->Rows->Add();
			dataGridView1->Rows[dataGridView1->RowCount - 1]->Cells[0]->Value = urzadzenia[urzadzenia->Length - 1]->nazwa;
			dataGridView1->Rows[dataGridView1->RowCount - 1]->Cells[1]->Value = urzadzenia[urzadzenia->Length - 1]->port;
			dataGridView1->Rows[dataGridView1->RowCount - 1]->Cells[2]->Value = "Brak";
		}
		delete dodaj;
	}

	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {										//USU�
		if (urzadzenia->Length > 0)
		{
			int wiersz = dataGridView1->SelectedRows[0]->Index;
			dataGridView1->Rows->Remove(dataGridView1->SelectedRows[0]);
			delete urzadzenia[wiersz];
			if (wiersz + 1 != urzadzenia->Length)
				urzadzenia->Copy(urzadzenia, wiersz + 1, urzadzenia, wiersz, urzadzenia->Length - (wiersz + 1));
			urzadzenia->Resize(urzadzenia, urzadzenia->Length - 1);
		}
	}
	private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {										//ZAMKNIJ
		this->Close();
	}
	private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {										//KALIBRUJ
		if (urzadzenia->Length>0)
		{
			int wiersz = dataGridView1->SelectedRows[0]->Index;
			Kalibracja::Kalibracja^ kalibracja = gcnew Kalibracja::Kalibracja();
			//urzadzenia[wiersz]->kalibracja = kalibracja;
			kalibracja->portszereg = urzadzenia[wiersz];
			kalibracja->ShowDialog();
			//urzadzenia[wiersz]->kalibracja = nullptr;

			if (kalibracja->ok && kalibracja->Cg != 0)
			{
				kalibracja->ok = false;
				dataGridView1->Rows[wiersz]->Cells[3]->Value = kalibracja->Cg;
				dataGridView1->Rows[wiersz]->Cells[4]->Value = kalibracja->Cgk;
				dataGridView1->Rows[wiersz]->Cells[2]->Value = kalibracja->textBox4->Text;
			}
			delete kalibracja;
		}

	}

	private: System::Void Ustawienia_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
		StreamWriter^ plik = gcnew StreamWriter("..\\emptyTest1\\niewiem.txt", 0, System::Text::Encoding::Default);
		StreamWriter^ plik2 = gcnew StreamWriter("..\\emptyTest1\\niewiem2.txt", 0, System::Text::Encoding::Default);
		int i = 0;
		for each (portszer::Portszeregowy^ var in urzadzenia)
		{
			plik->WriteLine(var->nazwa + " " + var->port + " " + dataGridView1->Rows[i]->Cells[2]->Value + " " + dataGridView1->Rows[i]->Cells[3]->Value + " " + dataGridView1->Rows[i]->Cells[3]->Value);
			i++;
		}
		plik->Close();
		if (checkBox1->Checked)
			plik2->Write("1");
		else
			plik2->Write("0");
		if (checkBox2->Checked)
			plik2->Write(" 1");
		else
			plik2->Write(" 0");
		if (checkBox3->Checked)
			plik2->Write(" 1");
		else
			plik2->Write(" 0");
		if (checkBox4->Checked)
			plik2->Write(" 1");
		else
			plik2->Write(" 0");
		plik2->Write(" " + textBox1->Text + " " + textBox3->Text + " " + textBox2->Text + " " + textBox5->Text + " " + textBox4->Text + " " + textBox6->Text);
		plik2->Close();
	}

	private: System::Void Ustawienia_Load(System::Object^  sender, System::EventArgs^  e) {
		//this->Icon = gcnew System::Drawing::Icon("..\\Suwmiarka\\Logo2.ico");
		pictureBox1->Image = Image::FromFile("..\\emptyTest1\\trend.png");
		pictureBox2->Image = Image::FromFile("..\\emptyTest1\\strefaA.png");
		pictureBox3->Image = Image::FromFile("..\\emptyTest1\\StrefaB.png");
		pictureBox4->Image = Image::FromFile("..\\emptyTest1\\maleduze.png");
	}

	private: System::Void checkBox1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		if (checkBox1->Checked)
		{
			textBox1->Enabled = true;
			label1->Enabled = true;
			pictureBox1->Enabled = true;

		}
		else
		{
			//maskedTextBox1->Text = "0";
			textBox1->Enabled = false;
			label1->Enabled = false;
			pictureBox1->Enabled = false;
		}
	}
	private: System::Void checkBox2_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		if (checkBox2->Checked)
		{
			textBox2->Enabled = true;
			textBox3->Enabled = true;
			label2->Enabled = true;
			label5->Enabled = true;
			pictureBox2->Enabled = true;
		}
		else
		{
			//maskedTextBox2->Text = "0";
			//maskedTextBox3->Text = "0";
			textBox2->Enabled = false;
			textBox3->Enabled = false;
			label2->Enabled = false;
			label5->Enabled = false;
			pictureBox2->Enabled = false;
		}
	}
	private: System::Void checkBox3_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		if (checkBox3->Checked)
		{
			textBox4->Enabled = true;
			textBox5->Enabled = true;
			label3->Enabled = true;
			label6->Enabled = true;
			pictureBox3->Enabled = true;
		}
		else
		{
			//maskedTextBox4->Text = "0";
			//maskedTextBox5->Text = "0";
			textBox4->Enabled = false;
			textBox5->Enabled = false;
			label3->Enabled = false;
			label6->Enabled = false;
			pictureBox3->Enabled = false;
		}
	}
	private: System::Void checkBox4_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		if (checkBox4->Checked)
		{
			textBox6->Enabled = true;
			label4->Enabled = true;
			pictureBox4->Enabled = true;
		}
		else
		{
			//maskedTextBox6->Text = "0";
			textBox6->Enabled = false;
			label4->Enabled = false;
			pictureBox4->Enabled = false;
		}
	}
	private: System::Void tabControl1_DrawItem(System::Object^  sender, System::Windows::Forms::DrawItemEventArgs^  e) {

		e->Graphics->FillRectangle(gcnew SolidBrush(System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(38)),
			static_cast<System::Int32>(static_cast<System::Byte>(57)))), e->Bounds);
		// Then draw the current tab button text 
		Rectangle paddedBounds = e->Bounds;
		paddedBounds.Inflate(-2, -2);
		e->Graphics->DrawString(tabControl1->TabPages[e->Index]->Text, tabControl1->Font, SystemBrushes::HighlightText, paddedBounds);

		Rectangle^ lasttabrect = tabControl1->GetTabRect(tabControl1->TabPages->Count - 1);
		RectangleF emptyspacerect = RectangleF(
			lasttabrect->X + lasttabrect->Width + tabControl1->Left,
			tabControl1->Top + lasttabrect->Y,
			tabControl1->Width - (lasttabrect->X + lasttabrect->Width),
			lasttabrect->Height);

		e->Graphics->FillRectangle(gcnew SolidBrush(System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(57)),
			static_cast<System::Int32>(static_cast<System::Byte>(121)))), emptyspacerect);

	}
	};
}
