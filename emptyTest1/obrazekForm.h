#pragma once

namespace emptyTest1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace System::Net;
	using namespace System::Net::Sockets;
	using namespace System::Text;
	using namespace System::Threading;
	using namespace System::Runtime::Remoting;
	using namespace System::Data::SqlClient;
	using namespace Npgsql;

	/// <summary>
	/// Summary for obrazekForm
	/// </summary>
	public ref class obrazekForm : public System::Windows::Forms::Form
	{
	public:
		obrazekForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~obrazekForm()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::PictureBox^  pictureBox1;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Location = System::Drawing::Point(30, 12);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(397, 323);
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			// 
			// obrazekForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(473, 378);
			this->Controls->Add(this->pictureBox1);
			this->Name = L"obrazekForm";
			this->Text = L"obrazekForm";
			this->Load += gcnew System::EventHandler(this, &obrazekForm::obrazekForm_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion

	private: NpgsqlConnection^ connection;
	private: NpgsqlCommand^ selectcomm;

	private: System::Void obrazekForm_Load(System::Object^  sender, System::EventArgs^  e) {
		connection = gcnew NpgsqlConnection("server=127.0.0.1; Port=5432; User Id=postgres; Password=tomek; Database=postgres");
		connection->Open();
		selectcomm = gcnew NpgsqlCommand("SELECT obraz FROM tmp  WHERE  id_kart = @id_kart;", connection);
		selectcomm->Parameters->Add("@id_kart", 1);
		DataSet^ ds = gcnew DataSet("Wymiary1");
		NpgsqlDataAdapter^ adapter = gcnew NpgsqlDataAdapter(selectcomm);
		adapter->Fill(ds);
		//array<Byte>^ kartaBin = Convert::ToByte(ds->Tables[0]->Rows[0][0]);


		Object^ SqlRetVal = nullptr;
		SqlRetVal = selectcomm->ExecuteScalar();

		array<Byte>^ rawData = gcnew array<Byte>(0);
		rawData = safe_cast<array<Byte>^>(SqlRetVal);

		MemoryStream^ memStream = gcnew MemoryStream();
		memStream->Write(rawData, 0, rawData->Length);
		Image^ kartaX = Image::FromStream(memStream);
		//Image^ kartaX = Image::FromStream((rawData));
		pictureBox1->Image = kartaX;

	}
	};
}
