#pragma once
#include "MyForm1.h"
#include "ObrobkaForm.h"
#include "PomiaryForm.h"
#include "Header_Form.h"
#include "KomentarzForm.h"
#include "Ustawienia.h"
#include "Portszeregowy.h"
#include "Wymiar.h"

//#include "MyForm2.h"

namespace emptyTest1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Data::SqlClient;
	using namespace Npgsql;
	using namespace System::Diagnostics;
	using namespace System::Threading::Tasks;
	using namespace portszer;
	using namespace Ustawienia1;


	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
		bool stopTemp = false;
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}

	private: Form^ okno_rysunku;
	public: System::Int32 id_operacji;
	private: String^ adrrRysTech;
	private: String^ adrrDokumentacji;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  plikToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ustawieniaToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  komunikacjaToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  mikrometrToolStripMenuItem;
	private: System::IO::Ports::SerialPort^  serialPort1;
	private: System::Windows::Forms::ToolStripMenuItem^  czytnikKod�wToolStripMenuItem;
	private: System::IO::Ports::SerialPort^  serialPort2;
	private: System::Windows::Forms::BindingSource^  bindingSource1;
	private: System::Windows::Forms::DataGridView^  dataGridView1;
private: System::IO::Ports::SerialPort^  serialPort3;
private: System::Windows::Forms::ToolStripMenuItem^  pirometrToolStripMenuItem;
private: System::Windows::Forms::TextBox^  textBox10;
private: System::Windows::Forms::TextBox^  textBox11;
private: System::Windows::Forms::GroupBox^  groupBox1;
private: System::Windows::Forms::Label^  label34;
private: System::Windows::Forms::TextBox^  textBox18;
private: System::Windows::Forms::TextBox^  textBox17;
private: System::Windows::Forms::TextBox^  textBox14;
private: System::Windows::Forms::Label^  label40;
private: System::Windows::Forms::Label^  label39;
private: System::Windows::Forms::Label^  label36;
private: System::Windows::Forms::TextBox^  textBox16;
private: System::Windows::Forms::Label^  label38;
private: System::Windows::Forms::TextBox^  textBox15;
private: System::Windows::Forms::TextBox^  textBox12;
private: System::Windows::Forms::Label^  label37;
private: System::Windows::Forms::GroupBox^  groupBox2;
private: System::Windows::Forms::PictureBox^  pictureBox2;
private: System::Windows::Forms::Label^  label33;
private: System::Windows::Forms::Label^  label32;
private: System::Windows::Forms::Label^  label31;
private: System::Windows::Forms::Label^  label30;
private: System::Windows::Forms::PictureBox^  pictureBox1;
private: System::Windows::Forms::TextBox^  textBox13;
private: System::Windows::Forms::Label^  label35;
private: System::Windows::Forms::Timer^  timer1;
private: System::Windows::Forms::Label^  label15;
private: System::Windows::Forms::Button^  button1;
private: System::Windows::Forms::GroupBox^  groupBox3;
private: System::Windows::Forms::Button^  button2;
private: System::Windows::Forms::Button^  button3;
private: System::Windows::Forms::Button^  button4;
private: System::Windows::Forms::Button^  button5;
private: System::Windows::Forms::Button^  button6;
private: System::Windows::Forms::GroupBox^  groupBox4;
private: System::Windows::Forms::PictureBox^  pictureBox3;
private: System::Windows::Forms::GroupBox^  groupBox5;
















	private: System::Windows::Forms::DataGridView^  dataGridView2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  textBox9;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::ComponentModel::IContainer^  components;
	protected:

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm::typeid));
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->plikToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->ustawieniaToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->komunikacjaToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->mikrometrToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->czytnikKod�wToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->pirometrToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->serialPort1 = (gcnew System::IO::Ports::SerialPort(this->components));
			this->serialPort2 = (gcnew System::IO::Ports::SerialPort(this->components));
			this->bindingSource1 = (gcnew System::Windows::Forms::BindingSource(this->components));
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->serialPort3 = (gcnew System::IO::Ports::SerialPort(this->components));
			this->textBox10 = (gcnew System::Windows::Forms::TextBox());
			this->textBox11 = (gcnew System::Windows::Forms::TextBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->textBox18 = (gcnew System::Windows::Forms::TextBox());
			this->textBox17 = (gcnew System::Windows::Forms::TextBox());
			this->textBox14 = (gcnew System::Windows::Forms::TextBox());
			this->label40 = (gcnew System::Windows::Forms::Label());
			this->label39 = (gcnew System::Windows::Forms::Label());
			this->label36 = (gcnew System::Windows::Forms::Label());
			this->textBox16 = (gcnew System::Windows::Forms::TextBox());
			this->textBox13 = (gcnew System::Windows::Forms::TextBox());
			this->label38 = (gcnew System::Windows::Forms::Label());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->textBox15 = (gcnew System::Windows::Forms::TextBox());
			this->textBox12 = (gcnew System::Windows::Forms::TextBox());
			this->label37 = (gcnew System::Windows::Forms::Label());
			this->label34 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->label31 = (gcnew System::Windows::Forms::Label());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->dataGridView2 = (gcnew System::Windows::Forms::DataGridView());
			this->Column1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBox9 = (gcnew System::Windows::Forms::TextBox());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->groupBox3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView2))->BeginInit();
			this->groupBox4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
			this->groupBox5->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->BackColor = System::Drawing::SystemColors::MenuBar;
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->plikToolStripMenuItem,
					this->ustawieniaToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Padding = System::Windows::Forms::Padding(12, 5, 0, 5);
			this->menuStrip1->Size = System::Drawing::Size(1860, 29);
			this->menuStrip1->TabIndex = 5;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// plikToolStripMenuItem
			// 
			this->plikToolStripMenuItem->Name = L"plikToolStripMenuItem";
			this->plikToolStripMenuItem->Size = System::Drawing::Size(38, 19);
			this->plikToolStripMenuItem->Text = L"Plik";
			// 
			// ustawieniaToolStripMenuItem
			// 
			this->ustawieniaToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->komunikacjaToolStripMenuItem });
			this->ustawieniaToolStripMenuItem->Name = L"ustawieniaToolStripMenuItem";
			this->ustawieniaToolStripMenuItem->Size = System::Drawing::Size(76, 19);
			this->ustawieniaToolStripMenuItem->Text = L"Ustawienia";
			// 
			// komunikacjaToolStripMenuItem
			// 
			this->komunikacjaToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->mikrometrToolStripMenuItem,
					this->czytnikKod�wToolStripMenuItem, this->pirometrToolStripMenuItem
			});
			this->komunikacjaToolStripMenuItem->Name = L"komunikacjaToolStripMenuItem";
			this->komunikacjaToolStripMenuItem->Size = System::Drawing::Size(143, 22);
			this->komunikacjaToolStripMenuItem->Text = L"Komunikacja";
			// 
			// mikrometrToolStripMenuItem
			// 
			this->mikrometrToolStripMenuItem->Name = L"mikrometrToolStripMenuItem";
			this->mikrometrToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->mikrometrToolStripMenuItem->Text = L"Mikrometr";
			this->mikrometrToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::mikrometrToolStripMenuItem_Click);
			// 
			// czytnikKod�wToolStripMenuItem
			// 
			this->czytnikKod�wToolStripMenuItem->Name = L"czytnikKod�wToolStripMenuItem";
			this->czytnikKod�wToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->czytnikKod�wToolStripMenuItem->Text = L"Czytnik kod�w";
			this->czytnikKod�wToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::czytnikKod�wToolStripMenuItem_Click);
			// 
			// pirometrToolStripMenuItem
			// 
			this->pirometrToolStripMenuItem->Name = L"pirometrToolStripMenuItem";
			this->pirometrToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->pirometrToolStripMenuItem->Text = L"Pirometr";
			this->pirometrToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::pirometrToolStripMenuItem_Click);
			// 
			// serialPort1
			// 
			this->serialPort1->DtrEnable = true;
			this->serialPort1->ReadBufferSize = 2048;
			this->serialPort1->RtsEnable = true;
			this->serialPort1->DataReceived += gcnew System::IO::Ports::SerialDataReceivedEventHandler(this, &MyForm::serialPort1_DataReceived);
			// 
			// serialPort2
			// 
			this->serialPort2->DtrEnable = true;
			this->serialPort2->RtsEnable = true;
			this->serialPort2->DataReceived += gcnew System::IO::Ports::SerialDataReceivedEventHandler(this, &MyForm::serialPort2_DataReceived);
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->BackgroundColor = System::Drawing::Color::White;
			this->dataGridView1->BorderStyle = System::Windows::Forms::BorderStyle::None;
			dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle2->BackColor = System::Drawing::SystemColors::Window;
			dataGridViewCellStyle2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			dataGridViewCellStyle2->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle2->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle2->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle2->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dataGridView1->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Location = System::Drawing::Point(281, 176);
			this->dataGridView1->Margin = System::Windows::Forms::Padding(6);
			this->dataGridView1->MultiSelect = false;
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->Size = System::Drawing::Size(1403, 250);
			this->dataGridView1->TabIndex = 10;
			this->dataGridView1->CellEndEdit += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MyForm::dataGridView1_CellEndEdit);
			this->dataGridView1->SelectionChanged += gcnew System::EventHandler(this, &MyForm::dataGridView1_SelectionChanged);
			this->dataGridView1->Sorted += gcnew System::EventHandler(this, &MyForm::dataGridView1_Sorted);
			this->dataGridView1->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// serialPort3
			// 
			this->serialPort3->BaudRate = 115200;
			this->serialPort3->DtrEnable = true;
			this->serialPort3->RtsEnable = true;
			this->serialPort3->DataReceived += gcnew System::IO::Ports::SerialDataReceivedEventHandler(this, &MyForm::serialPort3_DataReceived);
			// 
			// textBox10
			// 
			this->textBox10->BackColor = System::Drawing::SystemColors::Window;
			this->textBox10->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->textBox10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->textBox10->Location = System::Drawing::Point(481, 22);
			this->textBox10->Margin = System::Windows::Forms::Padding(6);
			this->textBox10->Multiline = true;
			this->textBox10->Name = L"textBox10";
			this->textBox10->ScrollBars = System::Windows::Forms::ScrollBars::Both;
			this->textBox10->Size = System::Drawing::Size(932, 85);
			this->textBox10->TabIndex = 22;
			this->textBox10->Text = L"Komunikat: ";
			this->textBox10->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// textBox11
			// 
			this->textBox11->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(51)),
				static_cast<System::Int32>(static_cast<System::Byte>(51)));
			this->textBox11->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->textBox11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->textBox11->Location = System::Drawing::Point(1425, 22);
			this->textBox11->Margin = System::Windows::Forms::Padding(6);
			this->textBox11->Multiline = true;
			this->textBox11->Name = L"textBox11";
			this->textBox11->Size = System::Drawing::Size(257, 85);
			this->textBox11->TabIndex = 23;
			this->textBox11->Text = L"Klasyfikacja eksportowa: ";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->textBox18);
			this->groupBox1->Controls->Add(this->textBox17);
			this->groupBox1->Controls->Add(this->textBox14);
			this->groupBox1->Controls->Add(this->label40);
			this->groupBox1->Controls->Add(this->label39);
			this->groupBox1->Controls->Add(this->label36);
			this->groupBox1->Controls->Add(this->textBox16);
			this->groupBox1->Controls->Add(this->textBox13);
			this->groupBox1->Controls->Add(this->label38);
			this->groupBox1->Controls->Add(this->label35);
			this->groupBox1->Controls->Add(this->textBox15);
			this->groupBox1->Controls->Add(this->textBox12);
			this->groupBox1->Controls->Add(this->label37);
			this->groupBox1->Controls->Add(this->label34);
			this->groupBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->groupBox1->ForeColor = System::Drawing::Color::Black;
			this->groupBox1->Location = System::Drawing::Point(15, 344);
			this->groupBox1->Margin = System::Windows::Forms::Padding(6);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Padding = System::Windows::Forms::Padding(6);
			this->groupBox1->Size = System::Drawing::Size(234, 498);
			this->groupBox1->TabIndex = 24;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Informacje";
			// 
			// textBox18
			// 
			this->textBox18->BackColor = System::Drawing::SystemColors::Window;
			this->textBox18->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox18->Location = System::Drawing::Point(10, 456);
			this->textBox18->Margin = System::Windows::Forms::Padding(6);
			this->textBox18->Name = L"textBox18";
			this->textBox18->Size = System::Drawing::Size(208, 25);
			this->textBox18->TabIndex = 5;
			this->textBox18->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// textBox17
			// 
			this->textBox17->BackColor = System::Drawing::SystemColors::Window;
			this->textBox17->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox17->Location = System::Drawing::Point(10, 392);
			this->textBox17->Margin = System::Windows::Forms::Padding(6);
			this->textBox17->Name = L"textBox17";
			this->textBox17->Size = System::Drawing::Size(208, 25);
			this->textBox17->TabIndex = 5;
			this->textBox17->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// textBox14
			// 
			this->textBox14->BackColor = System::Drawing::SystemColors::Window;
			this->textBox14->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox14->Location = System::Drawing::Point(12, 203);
			this->textBox14->Margin = System::Windows::Forms::Padding(6);
			this->textBox14->Name = L"textBox14";
			this->textBox14->Size = System::Drawing::Size(208, 25);
			this->textBox14->TabIndex = 5;
			this->textBox14->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// label40
			// 
			this->label40->AutoSize = true;
			this->label40->ForeColor = System::Drawing::Color::Black;
			this->label40->Location = System::Drawing::Point(8, 425);
			this->label40->Margin = System::Windows::Forms::Padding(6, 0, 6, 0);
			this->label40->Name = L"label40";
			this->label40->Size = System::Drawing::Size(134, 26);
			this->label40->TabIndex = 4;
			this->label40->Text = L"Nr programu";
			// 
			// label39
			// 
			this->label39->AutoSize = true;
			this->label39->ForeColor = System::Drawing::Color::Black;
			this->label39->Location = System::Drawing::Point(6, 359);
			this->label39->Margin = System::Windows::Forms::Padding(6, 0, 6, 0);
			this->label39->Name = L"label39";
			this->label39->Size = System::Drawing::Size(160, 26);
			this->label39->TabIndex = 4;
			this->label39->Text = L"Edycja operacji";
			// 
			// label36
			// 
			this->label36->AutoSize = true;
			this->label36->ForeColor = System::Drawing::Color::Black;
			this->label36->Location = System::Drawing::Point(6, 170);
			this->label36->Margin = System::Windows::Forms::Padding(6, 0, 6, 0);
			this->label36->Name = L"label36";
			this->label36->Size = System::Drawing::Size(106, 26);
			this->label36->TabIndex = 4;
			this->label36->Text = L"Nr detalu:";
			// 
			// textBox16
			// 
			this->textBox16->BackColor = System::Drawing::SystemColors::Window;
			this->textBox16->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox16->Location = System::Drawing::Point(10, 330);
			this->textBox16->Margin = System::Windows::Forms::Padding(6);
			this->textBox16->Name = L"textBox16";
			this->textBox16->Size = System::Drawing::Size(208, 25);
			this->textBox16->TabIndex = 3;
			this->textBox16->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// textBox13
			// 
			this->textBox13->BackColor = System::Drawing::SystemColors::Window;
			this->textBox13->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox13->Location = System::Drawing::Point(12, 141);
			this->textBox13->Margin = System::Windows::Forms::Padding(6);
			this->textBox13->Name = L"textBox13";
			this->textBox13->Size = System::Drawing::Size(208, 25);
			this->textBox13->TabIndex = 3;
			this->textBox13->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// label38
			// 
			this->label38->AutoSize = true;
			this->label38->ForeColor = System::Drawing::Color::Black;
			this->label38->Location = System::Drawing::Point(8, 297);
			this->label38->Margin = System::Windows::Forms::Padding(6, 0, 6, 0);
			this->label38->Name = L"label38";
			this->label38->Size = System::Drawing::Size(161, 26);
			this->label38->TabIndex = 2;
			this->label38->Text = L"Nazwa operacji";
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->ForeColor = System::Drawing::Color::Black;
			this->label35->Location = System::Drawing::Point(8, 108);
			this->label35->Margin = System::Windows::Forms::Padding(6, 0, 6, 0);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(123, 26);
			this->label35->TabIndex = 2;
			this->label35->Text = L"Nr operacji:";
			// 
			// textBox15
			// 
			this->textBox15->BackColor = System::Drawing::SystemColors::Window;
			this->textBox15->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox15->Location = System::Drawing::Point(10, 266);
			this->textBox15->Margin = System::Windows::Forms::Padding(6);
			this->textBox15->Name = L"textBox15";
			this->textBox15->Size = System::Drawing::Size(208, 25);
			this->textBox15->TabIndex = 1;
			this->textBox15->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// textBox12
			// 
			this->textBox12->BackColor = System::Drawing::SystemColors::Window;
			this->textBox12->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox12->Location = System::Drawing::Point(12, 77);
			this->textBox12->Margin = System::Windows::Forms::Padding(6);
			this->textBox12->Name = L"textBox12";
			this->textBox12->Size = System::Drawing::Size(208, 25);
			this->textBox12->TabIndex = 1;
			this->textBox12->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// label37
			// 
			this->label37->AutoSize = true;
			this->label37->ForeColor = System::Drawing::Color::Black;
			this->label37->Location = System::Drawing::Point(8, 234);
			this->label37->Margin = System::Windows::Forms::Padding(6, 0, 6, 0);
			this->label37->Name = L"label37";
			this->label37->Size = System::Drawing::Size(124, 26);
			this->label37->TabIndex = 0;
			this->label37->Text = L"Stanowisko";
			// 
			// label34
			// 
			this->label34->AutoSize = true;
			this->label34->ForeColor = System::Drawing::Color::Black;
			this->label34->Location = System::Drawing::Point(6, 44);
			this->label34->Margin = System::Windows::Forms::Padding(6, 0, 6, 0);
			this->label34->Name = L"label34";
			this->label34->Size = System::Drawing::Size(87, 26);
			this->label34->TabIndex = 0;
			this->label34->Text = L"Nr serii:";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->pictureBox2);
			this->groupBox2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->groupBox2->Location = System::Drawing::Point(281, 516);
			this->groupBox2->Margin = System::Windows::Forms::Padding(6);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Padding = System::Windows::Forms::Padding(6);
			this->groupBox2->Size = System::Drawing::Size(345, 309);
			this->groupBox2->TabIndex = 25;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Rysunek";
			// 
			// pictureBox2
			// 
			this->pictureBox2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox2.Image")));
			this->pictureBox2->Location = System::Drawing::Point(11, 22);
			this->pictureBox2->Margin = System::Windows::Forms::Padding(6);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(322, 278);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox2->TabIndex = 0;
			this->pictureBox2->TabStop = false;
			this->pictureBox2->Click += gcnew System::EventHandler(this, &MyForm::pictureBox2_Click);
			this->pictureBox2->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MyForm::pictureBox2_Paint);
			// 
			// label33
			// 
			this->label33->AutoSize = true;
			this->label33->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label33->Location = System::Drawing::Point(107, 297);
			this->label33->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(0, 26);
			this->label33->TabIndex = 37;
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label32->Location = System::Drawing::Point(75, 258);
			this->label32->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(82, 26);
			this->label32->TabIndex = 36;
			this->label32->Text = L"label32";
			// 
			// label31
			// 
			this->label31->AutoSize = true;
			this->label31->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label31->Location = System::Drawing::Point(39, 221);
			this->label31->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label31->Name = L"label31";
			this->label31->Size = System::Drawing::Size(82, 26);
			this->label31->TabIndex = 35;
			this->label31->Text = L"label31";
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label30->Location = System::Drawing::Point(75, 179);
			this->label30->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(82, 26);
			this->label30->TabIndex = 34;
			this->label30->Text = L"label30";
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(15, 35);
			this->pictureBox1->Margin = System::Windows::Forms::Padding(6);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(458, 120);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox1->TabIndex = 33;
			this->pictureBox1->TabStop = false;
			// 
			// timer1
			// 
			this->timer1->Interval = 1000;
			this->timer1->Tick += gcnew System::EventHandler(this, &MyForm::timer1_Tick);
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 20.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label15->ForeColor = System::Drawing::Color::White;
			this->label15->Location = System::Drawing::Point(1740, 128);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(120, 31);
			this->label15->TabIndex = 38;
			this->label15->Text = L"12:00:00";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(70, 883);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(179, 71);
			this->button1->TabIndex = 39;
			this->button1->Text = L"Dokumentacja PDF";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click_1);
			this->button1->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->dataGridView2);
			this->groupBox3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->groupBox3->Location = System::Drawing::Point(1084, 516);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(345, 309);
			this->groupBox3->TabIndex = 41;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Narz�dzia pomiarowe";
			// 
			// dataGridView2
			// 
			this->dataGridView2->AllowUserToAddRows = false;
			this->dataGridView2->AllowUserToDeleteRows = false;
			this->dataGridView2->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView2->BackgroundColor = System::Drawing::Color::White;
			this->dataGridView2->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView2->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(1) { this->Column1 });
			this->dataGridView2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->dataGridView2->Location = System::Drawing::Point(3, 22);
			this->dataGridView2->MultiSelect = false;
			this->dataGridView2->Name = L"dataGridView2";
			this->dataGridView2->ReadOnly = true;
			this->dataGridView2->RowHeadersVisible = false;
			this->dataGridView2->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
			this->dataGridView2->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView2->Size = System::Drawing::Size(339, 284);
			this->dataGridView2->TabIndex = 0;
			this->dataGridView2->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MyForm::dataGridView2_CellClick);
			this->dataGridView2->SelectionChanged += gcnew System::EventHandler(this, &MyForm::dataGridView2_SelectionChanged);
			this->dataGridView2->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// Column1
			// 
			this->Column1->HeaderText = L"Urz�dzenie pomiarowe";
			this->Column1->Name = L"Column1";
			this->Column1->ReadOnly = true;
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(331, 883);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(190, 75);
			this->button2->TabIndex = 42;
			this->button2->Text = L"START operacji";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			this->button2->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(605, 889);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(184, 69);
			this->button3->TabIndex = 43;
			this->button3->Text = L"STOP operacji";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			this->button3->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(1312, 889);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(180, 70);
			this->button4->TabIndex = 44;
			this->button4->Text = L"Dodaj Komentarz";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &MyForm::button4_Click);
			this->button4->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(854, 888);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(180, 70);
			this->button5->TabIndex = 44;
			this->button5->Text = L"Obr�bka";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &MyForm::button5_Click);
			this->button5->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(1084, 889);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(180, 70);
			this->button6->TabIndex = 44;
			this->button6->Text = L"Ustawienia pomiar�w";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &MyForm::button6_Click_1);
			this->button6->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->pictureBox3);
			this->groupBox4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->groupBox4->Location = System::Drawing::Point(689, 516);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(345, 309);
			this->groupBox4->TabIndex = 41;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"SPC";
			// 
			// pictureBox3
			// 
			this->pictureBox3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox3.Image")));
			this->pictureBox3->Location = System::Drawing::Point(14, 22);
			this->pictureBox3->Margin = System::Windows::Forms::Padding(6);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(322, 278);
			this->pictureBox3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox3->TabIndex = 0;
			this->pictureBox3->TabStop = false;
			this->pictureBox3->Click += gcnew System::EventHandler(this, &MyForm::pictureBox3_Click);
			this->pictureBox3->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MyForm::pictureBox2_Paint);
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->textBox1);
			this->groupBox5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->groupBox5->Location = System::Drawing::Point(1485, 513);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(345, 309);
			this->groupBox5->TabIndex = 41;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"Monitorowanie obr�bki";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->ForeColor = System::Drawing::Color::Black;
			this->label1->Location = System::Drawing::Point(713, 452);
			this->label1->Margin = System::Windows::Forms::Padding(6, 0, 6, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(239, 26);
			this->label1->TabIndex = 6;
			this->label1->Text = L"Pomiar ze sprawdzianu";
			// 
			// textBox9
			// 
			this->textBox9->BackColor = System::Drawing::SystemColors::Window;
			this->textBox9->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox9->Location = System::Drawing::Point(964, 452);
			this->textBox9->Margin = System::Windows::Forms::Padding(6);
			this->textBox9->Name = L"textBox9";
			this->textBox9->Size = System::Drawing::Size(208, 25);
			this->textBox9->TabIndex = 5;
			this->textBox9->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(26, 35);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(155, 248);
			this->textBox1->TabIndex = 2;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(12, 25);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoScroll = true;
			this->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(51)), static_cast<System::Int32>(static_cast<System::Byte>(102)),
				static_cast<System::Int32>(static_cast<System::Byte>(153)));
			this->ClientSize = System::Drawing::Size(1480, 1013);
			this->Controls->Add(this->textBox9);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->groupBox5);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->label15);
			this->Controls->Add(this->label33);
			this->Controls->Add(this->label32);
			this->Controls->Add(this->label31);
			this->Controls->Add(this->label30);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->textBox11);
			this->Controls->Add(this->textBox10);
			this->Controls->Add(this->dataGridView1);
			this->Controls->Add(this->menuStrip1);
			this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->MainMenuStrip = this->menuStrip1;
			this->Margin = System::Windows::Forms::Padding(6);
			this->Name = L"MyForm";
			this->Text = L" ";
			this->WindowState = System::Windows::Forms::FormWindowState::Maximized;
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &MyForm::MyForm_FormClosing);
			this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &MyForm::MyForm_FormClosed);
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->groupBox3->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView2))->EndInit();
			this->groupBox4->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private: NpgsqlConnection^ connection;
private: NpgsqlCommand^ selectcomm;
private: NpgsqlCommand^ selectcomm2;
private: NpgsqlCommand^ inscomm;
private: NpgsqlCommand^ updacom;
private: NpgsqlCommand^ delcomm;
private: NpgsqlTransaction^ transaction;
private: String^ CDNumber;
public: Przekazywane^ odbierane;
private: Double pomiar;
		 Double pomiar_skom;
		 array<Wymiar1::Wymiar^>^ klasywymiary = gcnew array<Wymiar1::Wymiar^>(0);							//tablica wymiar�w
		 Double temperatura=30;																				//TOMEK trzeba tu co� wpisa� z bazy
		 Double wspolczynnik=0.00003;																		//TOMEK trzeba tu co� wpisa� z bazy
		 Ustawienia^ ustawienia;
		 


private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
//		MyForm2^ newForm = gcnew MyForm2();
//		newForm->ShowDialog();
		//label9->Text = newForm->textBox1->Text;
	}
private: System::Void mikrometrToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		MyForm1^ form2 = gcnew MyForm1();

		array<Object^>^ objectArrayCombo = serialPort1->GetPortNames();
		
		form2->comboBox1->Items->AddRange(objectArrayCombo);
		form2->ShowDialog();
		//label9->Text = form2->comboBox1->Text;
		

		if (form2->DialogResult == System::Windows::Forms::DialogResult::OK && !(serialPort1->IsOpen)) {
			try
			{
				serialPort1->PortName = form2->comboBox1->Text;
				serialPort1->Open();
				//label4->Text = Convert::ToString(serialPort1->PortName);
			}
			catch (System::ArgumentException^ e)
			{
				MessageBox::Show("Wybierz port COM");
			}
		}
			if (form2->DialogResult == System::Windows::Forms::DialogResult::Abort && serialPort1->IsOpen) {
				try
				{
					serialPort1->Close();
				}
				catch (System::ArgumentException^ e)
				{
					MessageBox::Show("Komunikacja prawdopodobnie ju� nie dzia�a.");
				}
			}
		  
	}
private: System::Void czytnikKod�wToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	MyForm1^ form3 = gcnew MyForm1();

	array<Object^>^ objectArrayCombo = serialPort2->GetPortNames();

	form3->comboBox1->Items->AddRange(objectArrayCombo);
	form3->ShowDialog();
	//label27->Text = form3->comboBox1->Text;


	if (form3->DialogResult == System::Windows::Forms::DialogResult::OK && !(serialPort2->IsOpen)) {
		try
		{
			serialPort2->PortName = form3->comboBox1->Text;
			serialPort2->Open();
			//label27->Text = Convert::ToString(serialPort2->PortName);
		}
		catch (System::ArgumentException^ e)
		{
			MessageBox::Show("Wybierz port COM");
		}
	}
		if (form3->DialogResult == System::Windows::Forms::DialogResult::Abort && serialPort2->IsOpen) {
			try
			{
				serialPort2->Close();

			}
			catch (System::ArgumentException^ e)
			{
				MessageBox::Show("Komunikacja prawdopodobnie ju� nie dzia�a.");
			}
		}
	
}
private: System::Void pirometrToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	MyForm1^ form4 = gcnew MyForm1();

	array<Object^>^ objectArrayCombo = serialPort3->GetPortNames();

	form4->comboBox1->Items->AddRange(objectArrayCombo);
	form4->comboBox2->Text = "115200";
	form4->ShowDialog();
	//label29->Text = form4->comboBox1->Text;


	if (form4->DialogResult == System::Windows::Forms::DialogResult::OK && !(serialPort3->IsOpen)) {
		try
		{
			serialPort3->PortName = form4->comboBox1->Text;
			serialPort3->BaudRate = System::Int32::Parse(form4->comboBox2->Text);
			serialPort3->Open();
			//label29->Text = Convert::ToString(serialPort3->PortName);
			System::Threading::Thread::Sleep(15);
			
			sendForTemp();
		}
		catch (System::ArgumentException^ e)
		{
			MessageBox::Show("Wybierz port COM");
		}
	}
	if (form4->DialogResult == System::Windows::Forms::DialogResult::Abort && serialPort2->IsOpen) {
		try
		{
			serialPort3->Close();

		}
		catch (System::ArgumentException^ e)
		{
			MessageBox::Show("Komunikacja prawdopodobnie ju� nie dzia�a.");
		}
	}
}

private: System::Void wyswietl(String^ data1) {
	data1 = data1->Replace(".", ",");
	System::Single data = System::Single::Parse(data1);
	//textBox1->AppendText(Convert::ToString(data) + System::Environment::NewLine);
	//label11->Text = data.ToString();
	//porownanie(label7->Text, label9->Text, label11->Text);
} 
private: System::Void wyswietlKody(String^ data1) {
	//textBox2->AppendText(data1 + System::Environment::NewLine);
	//label25->Text = data1;
	/*
	for ( Int16 i = 0; i < dataGridView1->Rows->Count; i++)
	{
		if (dataGridView1->Rows[i]->Cells[1]->Value->ToString() == data1)
		{
			dataGridView1->Rows[i]->Selected = true;
			break;
		}
	}*//**/

}
private: delegate void wyswietlDelegat(String^ data);

private: System::Void serialPort1_DataReceived(System::Object^  sender, System::IO::Ports::SerialDataReceivedEventArgs^  e) {
	wyswietlDelegat^ wyswDelegat = gcnew wyswietlDelegat(this, &MyForm::wyswietl);
	//System::Int32^ dataLenght = serialPort1->BytesToRead;
	serialPort1->ReadTo("A");
	String^ data = serialPort1->ReadExisting();
	while (data->Length != 10)
	{
		data += serialPort1->ReadExisting();
	}
	serialPort1->ReadExisting();
	
	this->Invoke(wyswDelegat, gcnew array <System::Object^>(1) { data });
}

private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {
	connection = gcnew NpgsqlConnection("server=127.0.0.1; Port=5432; User Id=postgres; Password=tomek; Database=postgres");
	label30->Text =  odbierane->nazwa_obrabiarki;
	label31->Text = odbierane->uzytkownik;
	label32->Text = odbierane->stanowisko_operatora;
	label33->Text = odbierane->informacja;
	textBox11->Text = odbierane->klas_export;
	pobranieInformacji();

	odswiez();
	rysuj_kolo();
	timer1->Start();
	zaladowanie_ustawien();
	urzadzenia();
	zaladowanie_wymiarow();
	try
	{
		if (dataGridView1->CurrentCell->RowIndex <klasywymiary->Length)
		{
			pictureBox3->Image = klasywymiary[dataGridView1->CurrentCell->RowIndex]->karta;
		}
	}
	catch (Exception^ e)
	{
		MessageBox::Show(e->Message, "B��d", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
	
	//serialPort3->Open();  // port od pmaru temp    dorob mu kienko
	//if (serialPort3->IsOpen) {
	//	label29->Text = serialPort3->PortName;
	//}
	//System::Threading::Thread::Sleep(2000);

}
private: System::Void odswiez() {
	//label33->Text = Convert::ToString(id_operacji);
		//selectcomm = gcnew NpgsqlCommand("SELECT numer, kod, nominalny, maksymalny, minimalny, aktualny, nazwa, temperatura FROM zamowienia;", connection);
		//selectcomm = gcnew NpgsqlCommand("SELECT wymiary.nr_cechy, wymiary.nazwa_cechy, kat_procedur_probkowania.nazwa, wymiary.wartosc_zmierzona, wymiary.wartosc_min, wymiary.wartosc_max, wymiary.wprowadzajacy, wymiary.mqi, wymiary.kpc, wymiary.sprawdzony, wymiary.zgodny, wymiary.id_wymiaru, wymiary.procedura_probkowania FROM wymiary LEFT JOIN kat_procedur_probkowania ON wymiary.procedura_probkowania = kat_procedur_probkowania.id_parametru WHERE id_operacji = 1;", connection);
		selectcomm = gcnew NpgsqlCommand("SELECT kat_wymiarow.nr_cechy, kat_wymiarow.nazwa_wymiaru, kat_procedur_probkowania.nazwa, wymiary.wartosc_zmierzona, kat_wymiarow.wartosc_min, kat_wymiarow.wartosc_max, wymiary.wprowadzajacy, kat_wymiarow.mqi, kat_wymiarow.kpc, wymiary.sprawdzony, wymiary.zgodny, wymiary.id_wymiaru, kat_wymiarow.procedura_probkowania FROM wymiary LEFT JOIN kat_wymiarow ON wymiary.id_cechy = kat_wymiarow.id_wymiaru JOIN kat_procedur_probkowania ON kat_wymiarow.procedura_probkowania = kat_procedur_probkowania.id_parametru WHERE id_operacji = @id_operacji;", connection);
		selectcomm->Parameters->Add("@id_operacji", odbierane->id_operacji);
		DataSet^ ds = gcnew DataSet("Wymiary");
		NpgsqlDataAdapter^ adapter = gcnew NpgsqlDataAdapter(selectcomm);
		adapter->Fill(ds);
		bindingSource1->DataSource = ds->Tables[0];
		dataGridView1->DataSource = bindingSource1;
		dataGridView1->Columns["nr_cechy"]->Width = 80;
		dataGridView1->Columns["nr_cechy"]->HeaderText = "Nrumer";
		dataGridView1->Columns["nazwa_wymiaru"]->Width = 320;
		dataGridView1->Columns["nazwa_wymiaru"]->HeaderText = "Nazwa cechy";
		dataGridView1->Columns["nazwa"]->Width = 100;
		dataGridView1->Columns["nazwa"]->HeaderText = "Pr�bk.";
		dataGridView1->Columns["wartosc_zmierzona"]->Width = 170;
		dataGridView1->Columns["wartosc_zmierzona"]->HeaderText = "Warto��";
		dataGridView1->Columns["wartosc_max"]->Width = 100;
		dataGridView1->Columns["wartosc_max"]->HeaderText = "Max";
		dataGridView1->Columns["wartosc_min"]->Width = 100;
		dataGridView1->Columns["wartosc_min"]->HeaderText = "Min";
		dataGridView1->Columns["wprowadzajacy"]->Width = 150;
		dataGridView1->Columns["wprowadzajacy"]->HeaderText = "Wprowadz.";
		dataGridView1->Columns["mqi"]->Width = 60;
		dataGridView1->Columns["mqi"]->HeaderText = "MQI";
		dataGridView1->Columns["kpc"]->Width = 60;
		dataGridView1->Columns["kpc"]->HeaderText = "KPC";
		dataGridView1->Columns["sprawdzony"]->Width = 110;
		dataGridView1->Columns["sprawdzony"]->HeaderText = "Sprawdz.";
		dataGridView1->Columns["zgodny"]->Width = 110;
		dataGridView1->Columns["zgodny"]->HeaderText = "Zgodny";
		dataGridView1->Columns[11]->Visible = false;
		dataGridView1->Columns[12]->Visible = false;

		kolorowanieDataGridView();
		



		//dataGridView1->Rows[1]->Selected = true;
		/*
		DataRowView^ SelectedRow;
		SelectedRow = (DataRowView^)(dataGridView1->Rows->Co);
		CDNumber = SelectedRow[0]->ToString();
		label1->Text = SelectedRow[0]->ToString();*/
		// = true;
	}



private: System::Void serialPort2_DataReceived(System::Object^  sender, System::IO::Ports::SerialDataReceivedEventArgs^  e) {
	wyswietlDelegat^ wyswDelegat2 = gcnew wyswietlDelegat(this, &MyForm::wyswietlKody);
	//System::Int32^ dataLenght = serialPort2->BytesToRead;
	String^ data = serialPort2->ReadExisting();

	this->Invoke(wyswDelegat2, gcnew array <System::Object^>(1) { data });
}

private: System::Void dataGridView1_SelectionChanged(System::Object^  sender, System::EventArgs^  e) {
	DataRowView^ SelectedRow;
	SelectedRow = (DataRowView^)(bindingSource1->Current);
	if (bindingSource1->Current != nullptr)					// czy cokolwiek jest w tabeli
	{
		CDNumber = SelectedRow[0]->ToString();
															//textBox3->Text = SelectedRow[3]->ToString();
		//label5->Text = SelectedRow[2]->ToString();			//pobranie wartosci nominalnej do tableki
		//label9->Text = SelectedRow[3]->ToString();			//pobranie warto�ci maksymalnej do tableki
		//label7->Text = SelectedRow[4]->ToString();			//pobranie warto�ci minimalnej do tabelki
		//label25->Text = SelectedRow[1]->ToString();			//pobranie kodu do tabelki
		
		//pobranie_SPC(1112);		
	}
	//try
	//{
	if (dataGridView1->CurrentCell->RowIndex <klasywymiary->Length)
	{
		pictureBox3->Image = klasywymiary[dataGridView1->CurrentCell->RowIndex]->karta;
		if (klasywymiary[dataGridView1->CurrentCell->RowIndex]->urzadzenie != "")
		{
			dataGridView2->Rows[klasywymiary[dataGridView1->CurrentCell->RowIndex]->nr_urzadzenia]->Selected = true;
		}
	}

	///********************  pobieranie wartosci dla tego samego wymiaru w innych detalach *****************/
	//PomiaryWykonane^ wykonanePomiary = gcnew PomiaryWykonane();										// tworzony jest nowy obiekt
	//Int32 nrZlecenia = Convert::ToInt32(textBox12->Text);
	//Int32 nrOperacji = Convert::ToInt32(textBox13->Text);
	//Int32 nrCzesci = Convert::ToInt32(textBox14->Text);
	//Int32 nrCechyTomek = Convert::ToDouble(dataGridView1->CurrentRow->Cells["nr_cechy"]->Value);
	//array<Double>^ dt = wykonanePomiary->pobierzPomiary(nrCechyTomek, nrOperacji, nrZlecenia);		// metoda zwraca tablice Doubli
	//for each (Double var in dt)
	//{
	//	textBox1->AppendText(var.ToString() + Environment::NewLine);								// do textboxa s� wpisywane wszystkie wymiary 
	//}
	/////////////////////////////////// KONIEC /////////////////////////////////////////////////

	//}
	//catch (Exception^ e)
	//{ }
}


private: System::Void porownanie(String^ min, String^max, String^ aktualna) {
	min = min->Replace(".", ",");
	max = max->Replace(".", ",");
	aktualna = aktualna->Replace(".", ",");

	if (aktualna == "") aktualna = "0";

	if (Single::Parse(min) <= Single::Parse(aktualna) && Single::Parse(aktualna) <= Single::Parse(max))
	{
		//label13->Text = "OK";
		//label13->BackColor = System::Drawing::Color::Green;
		//label13->Refresh();
	}
	else
	{
		//label13->Text = "NO";
		//label13->BackColor = System::Drawing::Color::Red;
		//label13->Refresh();
	}
	
	//label13->Refresh();
	Application::DoEvents();
}


		 // wyswietlanie temperatury
public: System::Void sendForTemp() {
	array<Byte>^ toSend = { 1,1 };
	serialPort3->Write(toSend, 0, 2);
}
private: System::Void wyswietlTemp(String^ data) {
	//label28->Text=data;
	
	if (!stopTemp)
	{
		sendForTemp();
	}
	
	
	
}
private: delegate void wyswietlDelegatTemperatury(String^ data);


private: System::Void MyForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
	
		System::Windows::Forms::DialogResult odp_konczenia =  MessageBox::Show("Czy napewno chcesz zako�czy�?", "Zako�czy�?", MessageBoxButtons::YesNo, MessageBoxIcon::Question );
		switch (odp_konczenia)
		{
		case System::Windows::Forms::DialogResult::Yes:
			e->Cancel = false;
			break;
		case System::Windows::Forms::DialogResult::No:
			e->Cancel = true;
			break;
		default:
			break;
		}

}
private: System::Void MyForm_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
	timer1->Stop();
	//serialPort1->Close();
	//serialPort2->Close();
	if (serialPort3->IsOpen)
		stopTemp = true;
	for each (Portszeregowy^ var in ustawienia->urzadzenia)
	{
		delete var;
	}
}

private: System::Void serialPort3_DataReceived(System::Object^  sender, System::IO::Ports::SerialDataReceivedEventArgs^  e) {
	wyswietlDelegatTemperatury^ wyswDelegatTemp = gcnew wyswietlDelegatTemperatury(this, &MyForm::wyswietlTemp);

	array<Byte>^ temp = { 0x0,0x0,0x0 };
	for (Byte i = 0; i < 3; i++)
	{
		temp[i] = serialPort3->ReadByte();
	}
	serialPort3->ReadExisting();				//odczytanie wszystkiego co zosta�o
	Byte CRC = temp[0] ^ temp[1];				//przeliczenie sumy kontrolnej

	Single data1 = static_cast<Single>(temp[0]);
	Single data2 = static_cast<Single>(temp[1]);
	Single data3 = (((temp[0] << 8) + data2) - 1000) / 10;
	temperatura = data3;
	String^ data = data3.ToString();
	//String^ data = temp[2].ToString();	
	if (CRC != temp[2])
	{
		data = "Error";
		MessageBox::Show("B��d odczytu pirometru, uruchom go ponownie");
	}

	this->Invoke(wyswDelegatTemp, gcnew array <System::Object^>(1) { data });
	
	
}




		 /////////////////////// Rysowanie////////////////////////////////
private: System::Void rysuj_kolo() {
	Graphics^ g1 = this->CreateGraphics();
	g1->SmoothingMode = System::Drawing::Drawing2D::SmoothingMode::AntiAlias;
	SolidBrush^ pioro = gcnew SolidBrush(System::Drawing::Color::White);
	Pen^ pioro_linia = gcnew Pen(System::Drawing::Color::White);
	Point^ punkt;
	punkt = label30->Location;
	g1->FillPie(pioro, System::Convert::ToInt16(punkt->X) - 25, System::Convert::ToInt16(punkt->Y) + 2, 20, 20, 0, 360);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8, System::Convert::ToInt16(punkt->Y) + 2 + 8, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20 + label30->Width, System::Convert::ToInt16(punkt->Y) + 8 + 20);

	punkt = label31->Location;
	g1->FillPie(pioro, System::Convert::ToInt16(punkt->X) - 25, System::Convert::ToInt16(punkt->Y) + 2, 20, 20, 0, 360);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8, System::Convert::ToInt16(punkt->Y) + 2 + 8, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20 + label31->Width, System::Convert::ToInt16(punkt->Y) + 8 + 20);

	punkt = label32->Location;
	g1->FillPie(pioro, System::Convert::ToInt16(punkt->X) - 25, System::Convert::ToInt16(punkt->Y) + 2, 20, 20, 0, 360);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8, System::Convert::ToInt16(punkt->Y) + 2 + 8, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20 + label32->Width, System::Convert::ToInt16(punkt->Y) + 8 + 20);

	punkt = label33->Location;
	g1->FillPie(pioro, System::Convert::ToInt16(punkt->X) - 25, System::Convert::ToInt16(punkt->Y) + 2, 20, 20, 0, 360);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8, System::Convert::ToInt16(punkt->Y) + 2 + 8, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20);
	g1->DrawLine(pioro_linia, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20, System::Convert::ToInt16(punkt->Y) + 8 + 20, System::Convert::ToInt16(punkt->X) - 25 + 8 + 20 + label33->Width, System::Convert::ToInt16(punkt->Y) + 8 + 20);
}



public: System::Void pictureBox2_Click(System::Object^  sender, System::EventArgs^  e) {
	okno_rysunku = gcnew Form;
	PictureBox^ pictureBox_okno = gcnew PictureBox;
	//Graphics^ g2 = okno_rysunku->CreateGraphics();
	Image^ rysunek = Image::FromFile(adrrRysTech);
	//Point punkt_poczatkowu = Point(10, 10);
	//rysunek->FromFile("rystech.jpg");
	//pictureBox_okno->Image = rysunek;
	
	pictureBox_okno->Location = System::Drawing::Point(0, 27);
	pictureBox_okno->Name = L"pictureBox_okno";
	pictureBox_okno->Size = rysunek->Size;
	okno_rysunku->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
	okno_rysunku->Size = pictureBox_okno->Size;
	pictureBox_okno->ImageLocation = adrrRysTech;
	okno_rysunku->Controls->Add(pictureBox_okno);
	
	pictureBox_okno->Click += gcnew System::EventHandler(this, &MyForm::pictureBox_okno_Click);
	
	
	okno_rysunku->Show();
	//g2->DrawImage(rysunek, 0, 0, 600, 600);
}
private: System::Void pictureBox2_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
	rysuj_kolo();
}
private: System::Void pictureBox_okno_Click(System::Object^  sender, System::EventArgs^  e) {
	okno_rysunku->Close();
}
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
	//MyForm2^ okno_zamowien = gcnew MyForm2();

	//array<Object^>^ objectArrayCombo = serialPort2->GetPortNames();

	//okno_zamowien->comboBox1->Items->AddRange(objectArrayCombo);
	//okno_zamowien->ShowDialog();
}
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {				//	rysowanie zegarka
	DateTime czas = DateTime::Now;
	label15->Text = czas.Hour.ToString() + ":" + czas.Minute.ToString("D2") + ":" + czas.Second.ToString("D2");
	

	Graphics^ g1 = this->CreateGraphics();
	g1->SmoothingMode = System::Drawing::Drawing2D::SmoothingMode::HighQuality;

	Pen^ pioroWaskie = gcnew Pen(System::Drawing::Color::White);
	pioroWaskie->Width = 10;

	Point^ punkt;
	punkt = label15->Location;
	g1->DrawArc(pioroWaskie, System::Convert::ToInt16(punkt->X) + 50 - 90, System::Convert::ToInt16(punkt->Y) + 13 - 90, 180, 180, 0, 360);
	pioroWaskie->Width = 5;
	g1->DrawArc(pioroWaskie, System::Convert::ToInt16(punkt->X) + 50 - 75, System::Convert::ToInt16(punkt->Y) + 13 - 75, 150, 150, -90, czas.Minute * 6);
	if(czas.Hour >= 12)
		g1->DrawArc(pioroWaskie, System::Convert::ToInt16(punkt->X) + 50 - 65, System::Convert::ToInt16(punkt->Y) + 13 - 65, 130, 130, - 90, czas.AddHours(12).Hour * 30);
	else
		g1->DrawArc(pioroWaskie, System::Convert::ToInt16(punkt->X) + 50 - 65, System::Convert::ToInt16(punkt->Y) + 13 - 65, 130, 130, - 90, czas.Hour * 30);
	delete pioroWaskie;
	delete g1;
	

}
private: System::Void button1_Click_1(System::Object^  sender, System::EventArgs^  e) {
	
	System::Diagnostics::Process::Start(adrrDokumentacji);		//  Otwieranie PDF z dokumentacj�
}


private: System::Void dataGridView1_CellEndEdit(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {								//  potwierdzenia i zapisanie zmian do bazy
	if (e->ColumnIndex == 3 || e->ColumnIndex == 9)
	{
		//************************* kompensacja temperaturowa wymiaru ***************************//
		Temperatura^ pobierzTemp = gcnew Temperatura();
		Double wymiar_skompensowany;
		Double wymiar_nieskompensowany = Convert::ToDouble(dataGridView1->Rows[e->RowIndex]->Cells[e->ColumnIndex]->Value);
		wymiar_skompensowany = wymiar_nieskompensowany / (1 + pobierzTemp->GetWsp(textBox14->Text) *(pobierzTemp->GetTemp() - 25));			// Dla Bartka: TU PODSTAW WZ�R DO SKOMPENSOWANIA WYMIARU
		//************************************** KONIEC *****************************************//
		System::Windows::Forms::DialogResult odp = MessageBox::Show("Zapisa� zmiany?", "Wprowadzono zmiany", MessageBoxButtons::YesNo, MessageBoxIcon::Question);
		switch (odp)
		{
		case System::Windows::Forms::DialogResult::Yes:
			switch (e->ColumnIndex)
			{
			case 3:
				updacom = gcnew NpgsqlCommand("UPDATE wymiary SET wartosc_zmierzona = @wartosc_zmierzona, wartosc_zmierzona_skomp = @wartosc_zmierzona_skomp, wprowadzajacy = @wprowadzajacy, temperatura = @temperatura WHERE id_wymiaru = @id_wymiaru", connection);
				updacom->Parameters->Clear();
				updacom->Parameters->Add("@wartosc_zmierzona", dataGridView1->Rows[e->RowIndex]->Cells[e->ColumnIndex]->Value);
				updacom->Parameters->Add("@wartosc_zmierzona_skomp", wymiar_skompensowany);
				updacom->Parameters->Add("@wprowadzajacy", label32->Text + " / " + label31->Text);
				updacom->Parameters->Add("@temperatura", pobierzTemp->GetTemp());
				//updacom->Parameters->Add("@kod", textBox8->Text);
				updacom->Parameters->Add("@id_wymiaru", dataGridView1->Rows[e->RowIndex]->Cells["id_wymiaru"]->Value);

				try {
					connection->Open();
					transaction = connection->BeginTransaction();
					updacom->Transaction = transaction;
					updacom->ExecuteNonQuery();
					transaction->Commit();
					connection->Close();
				}
				catch (Exception^ e)
				{
					transaction->Rollback();
					MessageBox::Show(e->Message, "B��d", MessageBoxButtons::OK, MessageBoxIcon::Error);
				}
				odswiez();
				break;
			default:
				odswiez();
				break;
			}

			break;
		case System::Windows::Forms::DialogResult::No:
			odswiez();
			break;
		default:
			odswiez();
			break;
		}
	}
	
}


private: System::Void dataGridView1_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
	if(e->KeyCode == Keys::Enter)
	e->SuppressKeyPress = true;
}
private: System::Void kolorowanieDataGridView() {															//sprawdzenie czy procedura pr�bkowania zosta�a dope�niona
	//int nr_cechy = 1111;
	//dataGridView1->Rows[1]->Cells[2]->Style->BackColor = Color::Red;
	try
	{
		for (Int16 i = 0; i < dataGridView1->RowCount; i++)
		{
			int nr_cechy = Convert::ToInt16(dataGridView1->Rows[i]->Cells["nr_cechy"]->Value);
			//wszystkie cechy o danym numerze w serii
			selectcomm2 = gcnew NpgsqlCommand("SELECT COUNT(wymiary.id_cechy) FROM wymiary LEFT JOIN  public.operacje ON  wymiary.id_operacji = operacje.id_operacji JOIN public.detale ON operacje.id_detalu = detale.id_detalu JOIN kat_wymiarow ON wymiary.id_cechy = kat_wymiarow.id_wymiaru WHERE  kat_wymiarow.nr_cechy = @nr_cechy;", connection);
			selectcomm2->Parameters->Add("@nr_cechy", nr_cechy);
			DataSet^ ds = gcnew DataSet("Wymiary1");
			NpgsqlDataAdapter^ adapter = gcnew NpgsqlDataAdapter(selectcomm2);
			adapter->Fill(ds);
			Int16 cechyWSerii = Convert::ToInt16(ds->Tables[0]->Rows[0][0]);
			//wszystkie cechy w o danym numerze w serii z wymiarem
			selectcomm2 = gcnew NpgsqlCommand("SELECT COUNT(wymiary.id_cechy) FROM wymiary LEFT JOIN  public.operacje ON  wymiary.id_operacji = operacje.id_operacji JOIN public.detale ON operacje.id_detalu = detale.id_detalu JOIN kat_wymiarow ON wymiary.id_cechy = kat_wymiarow.id_wymiaru WHERE  kat_wymiarow.nr_cechy =  @nr_cechy AND wymiary.wartosc_zmierzona IS NOT NULL;;", connection);
			selectcomm2->Parameters->Add("@nr_cechy", nr_cechy);
			ds->Clear();
			adapter = gcnew NpgsqlDataAdapter(selectcomm2);
			adapter->Fill(ds);
			Int16 cechyWSeriiZmierzone = Convert::ToInt16(ds->Tables[0]->Rows[0][0]);
			//jaki typ pr�bkowania stotuje si� do danej cechy
			Int16 idProceduryProbkowania = Convert::ToInt16(dataGridView1->Rows[i]->Cells["procedura_probkowania"]->Value);

			//textBox10->AppendText(Environment::NewLine + "W szystkie cechy o nr " + nr_cechy + " w serii: " + cechyWSerii + Environment::NewLine + "cechy zmierzone: " + cechyWSeriiZmierzone + Environment::NewLine + "cechy zmierzone: " + idProceduryProbkowania);



			switch (idProceduryProbkowania)
			{
			case 1:
				if (cechyWSeriiZmierzone >= 1)
					dataGridView1->Rows[i]->Cells[2]->Style->BackColor = Color::LightGreen;
				else
					dataGridView1->Rows[i]->Cells[2]->Style->BackColor = Color::Red;
				break;
			case 2:
				if (cechyWSeriiZmierzone >= 2)
					dataGridView1->Rows[i]->Cells[2]->Style->BackColor = Color::LightGreen;
				else
					dataGridView1->Rows[i]->Cells[2]->Style->BackColor = Color::Red;
				break;
			case 3:
				if (cechyWSeriiZmierzone * 10 >= cechyWSerii)
					dataGridView1->Rows[i]->Cells[2]->Style->BackColor = Color::LightGreen;
				else
					dataGridView1->Rows[i]->Cells[2]->Style->BackColor = Color::Red;
				break;
			case 4:
				if (cechyWSeriiZmierzone * 5 >= cechyWSerii)
					dataGridView1->Rows[i]->Cells[2]->Style->BackColor = Color::LightGreen;
				else
					dataGridView1->Rows[i]->Cells[2]->Style->BackColor = Color::Red;
				break;
			case 5:
				if (cechyWSeriiZmierzone * 2 >= cechyWSerii)
					dataGridView1->Rows[i]->Cells[2]->Style->BackColor = Color::LightGreen;
				else
					dataGridView1->Rows[i]->Cells[2]->Style->BackColor = Color::Red;
				break;
			case 6:																									// wszystkie z serii musz� by� zmierzone
				if (cechyWSeriiZmierzone == cechyWSerii)
					dataGridView1->Rows[i]->Cells[2]->Style->BackColor = Color::LightGreen;
				else
					dataGridView1->Rows[i]->Cells[2]->Style->BackColor = Color::Red;
				break;

				//default:
				//break;
			}

			//		dataGridView1->Rows[i]->Cells[2]->Style->BackColor = Color::Red;
		}
	}
	catch (Exception^ e)
	{

	}
	
	//textBox10->Text = Convert::ToString(dataGridView1->RowCount);
}

private: System::Void dataGridView1_Sorted(System::Object^  sender, System::EventArgs^  e) {

	kolorowanieDataGridView();
}
private: System::Void pobranieInformacji() {
	try
	{
		selectcomm = gcnew NpgsqlCommand("SELECT seria.nr_serii, kat_operacji.nr_operacji, detale.nr_detalu, stanowiska.nazwa, kat_operacji.nazwa_operacji, operacje.start_operacji, kat_operacji.id_programu, operacje.komentarz_operatora, kat_operacji.rysunek_techniczny, kat_operacji.dokumentacja FROM public.operacje, public.detale, public.seria, public.kat_operacji, public.stanowiska WHERE detale.id_detalu = operacje.id_detalu AND seria.id_serii = detale.id_serii AND kat_operacji.id_kat_operacji = operacje.id_kat_operacji AND stanowiska.id_stanowiska = kat_operacji.id_stanowiska AND operacje.id_operacji = @id_operacji;", connection);
		selectcomm->Parameters->Add("@id_operacji", odbierane->id_operacji);
		DataSet^ ds = gcnew DataSet("informacje");
		NpgsqlDataAdapter^ adapter = gcnew NpgsqlDataAdapter(selectcomm);
		adapter->Fill(ds);
		bindingSource1->DataSource = ds->Tables[0];
		DataRowView^ SelectedRow = (DataRowView^)(bindingSource1->Current);
		if (bindingSource1->Current != nullptr)					// czy cokolwiek jest w tabeli
		{
			textBox12->Text = SelectedRow[0]->ToString();								// id pracownika
			textBox13->Text = SelectedRow[1]->ToString();
			textBox14->Text = SelectedRow[2]->ToString();
			textBox15->Text = SelectedRow[3]->ToString();
			textBox16->Text = SelectedRow[4]->ToString();
			textBox17->Text = SelectedRow[5]->ToString();
			textBox18->Text = SelectedRow[6]->ToString();
			textBox10->Text = "Komentarz operatora:" + Environment::NewLine + SelectedRow[7]->ToString();
			//textBox10->AppendText(Environment::NewLine + SelectedRow[7]->ToString());
			adrrRysTech = SelectedRow[8]->ToString();
			adrrDokumentacji = SelectedRow[9]->ToString();
			//doPrzekazania->id_operatora = Convert::ToInt32(SelectedRow[0]);
			//label31->Text = SelectedRow[1]->ToString() + " " + SelectedRow[2]->ToString();
			//doPrzekazania->imie_operatora = SelectedRow[1]->ToString();
			//doPrzekazania->nazwisko_operatora = SelectedRow[2]->ToString();
			//label32->Text = SelectedRow[3]->ToString();
			//doPrzekazania->stanowisko_operatora = SelectedRow[3]->ToString();
		}
		odswiez();
	}
	catch (Exception^ e)
	{

	}
}


private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	updacom = gcnew NpgsqlCommand("UPDATE operacje SET start_operacji = NOW(), status = 2 WHERE id_operacji = @id_operacji", connection);
	updacom->Parameters->Clear();
	updacom->Parameters->Add("@id_operacji", odbierane->id_operacji);

	try {
		connection->Open();
		transaction = connection->BeginTransaction();
		updacom->Transaction = transaction;
		updacom->ExecuteNonQuery();
		transaction->Commit();
		connection->Close();
	}
	catch (Exception^ e)
	{
		transaction->Rollback();
		MessageBox::Show(e->Message, "B��d", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	updacom = gcnew NpgsqlCommand("UPDATE operacje SET stop_operacji = NOW(), status = 6 WHERE id_operacji = @id_operacji", connection);
	updacom->Parameters->Clear();
	updacom->Parameters->Add("@id_operacji", odbierane->id_operacji);
	try
	{
		connection->Open();
		transaction = connection->BeginTransaction();
		updacom->Transaction = transaction;
		updacom->ExecuteNonQuery();
		transaction->Commit();
		connection->Close();
		//MyForm^ main_form = gcnew MyForm();
		//main_form->Show();
	}
	catch (Exception^ e)
	{
		transaction->Rollback();
		MessageBox::Show(e->Message, "B��d", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
}
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
	ObrobkaForm^ obrobkaForm = gcnew ObrobkaForm();
	obrobkaForm->Show();
}
private: System::Void button6_Click_1(System::Object^  sender, System::EventArgs^  e) {

	PomiaryForm^ pomiaryForm = gcnew PomiaryForm();
	pomiaryForm->nrZlecenia = Convert::ToInt32(textBox12->Text);
	pomiaryForm->nrOperacji = Convert::ToInt32(textBox13->Text);
	pomiaryForm->nrCzesci = Convert::ToInt32(textBox14->Text);
	pomiaryForm->nrCechyTomek = Convert::ToDouble(dataGridView1->CurrentRow->Cells["nr_cechy"]->Value);

	//int rows = dataGridView1->RowCount;
	//array<String^>^ nrwymiaru = gcnew array<String^>(0);
	//array<Double>^ wartosc = gcnew array<Double>(0);
	//array<Double>^ tolgorna = gcnew array<Double>(0);
	//array<Double>^ toldolna = gcnew array<Double>(0);

	//for (int i = 0; i < rows; i++)
	//{
	//	nrwymiaru->Resize(nrwymiaru, i + 1);
	//	nrwymiaru[i] = dataGridView1->Rows[i]->Cells[1]->Value->ToString();
	//	wartosc->Resize(wartosc, i + 1);
	//	if (dataGridView1->Rows[i]->Cells[3]->Value->ToString() == "" )
	//	{
	//		wartosc[i] = 0;
	//	}
	//	else
	//	{
	//		wartosc[i] = Convert::ToDouble(dataGridView1->Rows[i]->Cells[3]->Value);
	//	}
	//	tolgorna->Resize(tolgorna, i + 1);
	//	tolgorna[i] = Convert::ToDouble(dataGridView1->Rows[i]->Cells[5]->Value);
	//	toldolna->Resize(toldolna, i + 1);
	//	toldolna[i] = Convert::ToDouble(dataGridView1->Rows[i]->Cells[4]->Value);

	//}
	//pomiaryForm->nrwymiaru = nrwymiaru;
	//pomiaryForm->wartosc = wartosc;
	//pomiaryForm->toldolna = toldolna;
	//pomiaryForm->tolgorna = tolgorna;
	//pomiaryForm->wprowadzajacy = label32->Text + " / " + label31->Text;
	//
	//for (int i = 0; i < dataGridView1->Rows->Count; i++)
	//{
	//	pomiaryForm->idWymiarow->Resize(pomiaryForm->idWymiarow, i + 1);
	//	pomiaryForm->idWymiarow[i] = Convert::ToInt32(dataGridView1->Rows[i]->Cells["id_wymiaru"]->Value);
	//	pomiaryForm->nrCechy->Resize(pomiaryForm->nrCechy, i + 1);
	//	pomiaryForm->nrCechy[i] = Convert::ToInt16(dataGridView1->Rows[i]->Cells["nr_cechy"]->Value);

	//}

	////System::Diagnostics::Process::Start("C://Users//PSIK1//Desktop//Suwmiarka � kopia3//x64//Release//Suwmiarka.exe");
	//
	pomiaryForm->ShowDialog();
	////System::Diagnostics::Process::Start("C://Users//PSIK1//Desktop//Suwmiarka � kopia3//x64//Release//Suwmiarka.exe");
	//odswiez();

	//
	////System::Diagnostics::Process::Start("D:/Tomek/logowanie/Debug/logowanie.exe");

	/*ustawienia->ShowDialog();
	urzadzenia();*/

}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
	KomentarzForm^ komentarzForm = gcnew KomentarzForm();
	komentarzForm->id_operacji = odbierane->id_operacji;

	komentarzForm->ShowDialog();

	pobranieInformacji();

}
private: System::Void pobranie_SPC(Int16 nr_Cechy) {
	try
	{
		NpgsqlConnection^ connection2 = gcnew NpgsqlConnection("server=127.0.0.1; Port=5432; User Id=postgres; Password=tomek; Database=postgres");
		connection2->Open();
		NpgsqlCommand^ selectcomm22 = gcnew NpgsqlCommand("SELECT kartaX FROM kat_wymiarow  WHERE  nr_cechy = @nr_cechy;", connection2);
		selectcomm22->Parameters->Add("@nr_cechy", 1112);
		DataSet^ ds = gcnew DataSet("Wymiary1");
		NpgsqlDataAdapter^ adapter = gcnew NpgsqlDataAdapter(selectcomm22);
		adapter->Fill(ds);
		//array<Byte>^ kartaBin = Convert::ToByte(ds->Tables[0]->Rows[0][0]);


		Object^ SqlRetVal = nullptr;
		SqlRetVal = selectcomm22->ExecuteScalar();

		array<Byte>^ rawData = gcnew array<Byte>(0);
		rawData = safe_cast<array<Byte>^>(SqlRetVal);

		MemoryStream^ memStream = gcnew MemoryStream();
		memStream->Write(rawData, 0, rawData->Length);
		Image^ kartaX = Image::FromStream(memStream);
		//Image^ kartaX = Image::FromStream((rawData));
		//pictureBox1->Image = kartaX;
		pictureBox3->Image = kartaX;
	}
	catch (Exception^ e)
	{

	}
	
}
private: System::Void pictureBox3_Click(System::Object^  sender, System::EventArgs^  e) {
	//okno_rysunku = gcnew Form;
	//PictureBox^ pictureBox_okno = gcnew PictureBox;
	////Graphics^ g2 = okno_rysunku->CreateGraphics();
	//Image^ rysunek = pictureBox3->Image;
	////Point punkt_poczatkowu = Point(10, 10);
	////rysunek->FromFile("rystech.jpg");
	////pictureBox_okno->Image = rysunek;

	//pictureBox_okno->Location = System::Drawing::Point(0, 27);
	//pictureBox_okno->Name = L"pictureBox_okno";
	//pictureBox_okno->Size = rysunek->Size;
	//okno_rysunku->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
	//okno_rysunku->Size = pictureBox_okno->Size;
	//pictureBox_okno->Image = pictureBox3->Image;
	//okno_rysunku->Controls->Add(pictureBox_okno);

	//pictureBox_okno->Click += gcnew System::EventHandler(this, &MyForm::pictureBox_okno_Click);

	if (dataGridView1->CurrentCell->RowIndex < klasywymiary->Length)
	{
		klasywymiary[dataGridView1->CurrentCell->RowIndex]->Show();
	}
	PomiaryForm^ pomiaryForm = gcnew PomiaryForm();
	pomiaryForm->nrZlecenia = Convert::ToInt32(textBox12->Text);
	pomiaryForm->nrOperacji = Convert::ToInt32(textBox13->Text);
	pomiaryForm->nrCzesci = Convert::ToInt32(textBox14->Text);
	pomiaryForm->nrCechyTomek = Convert::ToDouble(dataGridView1->CurrentRow->Cells["nr_cechy"]->Value);
	pomiaryForm->ShowDialog();
	//okno_rysunku->Show();
	

}

private: System::Void MyForm_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
	if (e->KeyCode == Keys::Enter)
	{
		e->SuppressKeyPress = true;
		enter();
	}
}
private: void enter()
{
	int numerek = klasywymiary[dataGridView1->CurrentCell->RowIndex]->nr_urzadzenia;
	if (numerek == ustawienia->urzadzenia->Length)
	{
		if (textBox9->Text != "")
		{
			int numer = dataGridView1->CurrentCell->RowIndex;
			pomiar = Double::Parse(textBox9->Text);
			pomiar_skom = pomiar / (1 + wspolczynnik*(temperatura - 25));
			sprawdz(numer);
			textBox9->Clear();
		}
	}
	else
	{
		int numer = dataGridView1->CurrentCell->RowIndex;
		Ustawienia1::Ustawienia::urzadzenia[numerek]->wyslij();
		Threading::Thread::Sleep(200);
		pomiar = Ustawienia1::Ustawienia::urzadzenia[numerek]->pomiar;
		pomiar_skom = pomiar / (1 + wspolczynnik*(temperatura - 25));
		sprawdz(numer);
	}
}
private: void sprawdz(int numer)
{
	System::Windows::Forms::DialogResult odp;
	if ((pomiar_skom >= klasywymiary[numer]->toldolna) && (pomiar_skom <= klasywymiary[numer]->tolgorna))
	{
		odp = MessageBox::Show("Pomiar " + Math::Round(pomiar_skom, 3).ToString() + " MIE�CI si� w tolerancji wymiaru "
			+ klasywymiary[numer]->nazwa + "\r\nCzy chcesz zachowa� ten pomiar?", "Pytanie", MessageBoxButtons::YesNo, MessageBoxIcon::Question);
	}
	else
	{
		odp = MessageBox::Show("Pomiar " + Math::Round(pomiar_skom, 3).ToString() + " NIE MIE�CI si� w tolerancji wymiaru "
			+ klasywymiary[numer]->nazwa + "\r\nCzy chcesz zachowa� ten pomiar?", "Ostrze�enie", MessageBoxButtons::YesNo, MessageBoxIcon::Warning);
	}
	switch (odp)
	{
	case System::Windows::Forms::DialogResult::Yes:
		klasywymiary[numer]->Show();
		klasywymiary[numer]->wyswietl(pomiar_skom, temperatura);
		klasywymiary[numer]->wyswietl2(pomiar);
		//dataGridView1->Rows[numer]->Cells[5]->Value = Math::Round(pomiar_skom, 3);
		//dataGridView1->Rows[numer]->Cells[6]->Value = pomiar;
		if (dataGridView1->CurrentCell->RowIndex <klasywymiary->Length)
		{
			pictureBox3->Image = klasywymiary[dataGridView1->CurrentCell->RowIndex]->karta;
		}
		break;
	case System::Windows::Forms::DialogResult::No:
		break;
	default:
		break;
	}
}
private: void zaladowanie_ustawien()
{
	StreamReader^ plik = gcnew StreamReader("..\\emptyTest1\\niewiem.txt", System::Text::Encoding::Default);
	StreamReader^ plik2 = gcnew StreamReader("..\\emptyTest1\\niewiem2.txt", System::Text::Encoding::Default);
	String^ linia = "";
	int k = 0;
	int i = 0;
	String^ nazwaurzadzenia = "";
	String^ nrportu = "";
	String^ kalibracja = "";
	String^ Cg = "";
	String^ Cgk = "";
	array<String^>^ nazwy = gcnew array<String^>(0);
	array<String^>^ nrportow = gcnew array<String^>(0);
	array<String^>^ kalibracje = gcnew array<String^>(0);
	array<String^>^ tabCg = gcnew array<String^>(0);
	array<String^>^ tabCgk = gcnew array<String^>(0);
	try
	{
		while (linia = plik->ReadLine())
		{
			for (int j = 0; j < linia->Length; j++)
			{
				if (linia[j] == ' ') {
					k++;
				}
				if ((linia[j] != ' ') && (k == 0))
				{
					nazwaurzadzenia = nazwaurzadzenia + linia[j];
				}
				if ((linia[j] != ' ') && (k == 1))
				{
					nrportu = nrportu + linia[j];
				}
				if ((linia[j] != ' ') && (k == 2))
				{
					kalibracja = kalibracja + linia[j];
				}
				if ((linia[j] != ' ') && (k == 3))
				{
					Cg = Cg + linia[j];
				}
				if ((linia[j] != ' ') && (k == 4))
				{
					Cgk = Cgk + linia[j];
				}
			}
			k = 0;
			if (nazwaurzadzenia != "")
			{
				nazwy->Resize(nazwy, i + 1);
				nazwy[i] = nazwaurzadzenia;
				kalibracje->Resize(kalibracje, i + 1);
				kalibracje[i] = kalibracja;
				nrportow->Resize(nrportow, i + 1);
				nrportow[i] = nrportu;
				tabCg->Resize(tabCg, i + 1);
				tabCg[i] = Cg;
				tabCgk->Resize(tabCgk, i + 1);
				tabCgk[i] = Cgk;
				nazwaurzadzenia = "";
				kalibracja = "";
				nrportu = "";
				Cg = "";
				Cgk = "";				
			}
			i++;
		}
	}
	catch (Exception^ e)
	{
		MessageBox::Show(e->Message, "B��d", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
	ustawienia = gcnew Ustawienia(nazwy, nrportow, kalibracje, tabCg, tabCgk);
	linia = plik2->ReadLine();
	k = 0;
	ustawienia->textBox1->Clear();
	ustawienia->textBox2->Clear();
	ustawienia->textBox3->Clear();
	ustawienia->textBox4->Clear();
	ustawienia->textBox5->Clear();
	ustawienia->textBox6->Clear();
	for (int j = 0; j < linia->Length; j++)
	{
		if (linia[j] == ' ') {
			k++;
		}
		if ((linia[j] == '1') && (k == 0))
			ustawienia->checkBox1->Checked = true;
		if ((linia[j] == '0') && (k == 0))
			ustawienia->checkBox1->Checked = false;
		if ((linia[j] == '1') && (k == 1))
			ustawienia->checkBox2->Checked = true;
		if ((linia[j] == '0') && (k == 0))
			ustawienia->checkBox2->Checked = false;
		if ((linia[j] == '1') && (k == 2))
			ustawienia->checkBox3->Checked = true;
		if ((linia[j] == '0') && (k == 0))
			ustawienia->checkBox3->Checked = false;
		if ((linia[j] == '1') && (k == 3))
			ustawienia->checkBox4->Checked = true;
		if ((linia[j] == '0') && (k == 0))
			ustawienia->checkBox4->Checked = false;
		if ((linia[j] != ' ') && (k == 4))
			ustawienia->textBox1->Text += linia[j];
		if ((linia[j] != ' ') && (k == 5))
			ustawienia->textBox3->Text += linia[j];
		if ((linia[j] != ' ') && (k == 6))
			ustawienia->textBox2->Text += linia[j];
		if ((linia[j] != ' ') && (k == 7))
			ustawienia->textBox5->Text += linia[j];
		if ((linia[j] != ' ') && (k == 8))
			ustawienia->textBox4->Text += linia[j];
		if ((linia[j] != ' ') && (k == 9))
			ustawienia->textBox6->Text += linia[j];
	}
	plik2->Close();
	plik->Close();
}
private: void urzadzenia()
{
	dataGridView2->Rows->Clear();
	int l = 0;
	for each (Portszeregowy^ var in ustawienia->urzadzenia)
	{
		dataGridView2->Rows->Add();
		dataGridView2->Rows[l]->Cells[0]->Value = var->nazwa;
		l++;
	}
	dataGridView2->Rows->Add();
	dataGridView2->Rows[l]->Cells[0]->Value = "Sprawdzian";
}
private: void zaladowanie_wymiarow()
{
	//
	// do wywalenia
	//
	array <Double, 2>^pomiary_s = { { 3, 4, 3.5 },{ 21.5, 22, 21, 21.5 },{ 42.1, 42.3, 42.2, 44.4 },{ 29, 28, 30, 31 } };
	array <Double, 2>^pomiary = { { 3.1, 4.1, 3.53 },{ 21.55, 22.1, 21.2, 21.3 },{ 42.15, 42.35, 42.25, 44.45 },{ 29.1, 28.1, 30.1, 31.1 } };
	//pomiary_s = { {3, 4, 3.5, 3.8 }, {21.5, 22, 21, 21.5 }, {42.1, 42.3, 42.2, 44.4 }, {29, 28, 30, 31 } };
	//
	//
	//
	int l = 0;
	for each (DataGridViewRow^ var in dataGridView1->Rows)
	{
		klasywymiary->Resize(klasywymiary, l + 1);
		klasywymiary[l] = gcnew Wymiar1::Wymiar();
		klasywymiary[l]->nr = dynamic_cast<String^>(dataGridView1->Rows[l]->Cells[0]->Value);
		klasywymiary[l]->nazwa = dynamic_cast<String^>(dataGridView1->Rows[l]->Cells[1]->Value);
		klasywymiary[l]->toldolna = static_cast<Double>(dataGridView1->Rows[l]->Cells[4]->Value);
		klasywymiary[l]->tolgorna = static_cast<Double>(dataGridView1->Rows[l]->Cells[5]->Value);
		klasywymiary[l]->wartosc = klasywymiary[l]->toldolna + ((klasywymiary[l]->toldolna + klasywymiary[l]->tolgorna) / 2);
		//klasywymiary[l]->pomiary=               pomiary skompensowane
		//klasywymiary[l]->pomiary2=				pomiary nieskompensowane
		//klasywymiary[l]->temperatury=				temp
		//
		//to poni�ej trza zast�pi� i wpisywa� ze bazy (do wywalenia)
		//
		klasywymiary[l]->pomiary->Resize(klasywymiary[l]->pomiary, 4);
		klasywymiary[l]->pomiary2->Resize(klasywymiary[l]->pomiary2, 4);
		klasywymiary[l]->temperatury->Resize(klasywymiary[l]->temperatury, 4);
		for (int i = 0; i < 4; i++)
		{
				klasywymiary[l]->pomiary[i] = pomiary_s[l,i];
				klasywymiary[l]->pomiary2[i] = pomiary[l, i];
				klasywymiary[l]->temperatury[i] = 48 + i;
		}
		klasywymiary[l]->Show();
		klasywymiary[l]->Visible = false;
		l++;
		//
		//
		//		
	}
}
private: System::Void dataGridView2_SelectionChanged(System::Object^  sender, System::EventArgs^  e) {

	
	
}
private: System::Void dataGridView2_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	if (dataGridView1->CurrentCell->RowIndex < klasywymiary->Length)
	{
		klasywymiary[dataGridView1->CurrentCell->RowIndex]->nr_urzadzenia = dataGridView2->CurrentCell->RowIndex;
		if (dataGridView2->CurrentCell->RowIndex < ustawienia->urzadzenia->Length)
		{
			klasywymiary[dataGridView1->CurrentCell->RowIndex]->urzadzenie = ustawienia->urzadzenia[dataGridView2->CurrentCell->RowIndex]->nazwa;
		}
		if (dataGridView2->CurrentCell->RowIndex == ustawienia->urzadzenia->Length)
		{
			klasywymiary[dataGridView1->CurrentCell->RowIndex]->urzadzenie = "Sprawdzian";
		}
	}
}
};
}
